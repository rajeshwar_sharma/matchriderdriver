//
//  Utilities.swift
//  MatchRider-Bussiness
//
//  Created by daffolapmac on 20/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import MFSideMenu
import SwiftyJSON
import FBSDKCoreKit
import FBSDKLoginKit
class Utilities {
    
    class func getTopViewController()-> UIViewController
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        var vc = UIViewController()
        if  (topController is MFSideMenuContainerViewController) {
            let  sideMenuVC : MFSideMenuContainerViewController = topController as! MFSideMenuContainerViewController
            let navController = sideMenuVC.centerViewController as! UINavigationController
            vc = navController.topViewController!
        }
        
        return vc
    }
    
    class func getCurrentNavigationController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UINavigationController {
        
        
        let rootVC = base
        if rootVC?.presentedViewController == nil {
//            if let nav = base as? UINavigationController {
//                return nav
//            }
            
            if  (base is MFSideMenuContainerViewController) {
                let  sideMenuVC : MFSideMenuContainerViewController = base as! MFSideMenuContainerViewController
                let navController = sideMenuVC.centerViewController as! UINavigationController
                return navController
            }
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController
            }else{
                if (base is MFSideMenuContainerViewController) {
                    let  sideMenuVC : MFSideMenuContainerViewController = base as! MFSideMenuContainerViewController
                    let navController = sideMenuVC.centerViewController as! UINavigationController
                    return navController
                }
            }
            
        }
        
        return  UINavigationController()
    }
    
    
    class func toggelMenuToLeft(){
        let topController = UIApplication.shared.keyWindow?.rootViewController
        if  (topController is MFSideMenuContainerViewController) {
            let  sideMenuVC : MFSideMenuContainerViewController = topController as! MFSideMenuContainerViewController
            sideMenuVC.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    class func setSideMenuPanModeToDefault(){
        let topController = UIApplication.shared.keyWindow?.rootViewController
        if  (topController is MFSideMenuContainerViewController) {
            let  sideMenuVC : MFSideMenuContainerViewController = topController as! MFSideMenuContainerViewController
            sideMenuVC.panMode = MFSideMenuPanModeDefault
        }
    }
    
    class func checkedImage(_ checked: Bool) -> UIImage {
        if checked {
            return UIImage(named: "checked")!
        } else {
            return UIImage(named: "unchecked")!
        }
    }
    
    //    class func getUserDetailInDictionaryFormat(json:JSON)-> [String:Any]{
    //
    //        let dic: [String:Any] = [ "UserId":json["UserId"].string!,
    //                    "PersonId":json["PersonId"].int!,
    //                    "UserNumber":json["UserNumber"].string!
    //
    //                ]
    //        return dic
    //
    //    }
    
    class func saveUserData(user: Data){
        userDefault.set(user, forKey: "User")
    }
    class func getSignInUser()->User?{
        if let user = userDefault.object(forKey: "User"){
//            let userJson = HttpRequest.sharedRequest.parseData(data: user as! Data)
//            let loginUser = User(json: userJson)
//            
//            return loginUser
            let userJson = HttpRequest.sharedRequest.parseData(data: user as! Data)
            let jsonDic = userJson["Payload"].array!.first
            let loginUser = User(json: jsonDic!)
            return loginUser

        }
        else{
            return nil
        }
    }
    
    class func isUserLogedIn()->Bool{
        if (userDefault.object(forKey: "User") != nil){
            return true
        }
        else{
            return false
        }
    }
    class func logout(){
        userDefault.removeObject(forKey: "User")
        if appDelegate.timer != nil{
            appDelegate.timer!.invalidate()
        }
        FBSDKLoginManager().logOut()
        appDelegate.carDetail = nil
        DataContainer.sharedInstance.commonModel.removeAll()
        userDefault.removeObject(forKey: TimerStartTime)
        userDefault.removeObject(forKey: TimerDuration)
        Utilities.navigateToViewController(viewController: "LoginViewController")
    }
    class func  bundelResource(withName name: String)->[String: Any]{
        if let path = Bundle.main.path(forResource: name, ofType: "plist") {
            if let dic = NSDictionary(contentsOfFile: path) as? [String: Any] {
                return dic
            }
        }
        return [:]
    }
   class func navigateToViewController(viewController: String)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navContr = Utilities.getTopViewController().navigationController
        let vc = storyBoard.instantiateViewController(withIdentifier: viewController)
        navContr?.setViewControllers([vc], animated: false)
        
    }
   class func makeCall(number: String){
    
        if let url = NSURL(string: "tel://\(number.replacingOccurrences(of: " ", with: ""))") , UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
}

func CentsToString(_ cents: Int) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.currencyCode = "EUR"
    let value = Double(cents) / 100
    //return formatter.string(from: NSNumber(value))!
    
    return formatter.string(from: NSNumber(value: value))!
}

func DistanceToString(_ km: Double) -> String {
    return "\(km) km"
}
func SecondsToHsMsSs(_ seconds : Int) -> String {
    let duration =  "\(timeText(seconds / 3600)):\(timeText((seconds % 3600) / 60)):\(timeText((seconds % 3600) % 60))"
    return duration
}
func SecondsToHsMs(_ seconds : Int) -> String {
    
    var time = ""
    if seconds / 3600 != 0{
        time = "\(seconds / 3600) Stunde "
    }
    
    if (seconds % 3600) % 60 > 29{
        
        let second = (seconds % 3600) / 60 + 1
         time = "\(time)\(second) Minuten"
    }else{
       time = "\(time)\((seconds % 3600) / 60) Minuten"
    }
    
    return time
}
func timeText(_ s: Int) -> String {
    return s < 10 ? "0\(s)" : "\(s)"
}

func StringToDate(_ string: String) -> Date {
    let dateFormatter = DateFormatter() //2017-02-27 08:00:00
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateFormatter.date(from: string)!
}

func convertToDate(_ string: String) -> Date {
    let dateFormatter = DateFormatter() //"21-Apr 08:16"
    dateFormatter.dateFormat = "dd-MM HH:mm"
    dateFormatter.locale = Locale(identifier: "en_US") // Region set to English
    return dateFormatter.date(from: string)!
}

func getDriverTimeAtStop(_ string: String) -> String {
    let dateFormatter = DateFormatter() //"21-Apr 08:16"
    //dateFormatter.dateFormat = "dd-MM HH:mm"
    dateFormatter.dateFormat = "dd-MMM HH:mm"
    dateFormatter.locale = Locale(identifier: "en_US")  // Region set to English
    let date = dateFormatter.date(from: string)
    dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.dateFormat = "d. MMM HH:mm"
  
    return dateFormatter.string(from: date!)
}

func getDateFromTimeString(timeStr: String)->Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone! //change on 28/08/17
    // dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.locale = Locale(identifier: "en_US")
    if dateFormatter.date(from: timeStr) != nil{
        return dateFormatter.date(from: timeStr)!
    }
    else{
      dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
       return dateFormatter.date(from: timeStr)!
    }
 }

func getCurrentTimeFromTimeString(timeStr: String)->Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.locale = Locale(identifier: "en_US")
    if dateFormatter.date(from: timeStr) != nil{
        return dateFormatter.date(from: timeStr)!
    }
    else{
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        return dateFormatter.date(from: timeStr)!
    }
}


func getDateStringFromDate(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMM HH:mm"
    return dateFormatter.string(from: date)
}
func getCompleteDateFromDate(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "de_DE")
    //dateFormatter.dateFormat = "EEEE, dd MMM HH:mm"
    dateFormatter.dateFormat = "EEEE, d. MMM HH:mm"
    return dateFormatter.string(from: date)
}
func getDayWithDateFromDate(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.dateFormat = "EEEE, dd MMM"
    return dateFormatter.string(from: date)
}

func getDateWithYear(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.dateFormat = "EEEE, dd MMM yyyy"
    return dateFormatter.string(from: date)
}
func getTime(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.string(from: date)
}



func getDateWithDay(_ date: Date,timeFlag:Bool)-> String{
    //    formatter.dateFormat =  "EEEE',' MMMM dd',' yyyy"
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "de_DE")
    let difference = Calendar.current.dateComponents([.day,.hour,.minute], from: Date(), to: date)
    if difference.day! == 0 || difference.day! == 1{
        formatter.dateStyle = .short
        timeFlag ?(formatter.timeStyle = .short):(formatter.timeStyle = .none)
        formatter.doesRelativeDateFormatting = true
        return formatter.string(from: date)
    }
    else if difference.day! > 1 && difference.day! < 7{
        timeFlag ?(formatter.dateFormat = "EEEE, HH:mm"):(formatter.dateFormat = "EEEE")
        return  formatter.string(from: date).capitalized
        
    }else{
        timeFlag ?(formatter.dateFormat = "EEEE, MMMM dd, HH:mm"):(formatter.dateFormat = "EEEE, MMMM dd")
        return  formatter.string(from: date).capitalized
    }
    
}

func getAccountDetails(){
    let contoller =  Utilities.getTopViewController()
    contoller.view.showLoader()
    HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getAccountDetails) { (data) in
        guard let data = data else{
            contoller.view.hideLoader()
            return
        }
        let json = HttpRequest.sharedRequest.parseData(data: data)
        if json["Status"].string == HttpRequest.Status.Success{
            registerFCMDeviceTokenToServer()
            Utilities.saveUserData(user: data)
        }
        contoller.view.hideLoader()
    }
}

var updateAppCheck = false
func showAlertForNewVersionUpdate(){
    let actionSheetController: UIAlertController = UIAlertController(title: "Neues Update gefunden", message: "Um die App weiter nutzen zu können, lade dir bitte das Update herunter", preferredStyle: .alert)
    let cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
        exit(0)
    }
    actionSheetController.addAction(cancel)
    
    let okAction: UIAlertAction = UIAlertAction(title: "Update", style: .default) { action -> Void in
        UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/us/app/matchridergo-driver/id1236636702?mt=8")!)
        
        
    }
    actionSheetController.addAction(okAction)
    
    
    
    let navController = Utilities.getCurrentNavigationController()
    navController.present(actionSheetController, animated: true, completion: nil)
}

func RouteDirectionString(_ from: String, to: String) -> String {
    return "\(from) ➡︎ \(to)"
}
