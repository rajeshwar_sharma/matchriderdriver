import SwiftyJSON
import Foundation

struct Token {
  
  let id: String
  
  init(json: JSON) {
    id = json["token"].string!
  }
  
  init(string: String) {
    id = string
  }
  
  var params: [String: String] {
    return ["token": id]
  }
  
}
