//
//  DataContainer.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 28/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class DataContainer: NSObject {
    
    private override init() { }
    
    static let sharedInstance = DataContainer()
    
    // MARK: Local Variable
    
     var commonModel: [Any] = [] //Contains My Rides
     var rideSpecificId: Int?
     var reloadRideDetail = false
     var refreshGPSTracking = false
}
