//
//  ColorConstant.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//let MatchRiderColor = UIColor(red: 125/255.0, green: 190/255.0, blue: 127/255.0, alpha: 1)
let MatchRiderColor = "7DBE7F"
let TableSectionDefaultColor = "EBEBF1"
let LineLightGrayColor = "E1E1E1"
