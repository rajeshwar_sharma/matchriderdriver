//
//  StringConstant.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
//************ Check for device *************
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
let userDefault = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate

//*************** Basic String Constant ***********




let TimerStartTime = "TimerStartTime"
let TimerDuration  = "TimerDuration"
let MIXPANEL_TOKEN = "55d3006c85f3f6e195b99f861a228fa0"
let randomNum = 999999
// ************************************* Validation messages ****************************************


let ENTER_PASSWORD                   = "Bitte Passwort eingeben"
let CONFIRM_PASSWORD           = "Bitte bestätige Passwort"
let PASSWORD_MISMATCH                = "Passwörter stimmen nicht überein"
let ENTER_NAME                       = "Please enter your Full Name"
let ENTER_EMAIL_ID                   = "Bitte E-Mail-ID eingeben"
let ENTER_VALID_EMAIL_ID             = "Geben Sie eine gültige E-Mail-ID ein"
let ENTER_FIRST_NAME                 = "Bitte Vorname eingeben"
let ENTER_LAST_NAME                  = "Bitte Nachname eingebenn"
let ENTER_GENDER                     = "Bitte Geschlecht eingebenn"
let ENTER_POSTAL_CODE                = "Bitte Postleitzahl eingebenn"
let ENTER_TERMS_CONDITION            = "Bitte akzeptiere AGB"
let ShowLine    = "showLine"
let Monday = "Montag"

//****************** Message Alert *********************

let NoNetworkError = "Es konnte keine Verbindung mit MatchRiderGO aufgebaut werden"

let LoginError = "Inkorrekte Email oder Passwort. Bitte versuche es noch einmal."
let HolidayAddedSuccessfully = "Dein Urlaub wurde erfolgreich eingetragen."
let HolidayUpdatedSuccessfully = "Commute Holiday updated successfully."
let FailedToAddCommuteHoliday = "Fehler beim Kalendereintrag."
let FailedToUpdateCommuteHoliday = "Failed to update commute holiday."
let CommuteHolidayDeleted = "Dein Urlaub wurde erfolgreich gelöscht."
let FailedToDeleteCommuteHoliday = "Failed to delete commute holiday."

let EnquirySuccess = "Deine Nachricht wurde versendet."
let EnquiryFailed = "Einreichung fehlgeschlagen."

let TapToSelectRoute = "Tippen Sie auf, um die Route auszuwählen"
let TapToChangeRegion = "Tippen Sie auf, um die Region zu ändern"
let TapToChangeRoute = "Tippen Sie auf, um die Route zu ändern"
let RoutePreferenceUpdatedSuccessfully = "Route Präferenz erfolgreich aktualisiert"
let PleaseSelectRegionFirst = "Bitte zuerst Region auswählen"
let NoRoutesAvailableForSelectedRegion = "Keine Routen für ausgewählte Region verfügbar"
let Skip = "Unverbindlich anmelden"
let DriverRegistration = "Fahrer Anmeldung"
let GenericError = "Es ist ein Fehler aufgetreten."
let RideTimeError = "Die Fahrzeit kann nicht kleiner sein als die aktuelle Uhrzeit."
let NoRoutesAvailableToOffer = "Bitte kontaktiere uns um deine regelmäßige Fahrt einzustellen"

//let AppVersion = "1.1.0"
let AppVersion = "2.0"












