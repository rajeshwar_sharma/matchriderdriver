                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 //
//  AppDelegate.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MFSideMenu
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import Mixpanel
import Fabric
import Crashlytics
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SwiftyJSON
import MapKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var timer: Timer?
    var window: UIWindow?
    var storyboard : UIStoryboard!
    var nextRide: Ride!
    var vehicleId: Int! = 0
    var carDetail: Car!
    var navigationMatchpoints: [MKAnnotation] = []
    var driverAcceptRejectPassenger: Bool = false
    var sideMenuContainerViewController : MFSideMenuContainerViewController?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Rajeshwar
        //"EEE, dd MMM yyyy HH:mm:ss ZZZ
               
        Mixpanel.initialize(token: MIXPANEL_TOKEN)
        Mixpanel.mainInstance().track(event: "Tracked Event!")
        Fabric.with([Crashlytics.self])
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        IQKeyboardManager.sharedManager().enable = true
        setRootViewController()
        FIRApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
    
        
        return true
    }
    
 
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        FBSDKAppEvents.activateApp()
        if updateAppCheck {
            showAlertForNewVersionUpdate()
        }

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func setRootViewController()
    {
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.tintColor = hexStringToUIColor(hex: MatchRiderColor)
        var centerVC: UIViewController!
        if !Utilities.isUserLogedIn() {
            centerVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        }else{
            centerVC = storyboard.instantiateViewController(withIdentifier: "GPSTrackingViewController") as? GPSTrackingViewController
        }
        let navigationController = UINavigationController(rootViewController: centerVC!)
        navigationController.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white
        ]
        let leftmenuViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        sideMenuContainerViewController = MFSideMenuContainerViewController.container(withCenter: navigationController, leftMenuViewController: leftmenuViewController, rightMenuViewController: nil);
        sideMenuContainerViewController?.panMode =  MFSideMenuPanModeDefault
        sideMenuContainerViewController?.shadow.enabled = true;
        sideMenuContainerViewController?.setMenuWidth(self.window!.bounds.size.width*0.75, animated: true);
        self.window!.rootViewController = sideMenuContainerViewController
        self.window!.makeKeyAndVisible()
    }
    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let contoller =  Utilities.getTopViewController()
        let chatDetail = getChatDetailData(userInfo: userInfo)
        if (application.applicationState == .active ) {
            //Run when app is Active
            let rideSpecificFlag = getRideSpecificDetail(userInfo: userInfo)
            if rideSpecificFlag{
                
                let rideId =  getRideId(userInfo: userInfo)
                DataContainer.sharedInstance.rideSpecificId = rideId
                
                let contoller =  Utilities.getTopViewController()
                if contoller is RideDetailViewController || contoller is UpCommingTripViewController{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
                }
                showNotificationWhenAppIsActive(userInfo: userInfo)
                
            }else{
                if contoller is MessageViewController{
                    
                    if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId || chatDetail.messageThreadId == "Default"{
                        postNotification(chatDetail: chatDetail, check: true)
                    }else{
                        showNotificationWhenAppIsActive(userInfo: userInfo)
                    }
                }else{
                    showNotificationWhenAppIsActive(userInfo: userInfo)
                }
            }
            return
        }
        
        //Run when app comes from background
        
        let rideSpecificFlag = getRideSpecificDetail(userInfo:userInfo)
        if rideSpecificFlag{
            
            let rideId =  getRideId(userInfo: userInfo)
            DataContainer.sharedInstance.rideSpecificId = rideId
            
            let contoller =  Utilities.getTopViewController()
            if contoller is RideDetailViewController{
                DataContainer.sharedInstance.reloadRideDetail = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }else{
                redirectToRideDetailViewController()
            }
            
        }else{
            
            if contoller is MessageViewController{
                if chatDetail.messageThreadId != Store.sharedStore.currentMessageThreadId{
                    postNotification(chatDetail: chatDetail, check: false)
                }
            }else{
                redirectToMessageViewController(chatDetail: chatDetail)
            }
        }
    }
    func showNotificationWhenAppIsActive(userInfo: [AnyHashable: Any]){
        let tmpDic = userInfo["aps"] as! NSDictionary
        let alertDic = tmpDic["alert"] as! NSDictionary
        let localNotification = UILocalNotification()
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.alertTitle = alertDic["title"] as? String
        localNotification.alertBody = alertDic["body"] as? String
        localNotification.fireDate = Date()
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    // [START connect_to_fcm]
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let rideSpecificFlag = getRideSpecificDetail(userInfo: notification.request.content.userInfo)
        if rideSpecificFlag{
            
            let rideId =  getRideId(userInfo: notification.request.content.userInfo)
            DataContainer.sharedInstance.rideSpecificId = rideId
            
            let contoller =  Utilities.getTopViewController()
            if contoller is RideDetailViewController || contoller is UpCommingTripViewController{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }
            if contoller is GPSTrackingViewController{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }else{
               DataContainer.sharedInstance.refreshGPSTracking = true
            }
            
            completionHandler(UNNotificationPresentationOptions.alert)
            
        }else{
            
            
            let contoller =  Utilities.getTopViewController()
            if contoller is MessageViewController{
                let chatDetail = getChatDetailData(userInfo:notification.request.content.userInfo)
                if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId || chatDetail.messageThreadId == "Default"{
                    postNotification(chatDetail: chatDetail, check: true)
                }else{
                    completionHandler(UNNotificationPresentationOptions.alert)
                }
            }else{
                completionHandler(UNNotificationPresentationOptions.alert)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let rideSpecificFlag = getRideSpecificDetail(userInfo: response.notification.request.content.userInfo)
        if rideSpecificFlag{
            
            let rideId =  getRideId(userInfo: response.notification.request.content.userInfo)
            DataContainer.sharedInstance.rideSpecificId = rideId
            
            let contoller =  Utilities.getTopViewController()
            if contoller is RideDetailViewController{
                DataContainer.sharedInstance.reloadRideDetail = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }else{
                redirectToRideDetailViewController()
            }
             DataContainer.sharedInstance.refreshGPSTracking = true
            
        }else{
            
            let chatDetail = getChatDetailData(userInfo: response.notification.request.content.userInfo)
            let contoller =  Utilities.getTopViewController()
            if contoller is MessageViewController{
                if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId{
                    postNotification(chatDetail: chatDetail, check: false)
                }else{
                    //Reload chat
                    postNotification(chatDetail: chatDetail, check: false)
                }
            }else{
                redirectToMessageViewController(chatDetail: chatDetail)
            }
        }
    }
}

func getRideSpecificDetail(userInfo: [AnyHashable: Any])->Bool{
    let message = userInfo["rideSpecific"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    print("rideSpecific...\(json.bool)")
    return json.bool!
}

func getRideId(userInfo: [AnyHashable: Any])->Int{
    let message = userInfo["rideId"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    print("rideSpecific...\(json.int)")
    return json.int!
}


func getChatDetailData(userInfo: [AnyHashable: Any]) -> ChatModel {
    let message = userInfo["message"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    return ChatModel(json: json)
}

func postNotification(chatDetail: ChatModel, check: Bool){
    let dict:[String: Any] = ["chatDetail": chatDetail,"check":check]
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FCMNotification"), object: nil, userInfo: dict)
}


func redirectToMessageViewController(chatDetail: ChatModel){
    let navController = Utilities.getCurrentNavigationController()
    let messageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
    messageViewController.messageThreadId = chatDetail.messageThreadId
    messageViewController.passengerName = chatDetail.personName
    messageViewController.passengerId = chatDetail.personId
    navController.pushViewController(messageViewController, animated: true)
}
func redirectToRideDetailViewController(){
    let navController = Utilities.getCurrentNavigationController()
    let rideViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RideDetailViewController") as! RideDetailViewController
    navController.pushViewController(rideViewController, animated: true)
}


// [END ios_10_message_handling]

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}

