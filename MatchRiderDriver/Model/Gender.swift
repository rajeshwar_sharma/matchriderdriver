import SwiftyJSON
import Foundation
enum Gender: String {
  case Female = "F"
  case Male = "M"
  case Unknown
  
  init(json: JSON) {
    let gender = json["gender"].string!
    self = Gender(rawValue: gender) ?? .Unknown
  }
  
}
