//
//  SuperHighwayRoute.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON
struct SuperHighwayRoute {
    
    let id: Int
    let startLocation: String
    let destinationLocation: String
    let startLocationCity: String
    
    let startLocationCityShort: String
    let destinationLocationCity: String
    let destinationLocationCityShort: String
    let startMatchPointId: Int
    
    let destinationMatchPointId: Int
    let matchpoints: [Matchpoint]
    let polyline: MKPolyline
    
    init(json: JSON) {
        self.id = getIntValue(json:json["id"])
        self.startLocation = getStringValue(json: json["startLocation"])
        self.destinationLocation = getStringValue(json: json["destinationLocation"])
        self.startLocationCity = getStringValue(json: json["startLocationCity"])
        
        self.startLocationCityShort = getStringValue(json: json["startLocationCityShort"])
        self.destinationLocationCity = getStringValue(json: json["destinationLocationCity"])
        self.destinationLocationCityShort = getStringValue(json: json["destinationLocationCityShort"])
        self.startMatchPointId = getIntValue(json:json["startMatchPointId"])
        
        self.destinationMatchPointId = getIntValue(json:json["destinationMatchPointId"])
        
        self.matchpoints = json["matchPoints"].array!.map { json in
            return Matchpoint(json: json)
        }
        let rideRoute = json["rideRoute"].array!
        var coordinates = rideRoute.map { json -> CLLocationCoordinate2D in
            let latitude = json["latitude"].double!
            let longitude = json["longitude"].double!
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
    }
    
}
