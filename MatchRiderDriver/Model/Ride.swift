
import Foundation
import SwiftyJSON
import MapKit
class Ride {
  
  let id: Int
  let driver: Driver?
  let car: Car?
  let distance: Double
  let startTime: Date?
  let endTime: Date?
  let currentTime: Date?
  let date: Date?
  let bookedSeats: Int
    
  let price: Int
  let start: Matchpoint?
  let destination: Matchpoint?
  let messageThreadId: String?
    let passBookId: Int!
  
  var passengers: [PassengerListModel]?
  let freeSeats: Int
  let duration: Int
  let polyline: MKPolyline  
    
  init(json: JSON) {
    id = json["id"].int!
    distance = json["distance"].double!
    startTime = json["startTime"].string != nil ? StringToDate(json["startTime"].string!) : nil
        
    endTime = json["endTime"].string != nil ? StringToDate(json["endTime"].string!) : nil
    
    currentTime = json["currentTime"].string != nil ? getCurrentTimeFromTimeString(timeStr: json["currentTime"].string!) : nil
    //getDateFromTimeString
    date = json["date"].string != nil ? StringToDate(json["date"].string!) : nil
    
    car = json["car"] != JSON.null ? Car(json: json["car"]) : nil
    driver = json["driver"] != JSON.null ? Driver(json: json["driver"]) : nil
    price = json["price"].int!
    start = json["start"] != JSON.null ? Matchpoint(json: json["start"]) : nil
    destination = json["destination"] != JSON.null ? Matchpoint(json: json["destination"]) : nil
    messageThreadId = json["messageThreadId"].string
    passBookId = json["passBookId"].int != nil ? json["passBookId"].int! : 0
    
    
    let passengerArr = json["passengers"] != JSON.null ? json["passengers"].array! : []
    
    let passenger = passengerArr.map { json in
        return PassengerListModel(json: json)
    }
    self.passengers = passenger
    freeSeats = getIntValue(json: json["freeSeats"])
    duration = getIntValue(json: json["duration"])
    bookedSeats = getIntValue(json: json["bookedSeats"])
    
    let rideRoute = json["rideRoute"] != JSON.null ? json["rideRoute"].array! : []
    var coordinates = rideRoute.map { json -> CLLocationCoordinate2D in
        let latitude = json["latitude"].double!
        let longitude = json["longitude"].double!
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    self.polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
  }
  
}
