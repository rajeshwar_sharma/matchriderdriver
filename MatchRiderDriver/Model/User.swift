import Foundation
import SwiftyJSON
class User {

    let description: String
    let id: String
   // let personId: Int
    let userNumber: String
    let firstName: String
    let lastName: String
    let email: String
    
    let phone: String
    let deviceId: String
    let password: String
    let token: String
    let ext: String
    let photo: String
    let gender: String
    let isAdmin: Bool!
    
    let totalPassengerKMs: Double
    let totalPointsEarned: Int
    
    
    init(json: JSON) {
        description = getStringValue(json:json["description"])
        id = getStringValue(json:json["id"])
        userNumber = getStringValue(json:json["UserNumber"])
        firstName = getStringValue(json:json["firstName"])
        lastName = getStringValue(json:json["lastName"])
        email = getStringValue(json:json["email"])
        
        phone = getStringValue(json:json["phone"])
        deviceId = getStringValue(json:json["deviceId"])
        password = getStringValue(json:json["password"])
        token = getStringValue(json:json["token"])
        ext = getStringValue(json:json["ext"])
        photo = getStringValue(json:json["photo"])
        gender = getStringValue(json:json["gender"])
       // isAdmin = json["isAdmin"].bool
        
        isAdmin =  json["isAdmin"] != JSON.null ? json["isAdmin"].bool : false
        
        totalPassengerKMs = getDoubleValue(json:json["totalPassengerKMs"])
        totalPointsEarned = getIntValue(json:json["totalPointsEarned"])
        
        //isAdmin = true
    }

    
}


