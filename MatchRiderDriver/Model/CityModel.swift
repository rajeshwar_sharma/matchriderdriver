//
//  CityModel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 23/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//
import SwiftyJSON
import Foundation
class CityModel{
    let cityId: Int
    let cityName: String
    let latitude: Double
    let longitude: Double
    let country: String
    
    init(json:JSON) {
        
        cityId = json["Id"].int!
        cityName = json["CityName"].string!
        latitude = json["Latitude"].double!
        longitude = json["Longitude"].double!
        country = json["Country"].string == nil ? "" : json["Country"].string!
    }
 }
