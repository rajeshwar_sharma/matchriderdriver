
import Foundation
import MapKit
import SwiftyJSON
struct Route {
    
    let commuterSelectionId: Int
    let commuterRouteId: Int
    let servicedCityId: Int
    let personId: Int
    let startLocation: String
    let destinationLocation: String
    let routeDescription: String
    let startLatitude: Double
    let startLongitude: Double
    let destinationLatitude: Double
    let destinationLongitude: Double
    
    init(json: JSON) {
        self.commuterSelectionId = getIntValue(json:json["commuterSelectionId"])
        self.commuterRouteId = getIntValue(json:json["commuterRouteId"])
        self.servicedCityId = getIntValue(json:json["servicedCityId"])
        self.personId = getIntValue(json:json["personId"])
        
        self.startLocation = getStringValue(json: json["startLocation"])
        self.destinationLocation = getStringValue(json: json["destinationLocation"])
        self.routeDescription = getStringValue(json: json["routeDescription"])
        
        self.startLatitude = json["startLatitude"].double!
        self.startLongitude = json["startLongitude"].double!
        self.destinationLatitude = json["destinationLatitude"].double!
        self.destinationLongitude = json["destinationLongitude"].double!
    }
}




