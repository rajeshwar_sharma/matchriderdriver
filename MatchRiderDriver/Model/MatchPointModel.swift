//
//  MatchPointModel.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import SwiftyJSON
class MatchPointModel {
    let locationId: Int
    let locationName: String
    let locationDescription: String
    let fullAddress: String
    let whereToStand: String
    let longitude: Double
    let latitude: Double
    let locationNameWithCity: String
    
    init(json: JSON) {
        locationId = getIntValue(json:json["LocationId"])
        locationName = getStringValue(json:json["LocationName"])
        locationDescription = getStringValue(json:json["LocationDescription"])
        fullAddress = getStringValue(json:json["FullAddress"])
        whereToStand = getStringValue(json:json["WhereToStand"])
        longitude = getDoubleValue(json:json["Longitude"])
        latitude = getDoubleValue(json:json["Latitude"])
        locationNameWithCity = getStringValue(json:json["LocationNameWithCity"])
        
    }
 }


import MapKit

class Annotation: NSObject, MKAnnotation
{
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    var title: String?
    var locationDescription: String?
    var locationAddress: String?
    var locationCity: String?
    var locationId: Int?
    var tag: Int = 0
    var passengers: [PassengerListModel]! = []
    var driverTimeAtStopDate: Date?
    var isStartPoint = false
}
