
import UIKit
import Foundation
//import Stripe
import MapKit
import CoreLocation
import SwiftyJSON
import Firebase 
private let matchriderHost = "https://api.matchrider.de"
 //let matchriderHost = "https://api.matchrider.de"
let googleApiKey = "AIzaSyBh1m5LWl-qV1nVkT1WZeWAzng5eP42RNk"
let googleNavigation = "https://maps.google.com/maps?"
struct Endpoint {
    static let FaceBook = "/api/Login/ValidateFBDriver"
    static let Register = "/api/register/post"
    static let Login = "/api/Login/ValidateDriver"
    static let getAccountDetails = "/api/2.0/Profile/GetAccountDetails"
    static let GoogleGeocoading = "https://maps.googleapis.com/maps/api/geocode/json?"
    static let getDriverActiveRequests = "/api/2.0/upcomingRides/GetPreConfiguredDriverActiveRequests"
    static let updateDriverLocation = "/api/preConfiguredRoute/updateDriverLocation"
    static let getLocationImage = "/api/2.0/image/GetLocationImage/"
    static let getPersonImage = "/api/2.0/image/GetPersonImage/"
    static let getVehicleImage = "/api/2.0/image/GetVehicleImage/"
    static let getMatchPointInfo = "/api/2.0/matchPoints/GetMatchPointDetail/"
    static let driverCancelRide = "/api/rides/DriverCancelRide"
    static let rejectPassengerRequest = "/api/upcomingRides/RejectPassengerRequest"
    static let getPreConfiguredDriveRequestNext = "/api/2.0/upcomingRides/GetPreConfiguredDriveRequestNext"
    static let confirmPassengerRequest = "/api/upcomingRides/ConfirmPassengerRequest"
   static let updateAccountDetails = "/api/Profile/UpdateAccountDetails"
   
    
    
    static let getThreadsByDriver = "/api/2.0/AppMessage/GetThreadsByDriver"
    static let getUnreadThreadCount = "/api/2.0/AppMessage/GetUnreadThreadCount"
    static let getNotRatedRideCount = "/api/2.0/AppMessage/GetNotRatedRideCountByDriver"
    static let getTotalDriverNotificationsCount = "/api/2.0/AppMessage/GetTotalDriverNotificationsCount"
    
    
    
    static let getMessagesByThread = "/api/2.0/AppMessage/GetMessagesByThread"
    static let getRideDetails = "/api/2.0/Rides/GetRideDetails"
    static let updateDeviceFCMToken = "/api/FCMMessage/UpdateDevice"
    static let getDriverRidesNotRated = "/api/2.0/Rides/GetDriverRidesNotRated"
    static let updateDriverRideRating = "/api/PreConfiguredRoute/UpdateDriverRideRating"
    static let getDriverRideDetails = "/api/2.0/Rides/GetDriverRideDetails/"
    //static let sendMessageToAll = "/api/FCMMessage/SendMessageToAll"
    static let sendMessage = "/api/FCMMessage/SendMessage"
    static let getMessageThreadId = "/api/AppMessage/GetMessageThreadId"
    static let updateMessageViewDate = "/api/FCMMessage/UpdateMessageViewDate"
    static let getCommuteHolidays = "/api/2.0/CommuteHoliday/GetCommuteHolidays"
    static let insertUpdateCommuteHoliday = "/api/CommuteHoliday/InsertUpdateCommuteHoliday"
    static let deleteCommuteHoliday = "/api/CommuteHoliday/DeleteCommuteHoliday?commuteHolidayId="
    static let enquiryPost = "/api/enquiry/post"
    static let getUserTokenForAdmin = "/api/Login/GetUserTokenForAdmin"
    static let getDriverCarDetails = "/api/2.0/Vehicle/GetDriverCarDetails"
    static let updateDriverVehicleDetails = "/api/Vehicle/UpdateDriverVehicleDetails"
    static let getDriveriOSAppVersion = "/api/2.0/Version/GetDriveriOSAppVersion"
    
    static let registerDriver = "/api/register/registerDriver"
    static let getCommuterRoutes = "/api/2.0/Register/GetCommuterRoutes"
    static let insertCommuterSelection = "/api/register/insertCommuterSelection"
    static let getServicedCities = "/api/2.0/PreConfiguredRoute/GetServicedCities"
    //static let getPreconfiguredHighways = "/api/2.0/PreconfiguredHighway/GetPreconfiguredHighways"
    
    static let getPreconfiguredHighwaysWithCustomization = "/api/2.0/PreconfiguredHighway/GetPreconfiguredHighwaysWithCustomization"
    

    
    static let getPreconfiguredDriverCustomization = "/api/2.0/PreconfiguredHighway/GetPreconfiguredDriverCustomization/"
    static let getPreconfiguredOppositeHighway = "/api/2.0/PreconfiguredHighway/GetPreconfiguredOppositeHighway/"
    
    static let addUpdatePreconfiguredRouteCustomization = "/api/PreconfiguredHighway/AddUpdatePreconfiguredRouteCustomization"
    static let addPreconfiguredRouteRide = "/api/PreconfiguredHighway/AddPreconfiguredRouteRide"
    static let getPreconfiguredRoute = "/api/2.0/PreConfiguredRoute/GetPreconfiguredRoute/"
    
   

    
    
}
class HttpRequest {
 
  static let sharedRequest = HttpRequest()
  
  //fileprivate var cachedImages: [String: UIImage] = [:]
     var cachedImages: [String: UIImage] = [:]

  func imagesWithURL(_ url: String) -> UIImage? {
    return cachedImages[url]
  }
  

    
    func parseData(data:Data)->JSON{
    return JSON(data: data)
    }
   
    func performPostRequest(_ token: Token? = nil, requestParam: [String:Any],endPoint: String, callback: @escaping (_ payload: Data?) -> Void){
        
        JSONRequest(matchriderHost, method: .Post, endPoint: endPoint,headerParams: token?.params, bodyParams: requestParam as [String : AnyObject]?) { payload in
            guard let payload = payload else {
                callback(nil)
                return
            }
        callback(payload)
        }
    }
    
    func performGetRequest(_ token: Token? = nil, endPoint: String, callback: @escaping (_ payload: Data?) -> Void){
        
        JSONRequest(matchriderHost, method: .Get, endPoint: endPoint,headerParams: token?.params) { payload in
            guard let payload = payload else {
                callback(nil)
                return
            }
            callback(payload)
        }
    }
    func performDeleteRequest(_ token: Token? = nil, endPoint: String, callback: @escaping (_ payload: Data?) -> Void){
        
        JSONRequest(matchriderHost, method: .Delete, endPoint: endPoint,headerParams: token?.params) { payload in
            guard let payload = payload else {
                callback(nil)
                return
            }
            callback(payload)
        }
    }

    
    
    
    
    func requestGoogleApi(_ url: String, callback: @escaping (_ payload: [JSON]?, _ error: JSON?) -> Void){
        
        let url: NSString = url as NSString
        let urlStr : NSString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = "POST"
        let session = URLSession.shared
        session.dataTask(with: request) {data, response, err in
            
            guard let data = data else {
                callback(nil, nil)
                return
            }
            let json = JSON(data: data)
            print(json)
            let payload = json["results"].array!
            switch json["status"].string! {
            case HttpRequest.Status.Ok:
                callback(payload, nil)
            case HttpRequest.Status.Failure:
                callback(nil, payload.first)
            default:
                callback(nil, nil)
            }

            
            }.resume()
    }
    
    
   func image(_ token: Token? = nil, url: String, useCache: Bool = true, callback: @escaping (_ image: UIImage?) -> Void) {
    let components = [
      matchriderHost,
      url
    ]
    let endpoint = NSString.path(withComponents: components)
    if useCache {
      if let image = cachedImages[url] {
        callback(image)
        return
      }
    }
    RawRequest(endpoint, method: .Get,headerParams: token?.params) { data in
      guard let data = data else {
        callback(nil)
        return
      }
      let image = UIImage(data: data)
      self.cachedImages[url] = image
      callback(image)
    }
  }
  
  struct Status {
    static let Success = "Success"
    static let Ok = "OK"
    static let Failure = "Failure"
  }
  
  enum Method: String {
    case Post = "POST"
    case Get = "GET"
    case Delete = "DELETE"
  }
  
    
}

private func RawRequest(_ host: String, method: HttpRequest.Method, endPoint:String = "", headerParams: [String: String]? = nil, bodyParams: [String:AnyObject]? = nil, callback: @escaping (_ payload: Data?) -> Void) {
  guard Reachability.isConnectedToNetwork() else {
    let controller = ActionControllerNoNetwork()
    let delegate = UIApplication.shared.delegate as! AppDelegate
   // delegate.mainNavigation.topViewController!.present(controller, animated: true, completion: nil)
    return
  }
  let serverAddress = host + endPoint
  let url = URL(string: serverAddress)!
  //let request = NSMutableURLRequest(url: url)
    
    var request = URLRequest(url: URL(string: serverAddress)!)
    request.httpMethod = "POST"
    
    
  request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
  request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Accept")
  if let headerParams = headerParams {
    for (key, value) in headerParams {
      request.addValue(value, forHTTPHeaderField: key)
    }
  }
  if let bodyParams = bodyParams {
    let data = try! JSONSerialization.data(withJSONObject: bodyParams, options: [])
    let dataString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
    request.httpBody = dataString!.data(using: String.Encoding.utf8.rawValue)
  }
  request.httpMethod = method.rawValue
  let config = URLSessionConfiguration.default
  let queue = OperationQueue.main
  let session = URLSession(configuration: config, delegate: nil, delegateQueue: queue)
  UIApplication.shared.isNetworkActivityIndicatorVisible = true
  
    let task = session.dataTask(with: request, completionHandler: { data, response, error in
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
    if let httpResponse = response as? HTTPURLResponse {
        if httpResponse.statusCode == 401{
            Store.sharedStore.logout()
            return
        }
    }
        
    guard error == nil else {
      callback(nil)
      return
    }
    guard let data = data else {
      callback(nil)
      return
    }
    guard response != nil else {
      callback(nil)
      return
    }
   
    callback(data)
  })
  
  task.resume()
}
//func logout() {
//    Store.sharedStore.reset()
//    FBSDKLoginManager().logOut()
//    let delegate = UIApplication.shared.delegate as! AppDelegate
//    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
//    delegate.mainNavigation.setViewControllers([controller], animated: false)
//    delegate.mainNavigation.dismiss(animated: true, completion: nil)
//}

private func JSONRequest(_ host: String, method: HttpRequest.Method, endPoint:String, headerParams: [String: String]? = nil, bodyParams: [String:AnyObject]? = nil, bodyString: String? = nil, callback: @escaping (_ payload: Data?) -> Void) {
  RawRequest(host, method: method, endPoint: endPoint, headerParams: headerParams, bodyParams: bodyParams) { payload in
    guard let data = payload else {
      callback(nil)
      return
    }
    
//    do {
//        if let dictionaryOK = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
//            // parse JSON
//            print(dictionaryOK)
//        }
//    } catch {
//        print(error)
//    }
    //let json = JSON(data: data)
    callback(data)
   
    
//    let payload = json["Payload"].array!
//    switch json["Status"].string! {
//    case HttpRequest.Status.Success:
//      callback(payload, nil)
//    case HttpRequest.Status.Failure:
//      callback(nil, payload.first)
//    default:
//      callback(nil, nil)
//    }
  }
}

import SystemConfiguration

open class Reachability {
  
  class func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
//    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//      SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
//    }
  
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
      return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
  }
  
}

enum ResponseError: String {
  
  case InvalidResponse = "invalid_response"
  case MissingBankAccount = "missing_bank_account"
  
}


func getStringValue(json: JSON)->String!{
   let value =  json != JSON.null ? json.string : ""
    return value
}
func getIntValue(json: JSON)->Int!{
    let value =  json != JSON.null ? json.int : 0
    return value
}
func getDoubleValue(json: JSON)->Double!{
    let value =  json != JSON.null ? json.double : 0
    return value
}

func requestRoute(_ from: CLLocationCoordinate2D, to: CLLocationCoordinate2D, callback: @escaping (_ route: MKRoute) -> Void) {
    let directionsRequest = MKDirectionsRequest()
    let startPlacemark = MKPlacemark(coordinate: from, addressDictionary: nil)
    let destinationPlacemark = MKPlacemark(coordinate: to, addressDictionary: nil)
    directionsRequest.source = MKMapItem(placemark: startPlacemark)
    directionsRequest.destination = MKMapItem(placemark: destinationPlacemark)
    directionsRequest.transportType = .automobile
    MKDirections(request: directionsRequest).calculate { response, error in
        guard let response = response else {
            return
        }
        guard let route = response.routes.first else {
            return
        }
        callback(route)
    }
}

func registerFCMDeviceTokenToServer(){
    if let deviceTokenId = FIRInstanceID.instanceID().token() {
        let params: [String : Any] = [
            "deviceId": deviceTokenId
        ]
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDeviceFCMToken) { payload in
            guard let data = payload else{
                //self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                //let token = json["Payload"].array?.first
                
            }
            
            
        }
    }
}

