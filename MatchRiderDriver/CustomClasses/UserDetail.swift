//
//  UserDetail.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class UserDetail: NSObject {
    var image: UIImage!
    var aboutDescription: String!
    static let sharedInstance = UserDetail()
}
