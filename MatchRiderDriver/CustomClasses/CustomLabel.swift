//
//  CustomLabel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 06/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
class CustomLabel: UILabel {
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)))
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
}
