//
//  Validation.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
class Validation{
    
    class func validateSignIn(_ email: String, password: String)->Bool{
        if email.trim().isEmpty{
            showErrorMessage(message: ENTER_EMAIL_ID)
            return false
        }
        if !email.trim().isEmpty{
            if !Validation.isValidEmail(email: email.trim()){
             showErrorMessage(message: ENTER_VALID_EMAIL_ID)
             return false
            }
           
        }
        if password.trim().isEmpty{
            showErrorMessage(message:ENTER_PASSWORD)
            return false
        }
        return true
    }
    
    
    class func validateVehicleDetail(_ carDetail: Car!)->Bool{
        if carDetail.photoCar == nil{
            //showErrorMessage(message: ENTER_EMAIL_ID)
            return false
        }
        if carDetail.make.isEmpty{
           
        }
        if carDetail.model.isEmpty{
            //showErrorMessage(message:ENTER_PASSWORD)
            return false
        }
        if carDetail.license.isEmpty{
            //showErrorMessage(message:ENTER_PASSWORD)
            return false
        }
        if carDetail.seats == 0{
            showErrorMessage(message:ENTER_PASSWORD)
            return false
        }
        return true
    }
    
    
    class func validateSignUp(_ firstName: String, secondName: String, email: String, password: String, confirmPassword: String,termsCondition: Bool)->Bool{
        
        
        var check: Bool = true
        var msg: String = ""
        
        if firstName.trim().isEmpty && check{
//            showErrorMessage(message: ENTER_FIRST_NAME)
//            return false
            check = false
            msg = ENTER_FIRST_NAME
        }
        if secondName.trim().isEmpty && check{
//            showErrorMessage(message: ENTER_LAST_NAME)
//            return false
            check = false
            msg = ENTER_LAST_NAME
        }
        if email.trim().isEmpty && check{
//            showErrorMessage(message: ENTER_EMAIL_ID)
//            return false
            check = false
            msg = ENTER_EMAIL_ID
        }
       if !email.trim().isEmpty && check{
            if !Validation.isValidEmail(email: email.trim()){
//                showErrorMessage(message: ENTER_VALID_EMAIL_ID)
//                return false
                check = false
                msg = ENTER_VALID_EMAIL_ID
            }
            
        }
        if password.trim().isEmpty && check{
//            showErrorMessage(message:ENTER_PASSWORD)
//            return false
            check = false
            msg = ENTER_PASSWORD
        }
       if confirmPassword.trim().isEmpty && check{
//            showErrorMessage(message:CONFIRM_PASSWORD)
//            return false
            check = false
            msg = CONFIRM_PASSWORD
        }
        
       if !password.trim().isEmpty && !confirmPassword.trim().isEmpty && check{
            if password != confirmPassword{
//                showErrorMessage(message:PASSWORD_MISMATCH)
//                return false
                check = false
                msg = PASSWORD_MISMATCH
            }
        }
     
        if !termsCondition && check{
//            showErrorMessage(message:ENTER_TERMS_CONDITION)
//            return false
            check = false
            msg = ENTER_TERMS_CONDITION
        }
        
        if check{
            return true
        } else{
            let vc = Utilities.getTopViewController()
            let controller = ActionControllerCommonAlert(msg)
            vc.present(controller, animated: true, completion: nil)
            return false
        }
        
        
    }
    
    class func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    /**
     This method Show the Error Message
     */
    class func showErrorMessage(message:String)
    {
        let vc : UIViewController = Utilities.getTopViewController()
        vc.showErrorMessage(message: message)
    }

}
