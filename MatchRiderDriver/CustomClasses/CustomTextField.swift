//
//  CustomTextField.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

   
    required init?(coder aDecoder: NSCoder){
        
        super.init(coder: aDecoder)
        //self.backgroundColor = ColorFromHexaCode(0xF1F1F1)
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        leftPadding(width: 6)
    }
    func leftPadding(width : CGFloat){
        
        //CGRectMake(0, 0, width, self.frame.size.height)
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: width, height: self.frame.size.height))
        self.leftView=paddingView
        //paddingView.backgroundColor = UIColor.redColor()
        self.leftViewMode = UITextFieldViewMode.always
        
    }
    func addRightImage(){
        self.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "ic_sortdown")
        imageView.image = image
        self.rightView = imageView
    }
    
}
