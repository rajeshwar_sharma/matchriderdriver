//
//  RideInfoViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 04/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class RideInfoViewController: UIViewController {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var bookingLabel: UILabel!
    @IBOutlet var startImgView: UIImageView!
    @IBOutlet var destinationImgView: UIImageView!
    @IBOutlet var startPonitLabel: UILabel!
    @IBOutlet var endPointLabel: UILabel!
    
    var nextRide: Ride!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Naechste Fahrt"
        setBackButton()
        dateLabel.text = getCompleteDateFromDate(date: nextRide.startTime!)
        bookingLabel.text = "Buchungen: \(nextRide.bookedSeats)"
        var imgPath = "\(Endpoint.getLocationImage)\(nextRide.start!.id)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.startImgView.image = image
        }
        imgPath = "\(Endpoint.getLocationImage)\(nextRide.destination!.id)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.destinationImgView.image = image
        }
        startPonitLabel.text = nextRide.start!.city
        endPointLabel.text = nextRide.destination!.city
    }
    
    //MARK:- Custom Method
    
    func setBackButton()
    {
        let buttonImage = UIImage(named: "ic_cancel_white");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.custom)
        leftBarButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        leftBarButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        leftBarButton.setImage(buttonImage, for: UIControlState.normal)
        leftBarButton.addTarget(self, action: #selector(MatchpointDetailController.backBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false);
    }
    
    //MARK:- Custom method action
    
    func backBtnClicked(sender: UIButton!)
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
