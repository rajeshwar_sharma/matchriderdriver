//
//  ThanksViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 21/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class ThanksViewController: UIViewController {
    
    @IBOutlet var messageLabel: UILabel!
    
    var preConfiguredRouteId: Int!
    var earnedPoints: Int!
    var networkViewObj: NetworkView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkNetworkAvailabilty()
    }
    
    //MARK:- Custom method
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            getPreconfiguredDriverCustomization()
        }
        networkViewObj?.delegate = self
    }
    
    func showMessage(){
        let user = Utilities.getSignInUser()
        let highLightStr1 = user!.firstName
        let highLightStr2 = "\(earnedPoints!)"
        let highLightStr3 = "Punkte"
        let msgStr = "Glückwunsch \(highLightStr1), du hast mit dieser Fahrt \(highLightStr2) Punkte verdient! Die Punkte wurden deinem Konto zugefügt."
        let range1 = (msgStr as NSString).range(of: highLightStr1)
        let range2 = (msgStr as NSString).range(of: highLightStr2)
        let range3 = (msgStr as NSString).range(of: highLightStr3)
        let myMutableString = NSMutableAttributedString(string: msgStr)
        
        myMutableString.setAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 24)
            , NSForegroundColorAttributeName : hexStringToUIColor(hex: MatchRiderColor)], range: range1)
        
        myMutableString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 24)
            , NSForegroundColorAttributeName : hexStringToUIColor(hex: "659966")], range: range2)
        
        myMutableString.setAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 26)
            , NSForegroundColorAttributeName : hexStringToUIColor(hex: MatchRiderColor)], range: range3)
        messageLabel.attributedText = myMutableString
    }
    
    //MARK:- Custom method action
    
    @IBAction func returnButtonAction(_ sender: Any) {
        let navControl = Utilities.getCurrentNavigationController()
        self.dismiss(animated: true, completion: nil)
        DataContainer.sharedInstance.commonModel.removeAll()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpCommingTripViewController") as! UpCommingTripViewController
        navControl.setViewControllers([vc], animated: false)
    }
    
    //MARK:- Service Api calling
    
    func getPreconfiguredDriverCustomization(){
        self.view.showLoader()
        let components = [
            Endpoint.getPreconfiguredDriverCustomization,
            "\(preConfiguredRouteId!)"
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let superHighways = payload.map { json in
                    return SuperHighway(json: json)
                }
                let superHighway = superHighways.first
                self.earnedPoints = superHighway!.pointsEarned
                self.showMessage()
            }
        }
    }
    
}

extension ThanksViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}
