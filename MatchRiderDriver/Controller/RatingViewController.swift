//
//  RatingViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 19/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
import Cosmos

class RatingViewController: BaseViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var userImageView: IconView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var halfCosmosView: CosmosView!
    
    var ride: Ride!
    let thanksLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bewertung abgeben"
        setBackButton()
        thanksLabel.font = UIFont.systemFont(ofSize: 30, weight: UIFontWeightBold)
        thanksLabel.frame = CGRect(x: 0, y: 120, width: screenWidth, height: 40)
        thanksLabel.textColor = hexStringToUIColor(hex: "3C3D3C")
        thanksLabel.isHidden = true
        thanksLabel.textAlignment = .center
        thanksLabel.text = "Danke!"
        self.view.addSubview(thanksLabel)
        halfCosmosView.settings.fillMode = .full
        halfCosmosView.didFinishTouchingCosmos = { rating in
            print(Float(rating))
        }
        halfCosmosView.didTouchCosmos = { rating in
            print(Float(rating))
            self.halfCosmosView.rating = rating
        }
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: ride.passengers!.first!.photo, useCache: true) { image in
            self.userImageView.image = image
        }
        descriptionLabel.text = "Wie war deine Fahrt mit \(ride.passengers!.first!.firstName)?"
    }
    
    fileprivate func updateRideRating(_ token: Token,rideRating:Int) {
       self.view.showLoader()
       let params = [
            "passBookId": "\(ride.passengers!.first!.passBookId)",
            "rideRating": "\(rideRating)"
        ]
        HttpRequest.sharedRequest.performPostRequest( Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDriverRideRating) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                self.contentView.isHidden = true
                self.thanksLabel.isHidden = false
                self.navigateToBackViewController()
            }
            self.view.hideLoader()
        }
    }

    func navigateToBackViewController(){
        let when = DispatchTime.now() + 1.1 // change 1.1 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
           _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
  @IBAction func sendButtonAction(_ sender: AnyObject) {
        if Int(halfCosmosView.rating) > 0{
          updateRideRating(Store.sharedStore.token!, rideRating: Int(halfCosmosView.rating))
        }
    }
 }
