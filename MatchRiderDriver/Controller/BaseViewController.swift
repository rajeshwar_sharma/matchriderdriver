//
//  BaseViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftMenuButton()
    }

    // *********** Method to set left menu button **********
    func setLeftMenuButton()
    {
        let buttonImage = UIImage(named: "icn_menu.png");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.custom)
        leftBarButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftBarButton.setImage(buttonImage, for: UIControlState.normal)
        leftBarButton.addTarget(self, action: #selector(BaseViewController.leftMenuBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false);
        
        }
    
    func leftMenuBtnClicked(sender: UIButton!)
    {
        Utilities.toggelMenuToLeft()
    }
       
    func setBackButton()
    {
        let buttonImage = UIImage(named: "ic_back_white");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.custom)
        leftBarButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        leftBarButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        leftBarButton.setImage(buttonImage, for: UIControlState.normal)
        leftBarButton.addTarget(self, action: #selector(BaseViewController.backBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false);
    }
    
    func backBtnClicked(sender: UIButton!)
    {
        Utilities.setSideMenuPanModeToDefault()
       _ = self.navigationController?.popViewController(animated: true)
    }

}
