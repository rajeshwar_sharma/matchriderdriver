//
//  MatchPointViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 08/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
class MatchPointViewController: BaseViewController {
   
    @IBOutlet var passengerImgView: UIImageView!
    @IBOutlet var passengerName: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var startImgView: UIImageView!
    @IBOutlet var startLocationDescription: UILabel!
    @IBOutlet var startFullAddress: UILabel!
    @IBOutlet var startWhereToStand: UILabel!
    @IBOutlet var destImgView: UIImageView!
    @IBOutlet var destLocationDescription: UILabel!
    @IBOutlet var destFullAddressLabel: UILabel!
    @IBOutlet var destWhereToStand: UILabel!
    @IBOutlet var arrowImgView: UIImageView!
    
    var passenger: PassengerListModel!
    var startMatchPoint: MatchPointModel!
    var destMatchPoint: MatchPointModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        self.title = "Dein Mitfahrer"
        startImgView.contentMode = .scaleAspectFill
        startImgView.clipsToBounds = true
        destImgView.contentMode = .scaleAspectFill
        destImgView.clipsToBounds = true
        
        arrowImgView.image = arrowImgView.image!.withRenderingMode(.alwaysTemplate)
        arrowImgView.tintColor = hexStringToUIColor(hex: MatchRiderColor)
        passengerImgView.setCircleView()
        let imgPath = "\(Endpoint.getPersonImage)\(passenger.userId)/l"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.passengerImgView.image = image
        }
        passengerName.text = passenger.firstName
         timeLabel.text = "Abholzeit: \(passenger.driverTimeAtStop.replacingOccurrences(of: "-", with: " "))"
        getStartMatchPointInfo()
        getDestinationMatchPointInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        let viewController = appDelegate.window!.rootViewController
        viewController?.menuContainerViewController.panMode = MFSideMenuPanModeNone

    }
    
    func getStartMatchPointInfo(){
        
        self.view.showLoader()
        let endPoint = "\(Endpoint.getMatchPointInfo)\(passenger.start!.id)"
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token,endPoint:endPoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
               let dataDic = json["Payload"].array?.first
                let matchPoint = MatchPointModel(json: dataDic!)
                self.startMatchPoint = matchPoint
                self.setStartMatchPointData()
            }
            self.view.hideLoader()
        }
    }
    
    func getDestinationMatchPointInfo(){
        
        self.view.showLoader()
        let endPoint = "\(Endpoint.getMatchPointInfo)\(passenger.destination!.id)"
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token,endPoint:endPoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataDic = json["Payload"].array?.first
                let matchPoint = MatchPointModel(json: dataDic!)
                self.destMatchPoint = matchPoint
                self.setDestMatchPointData()
            }
            self.view.hideLoader()
        }
    }
    
    @IBAction func destMatchPointImageViewTapAction(_ sender: AnyObject) {
        if destMatchPoint != nil{
        navigateToMatchPointMapViewController(matchPoint: destMatchPoint)
        }
    }
    
    func setStartMatchPointData(){
        let imgPath = "\(Endpoint.getLocationImage)\(startMatchPoint.locationId)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.startImgView.image = image
        }
        startLocationDescription.text = startMatchPoint.locationDescription
        startFullAddress.text = startMatchPoint.fullAddress
        startWhereToStand.text = startMatchPoint.whereToStand
    }
    
    func setDestMatchPointData(){
        let imgPath = "\(Endpoint.getLocationImage)\(destMatchPoint.locationId)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.destImgView.image = image
        }
        destLocationDescription.text = destMatchPoint.locationDescription
        destFullAddressLabel.text = destMatchPoint.fullAddress
        destWhereToStand.text = destMatchPoint.whereToStand
    }
    
    @IBAction func startMatchPointImageViewTapAction(_ sender: AnyObject) {
        if startMatchPoint != nil{
        navigateToMatchPointMapViewController(matchPoint: startMatchPoint)
        }
    }
    
    func navigateToMatchPointMapViewController(matchPoint: MatchPointModel){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPointMapViewController") as! MatchPointMapViewController
        vc.matchPoint = matchPoint
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
}
