//
//  ContactViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MessageUI
class ContactViewController: BaseViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet var contactLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var textView: UITextView!
    let user = Utilities.getSignInUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Match Rider kontaktieren"
        setBackButton()
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func submitButtonAction(_ sender: AnyObject) {
        enquiryPost()
    }
    
    @IBAction func contactTapAction(_ sender: AnyObject) {
        Utilities.makeCall(number: contactLabel.text!)
    }
    
    @IBAction func emailTapAction(_ sender: AnyObject) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailLabel.text!])
            present(mail, animated: true)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    func enquiryPost(){
        if textView.text.trim().isEmpty{
            return
        }
        let params = [
            "Email": user!.email,
            "Message": textView.text!
            
        ]
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.enquiryPost) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
                self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json.bool
            if status!{
                //self.showSuccessMessage(message: EnquirySuccess)
                self.showSuccessMessageWithTitle(title: .danke, message: EnquirySuccess)
                self.textView.text = ""
            }else{
                self.showErrorMessage(message: EnquiryFailed)
            }
            print(json)
        }
    }
}

