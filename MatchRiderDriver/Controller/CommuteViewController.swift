//
//  CommuteViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 10/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CommuteViewController: BaseViewController {
    
    @IBOutlet var descriptionLabel: FRHyperLabel!
    @IBOutlet var regionLabel: UILabel!
    @IBOutlet var routeLabel: UILabel!
    @IBOutlet var submitButton: UIButton!
    
    var cityModel: [CityModel] = []
    var routeModel: [Route] = []
    var selectedRoute: Route?
    var selectedCity: CityModel?
    var bottomSheetView: BottomSheetView!

    @IBOutlet var skipButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = DriverRegistration
        getCityList()
        setDescription()
        userDefault.removeObject(forKey: "CityId")
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: skipButton.frame.height - 1, width: skipButton.frame.width, height: 1)
        bottomLine.backgroundColor = hexStringToUIColor(hex: MatchRiderColor).cgColor
        skipButton.layer.addSublayer(bottomLine)
        setSubmitButton(enable: false, alpha: 0.5)
    }
    
    //MARK:- Custom method
    
    /*** Method to set description and make it clickabel ***/
    func setDescription(){
        let string = "Du pendelst auf einer unserer Routen? Dann lass dein Auto für dich arbeiten und teile deine Fahrt mit anderen Pendlern."
        let attributes = [NSForegroundColorAttributeName: UIColor.darkGray,
                          NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        descriptionLabel.attributedText = NSAttributedString(string: string, attributes: attributes)
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if let url = URL(string: "http://www.matchridergo.de/index.php/unsere-routen-hd/") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:])
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        descriptionLabel.font = UIFont.systemFont(ofSize: 13)
        descriptionLabel.setLinksForSubstrings(["unserer Routen?"], withLinkHandler: handler)
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
        Utilities.navigateToViewController(viewController: "GPSTrackingViewController")
    }
    
    func setSubmitButton(enable: Bool, alpha: CGFloat){
        submitButton.isEnabled = enable
        submitButton.alpha = alpha
    }
    
    //MARK:- Api calling
    
    func getCityList(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getServicedCities) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let cityList = payload.map { json in
                    return CityModel(json: json)
                }
                self.cityModel = cityList
                Store.sharedStore.cityList = cityList
            }
            
        }
    }
    
    func getCommuteRouteList(city: CityModel){
        
        let components = [
            Endpoint.getCommuterRoutes,
            "\(city.cityId)"
        ]
        let endpoint = NSString.path(withComponents: components)
        
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let route = payload.map { json in
                    return Route(json: json)
                }
                self.routeModel = route
            }else{
                self.showErrorMessage(message: NoRoutesAvailableForSelectedRegion)
            }
        }
    }
    //*********** Add bottom sheet to show Available Routes and Cities ******
    func showBottomSheetView(){
        if bottomSheetView == nil {
            bottomSheetView = Bundle.main.loadNibNamed("BottomSheetView", owner: self, options: nil)?.first as! BottomSheetView
            bottomSheetView.isHidenFlag = true
        }
        if bottomSheetView.isHidenFlag!{
            bottomSheetView.isHidenFlag = false
            bottomSheetView.frame = self.view.bounds
            self.view.addSubview(bottomSheetView)
            bottomSheetView.delegate = self
            bottomSheetView.hideFilterButton = true
            
        }
    }
    
    
    @IBAction func regionTapGestureAction(_ sender: Any) {
        if cityModel.count == 0{
            getCityList()
        }else{
        showBottomSheetView()
        bottomSheetView.setData(city: cityModel)
        }
    }
    
    @IBAction func routeTapGestureAction(_ sender: Any) {
        if routeModel.count == 0{
            //self.showErrorMessage(message: PleaseSelectRegionFirst)
            let controller = ActionControllerCommonAlert(PleaseSelectRegionFirst)
            self.present(controller, animated: true, completion: nil)
            return
        }
        showBottomSheetView()
        bottomSheetView.setData(routes: routeModel)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        if selectedRoute == nil{
            return
        }
        let params = [
            "commuterRouteId": selectedRoute!.commuterRouteId
        ]
        print(params)
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.insertCommuterSelection) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                self.showSuccessMessage(message:RoutePreferenceUpdatedSuccessfully)
                Utilities.navigateToViewController(viewController: "GPSTrackingViewController")
            }else{
                
            }
        }
    }
}

extension CommuteViewController: BottomSheetViewDelegate{
    
    func filterButtonAction() {
    }
    
    func getSelected(route: Route?, city: CityModel?) {
        
        if route != nil{
            selectedRoute = route
            setSubmitButton(enable: true, alpha: 1.0)
            routeLabel.text = RouteDirectionString(route!.startLocation, to: route!.destinationLocation)
        } else{
            
            userDefault.set(city!.cityId, forKey: "CityId")
            regionLabel.text = city!.cityName
            selectedRoute = nil
            setSubmitButton(enable: false, alpha: 0.5)
            routeLabel.text = "Route auswählen"
            getCommuteRouteList(city: city!)
        }
    }
}
