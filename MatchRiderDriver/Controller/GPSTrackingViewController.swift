//
//  GPSTrackingViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Mixpanel
import Crashlytics

class GPSTrackingViewController: BaseViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var rideInfoView: UIView!
    @IBOutlet var rideInfoImgView: UIImageView!
    @IBOutlet var gpsImgview: UIImageView!
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var carContentView: UIView!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationImgView: UIImageView!
    @IBOutlet var rideListView: UIView!
    
    @IBOutlet var offerRideButton: UIButton!
    
    @IBOutlet var startTrackingButton: UIButton!
    
    var locationManager: CLLocationManager!
    var currentLocation : CLLocation?
    var trackingMapView: GPSTrackingMapView!
    var bottomMenuBar: BottomMenuBar?
    var networkViewObj: NetworkView!
    var impersonateField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.setCircleView()
        Mixpanel.mainInstance().track(event: "Tracked Event!")
        Mixpanel.mainInstance().track(event: "Driver tracking!")
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
      
         locationManager.requestWhenInUseAuthorization()
        
        rideInfoView.isHidden = true
        //rideListView.setCircleView()
        navigationView.setCircleView()
        navigationImgView.transform = navigationImgView.transform.rotated(by: CGFloat(Double.pi/2-45))
        navigationView.layer.shadowColor = UIColor.black.cgColor
        navigationView.layer.shadowOpacity = 0.7
        navigationView.layer.shadowOffset = CGSize(width: 3, height: 3)
        navigationView.layer.shadowRadius = 4.0
        navigationView.clipsToBounds = true
        
        
        startTrackingButton.layer.shadowColor = UIColor.black.cgColor
        startTrackingButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        startTrackingButton.layer.shadowOpacity = 0.5
        startTrackingButton.layer.shadowRadius = 3
        startTrackingButton.clipsToBounds = false
        
        
        Utilities.setSideMenuPanModeToDefault()
        self.navigationController?.isNavigationBarHidden = false
        rideInfoView.setCircleView()
        rideInfoImgView.changeImageColor(color: UIColor.white)
        registerFCMDeviceTokenToServer()
        getPassengeriOSAppVersion()
        //offerRideButton.showsTouchWhenHighlighted = true
        
        
        if DataContainer.sharedInstance.refreshGPSTracking {
            DataContainer.sharedInstance.refreshGPSTracking = false
            appDelegate.nextRide = nil
            setBottomViewFunctionality()
            callApiToGetNextRide()
        }else{
            checkIfTrackingIsEnabled()
        }
        if appDelegate.carDetail == nil{
            getDriverCarDetails()
        }
        
    }
    
    func setColorView(){
        let colorPickerView =  Bundle.main.loadNibNamed("ColorPickerView", owner: nil, options: nil)?[0] as! ColorPickerView
        colorPickerView.frame = self.view.bounds
        colorPickerView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.navigationController?.view.addSubview(colorPickerView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if DataContainer.sharedInstance.refreshGPSTracking {
//            DataContainer.sharedInstance.refreshGPSTracking = false
//            appDelegate.nextRide = nil
//            setBottomViewFunctionality()
//            callApiToGetNextRide()
//        }else{
//            checkIfTrackingIsEnabled()
//        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailViewController.rideSpecificNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        
        let user = Utilities.getSignInUser()
        if let user = user{
            if  user.isAdmin {
                NotificationCenter.default.addObserver(self, selector: #selector(self.impersonateNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "ImpersonateNotification"), object: nil)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ImpersonateNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
    }
    
    
    func bringSubViewToFront(){
        self.view.bringSubview(toFront: contentView)
    }
    
    func checkIfTrackingIsEnabled(){
        if (userDefault.object(forKey: TimerStartTime) != nil){
            if  appDelegate.nextRide == nil{
                callApiToGetNextRide()
            }else {
                carContentView.isHidden = true
                checkGPSTimer()
            }
        }
    }
    
    func checkGPSTimer(){
        let totalMinute = getTimerStartedDuration()
        if totalMinute != 999999 {
            let timeDuration =
                userDefault.object(forKey: TimerDuration) as! Int
            if appDelegate.timer == nil{
                setTimer()
            }
            if totalMinute > timeDuration{
                setBottomViewFunctionality()
                return
            }
            setTopViewFunctionality()
        }
    }
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            callApiToGetNextRide()
            
        }
        networkViewObj?.delegate = self
    }
    
    
    // MARK:- Service call
    
    func getImpersonateUserDetail(value: String!){
        
        var key = ""
        if value.isNumber{
            key = "Id"
        }else{
            key = "Email"
        }
        
        let params: [String:String] = [
            key: value
        ]
        
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.getUserTokenForAdmin) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            
            if status == HttpRequest.Status.Success{
                let token = Token(json: (json["Payload"].array?.first)!)
                Store.sharedStore.token = token
                getAccountDetails()
                self.setBottomViewFunctionality()
            }else{
                let payload = json["Payload"].array!
                let dicTmp = payload.first?.dictionary!
                let error = dicTmp?["error"]?.string!
                self.showErrorMessage(message: error! )
            }
        }
    }
    
    func callApiToGetNextRide(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getPreConfiguredDriveRequestNext) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let ride = dataArray.map { json in
                    return Ride(json: json)
                }
                
                if ride.count != 0{
                    var passengerCount = 0
                    for passenger in ride.first!.passengers!{
                        
                        if passenger.hasDriverRejectedBooking || passenger.hasCanceledBooking {
                            ride.first!.passengers?.remove(at: passengerCount)
                        }else{
                            passengerCount += 1
                        }
                    }
                }
                
                if ride.count == 0{
                    self.view.hideLoader()
                    self.carContentView.isHidden = false
                    self.showAlertForNoRide()
                    self.setBottomViewFunctionality()
                    return
                }
                if ride.first!.currentTime! > ride.first!.endTime!{
                    self.view.hideLoader()
                    self.carContentView.isHidden = false
                    self.setBottomViewFunctionality()
                    return
                }
                
                appDelegate.nextRide = ride[0]
                if self.bottomView.isHidden{
                    self.startTracking()
                }
                else{
                    self.drawRootOnMap()
                }
            }
            self.view.hideLoader()
        }
    }
    
    func getPassengeriOSAppVersion(){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriveriOSAppVersion) { (data) in
            guard let data = data else{
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                
                var versionStr = ""
                _ = payload.map { json in
                    versionStr = json.string!
                }
                if versionStr.isEmpty{
                    return
                }
                if  versionStr > AppVersion{
                    updateAppCheck = true
                    showAlertForNewVersionUpdate()
                }
            }
            
        }
    }
    
    // MARK:- Custom Method Action
    
    
//    @IBAction func offeredRideViewTapAction(_ sender: Any) {
//        
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "RoutesViewController") as! RoutesViewController
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    
    @IBAction func offerRideButtonAction(_ sender: Any) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "RoutesViewController") as! RoutesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func rideSpecificNotificationReceiver(_ notification: NSNotification){
        callApiToGetNextRide()
    }
    
    func impersonateNotificationReceiver(_ notification: NSNotification){
        setImpersonate()
        
    }
    
    func setImpersonate(){
        
        let controller = ActionControllerImpersonate {
            if self.impersonateField.text!.isEmpty{
                return
            }
            self.view.showLoader()
            self.getImpersonateUserDetail(value: self.impersonateField.text!)
        }
        controller.addTextField { textField in
            self.impersonateField = textField
            self.impersonateField.placeholder = "Enter person Id or email"
        }
        present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func topViewTapGestureAction(_ sender: AnyObject) {
        
        checkNetworkAvailabilty()
    }
    
    @IBAction func trackingButtonAction(_ sender: Any) {
        checkNetworkAvailabilty()
    }
    
    
    @IBAction func bottomViewTapGestureAction(_ sender: AnyObject) {
        setBottomViewFunctionality()
        removeTrackingView()
    }
    
    @IBAction func rideInfoViewTapAction(_ sender: AnyObject) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RideDetailViewController") as! RideDetailViewController
        vc.upcomingRide = appDelegate.nextRide
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rideListButtonAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpCommingTripViewController") as! UpCommingTripViewController
        self.navigationController?.setViewControllers([vc], animated: false)
    }
    
//    @IBAction func rideListViewTapAction(_ sender: AnyObject) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpCommingTripViewController") as! UpCommingTripViewController
//        self.navigationController?.setViewControllers([vc], animated: false)
//    }
    
    @IBAction func navigationTapAction(_ sender: Any) {
        if appDelegate.navigationMatchpoints.count == 0 {
            return
        }
        
        if appDelegate.navigationMatchpoints.count > 1{
            
            for i in 0 ..< appDelegate.navigationMatchpoints.count - 2 {
                for j in i+1 ..< appDelegate.navigationMatchpoints.count - 1  {
                    
                    let annotationTmp1 = appDelegate.navigationMatchpoints[i] as! Annotation
                    let annotationTmp2 = appDelegate.navigationMatchpoints[j] as! Annotation
                    if annotationTmp1.driverTimeAtStopDate! > annotationTmp2.driverTimeAtStopDate!{
                        let tmp = annotationTmp1
                        appDelegate.navigationMatchpoints[i] = annotationTmp2
                        appDelegate.navigationMatchpoints[j] = tmp
                    }
                }
            }
            var directionsURL = ""
            var wayPoint = ""
            
            if currentLocation != nil{
                let annotationFirst = appDelegate.navigationMatchpoints[0] as! Annotation
                directionsURL = "\(googleNavigation)saddr=\(currentLocation!.coordinate.latitude),\(currentLocation!.coordinate.longitude)&daddr=\(annotationFirst.coordinate.latitude),\(annotationFirst.coordinate.longitude)"
                
                for i in 1 ..< appDelegate.navigationMatchpoints.count  {
                    let annotation = appDelegate.navigationMatchpoints[i] as! Annotation
                    wayPoint = "\(wayPoint)+to:\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
                }
            }else{
                
                let annotationFirst = appDelegate.navigationMatchpoints[0] as! Annotation
                let annotationSecond = appDelegate.navigationMatchpoints[1] as! Annotation
                directionsURL = "\(googleNavigation)saddr=\(annotationFirst.coordinate.latitude),\(annotationFirst.coordinate.longitude)&daddr=\(annotationSecond.coordinate.latitude),\(annotationSecond.coordinate.longitude)"
                
                for i in 2 ..< appDelegate.navigationMatchpoints.count  {
                    let annotation = appDelegate.navigationMatchpoints[i] as! Annotation
                    wayPoint = "\(wayPoint)+to:\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
                }
            }
            directionsURL = "\(directionsURL)\(wayPoint)"
            showNavigation(directionsURL: directionsURL)
        }
        else{
            var directionsURL = ""
            if currentLocation != nil{
                let annotationFirst = appDelegate.navigationMatchpoints[0] as! Annotation
                directionsURL = "\(googleNavigation)saddr=\(currentLocation!.coordinate.latitude),\(currentLocation!.coordinate.longitude)&daddr=\(annotationFirst.coordinate.latitude),\(annotationFirst.coordinate.longitude)"
                
            }else{
                
                let annotationFirst = appDelegate.navigationMatchpoints[0] as! Annotation
                directionsURL = "\(googleNavigation)saddr=&daddr=\(annotationFirst.coordinate.latitude),\(annotationFirst.coordinate.longitude)"
            }
            showNavigation(directionsURL: directionsURL)
        }
        
    }
    
    func showNavigation(directionsURL: String){
        let url = URL(string: directionsURL)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK:- Custom Method
    
    func startTracking(){
        
        addTrackingMapView()
        let nextRideTmp = appDelegate.nextRide!
        userDefault.set(nextRideTmp.startTime, forKey: TimerStartTime)
        userDefault.set(getTrackingDuration(), forKey: TimerDuration)
        setTimer()
    }
    
    func getDateFromTimeString(timeStr: String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: timeStr)
        return date!
    }
    
    func getTrackingDuration()->Int{
        let nextRideTmp = appDelegate.nextRide!
        let difference = Calendar.current.dateComponents([.hour,.minute], from: nextRideTmp.startTime!, to: nextRideTmp.endTime!)
        let totalMinute = difference.hour! * 60 + difference.minute!
        return totalMinute
    }
    
    func setTopViewFunctionality(){
        addTrackingMapView()
    }
    
    func setBottomViewFunctionality(){
        self.title = ""
        userDefault.removeObject(forKey: TimerStartTime)
        userDefault.removeObject(forKey: TimerDuration)
        appDelegate.navigationMatchpoints.removeAll()
        if appDelegate.timer != nil{
            appDelegate.timer!.invalidate()
        }
        removeTrackingView()
    }
    
    func updateUserCurrentLocation(){
        let user = Utilities.getSignInUser()
        if currentLocation == nil || user == nil{
            return
        }
        let params: [String : Any] = [
            "latitude": currentLocation!.coordinate.latitude,
            "longitude": currentLocation!.coordinate.longitude,
            
            ]
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDriverLocation) { payload in
            guard let data = payload else{
                return
            }
            _ = HttpRequest.sharedRequest.parseData(data: data)
        }
    }
    
    func setTimer(){
        appDelegate.timer  = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:  #selector(self.methodToBeCalled(timer:)) , userInfo: nil, repeats: true)
    }
    
    func methodToBeCalled(timer: Timer){
        let totalMinute = getTimerStartedDuration()
        if totalMinute != 999999 {
            let timeDuration =
                userDefault.object(forKey: TimerDuration) as! Int
            if totalMinute > timeDuration{
                setBottomViewFunctionality()
                return
            }
            updateUserCurrentLocation()
        }
        else {
            timer.invalidate()
        }
    }
    
    func getTimerStartedDuration()->Int{
        if (userDefault.object(forKey: TimerStartTime) != nil){
            let nextRideTmp = appDelegate.nextRide!
            let time = userDefault.object(forKey: TimerStartTime) as! Date
            let difference = Calendar.current.dateComponents([.hour,.minute], from: time, to: nextRideTmp.currentTime!)
            let totalMinute = difference.hour! * 60 + difference.minute!
            return totalMinute
        }
        return 999999
    }
    
    override func viewDidLayoutSubviews() {
        bottomMenuBar?.frame = CGRect(x: 0, y: self.view.frame.height - 49, width: self.view.frame.width, height: 49)
    }
    
    func addTrackingMapView(){
        self.title = "GPS Tracking"
        trackingMapView?.removeFromSuperview()
        contentView.isHidden = false
        carContentView.isHidden = true
        bottomMenuBar = self.view.showBottomMenuBar()
        trackingMapView = GPSTrackingMapView.initView()
        bottomView.isHidden = false
        rideInfoView.isHidden = false
        trackingMapView.frame = contentView.bounds
        contentView.addSubview(trackingMapView)
        contentView.bringSubview(toFront: bottomView)
        contentView.bringSubview(toFront: rideInfoView)
        contentView.bringSubview(toFront: navigationView)
        self.drawRootOnMap()
    }
    
    func drawRootOnMap(){
        
        contentView.isHidden = false
        carContentView.isHidden = true
        if appDelegate.nextRide != nil{
            trackingMapView.drawRoot(ride: appDelegate.nextRide)
        }else{
            callApiToGetNextRide()
        }
    }
    
    func showUserCurrentLocation(){
        if currentLocation != nil && trackingMapView != nil && appDelegate.nextRide != nil{
            trackingMapView.currentLocation = currentLocation
            trackingMapView.showCurrentLocation()
        }
    }
    
    func removeTrackingView(){
        carContentView.isHidden = false
        contentView.isHidden = true
        bottomMenuBar?.removeFromSuperview()
        bottomView.isHidden = true
        rideInfoView.isHidden = true
        trackingMapView?.removeFromSuperview()
    }
    
    // MARK:- CLLocationManagerDelegate Method
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = locations.last! as CLLocation
        showUserCurrentLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func showAlertForNoRide(){
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Keine Fahrt in der nächsten 24 Stunden.", preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(okAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func getDriverCarDetails(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriverCarDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let car = dataArray.map { json in
                    return Car(json: json)
                }
                appDelegate.carDetail = car[0]
            }
            self.view.hideLoader()
        }
    }
    
}

//MARK:- Extension

extension GPSTrackingViewController: RideDetailViewControllerDelegate{
    func cancelTripStatus(success: Bool) {
        if success{
            setBottomViewFunctionality()
            callApiToGetNextRide()
        }
    }
}

extension GPSTrackingViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}
