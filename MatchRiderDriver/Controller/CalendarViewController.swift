//
//  CalendarViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 12/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

protocol CalendarViewControllerDelegate {
    func reloadData()
}

class CalendarViewController: BaseViewController {
    
    @IBOutlet var infoImgView: UIImageView!
    @IBOutlet var fromDateLabel: UILabel!
    @IBOutlet var fromDayLabel: UILabel!
    @IBOutlet var toDateLabel: UILabel!
    @IBOutlet var toDayLabel: UILabel!
    @IBOutlet var deleteButton: UIButton!
    
    var delegate: CalendarViewControllerDelegate! = nil
    var datePicker = MIDatePicker.getFromNib()
    var startDate: String!
    var endDate: String!
    let user = Utilities.getSignInUser()
    var holiday: Holiday!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Abwesenheitsplaner"
        setupDatePicker()
        setBackButton()
        
        if holiday != nil{
            setInitialValue(fromDate: holiday.holidayStart!, toDate: holiday.holidayEnd!)
            deleteButton.isHidden = false
        } else{
            setInitialValue(fromDate: Date(), toDate: Date())
            deleteButton.isHidden = true
        }
    }
    
    
    //MARK:- Custom Method
    
    func setInitialValue(fromDate: Date,toDate: Date){
        datePicker.tag = 1
        setValue(date: fromDate)
        datePicker.tag = 2
        setValue(date: toDate)
    }
    
     func setupDatePicker() {
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.config.animationDuration = 0.25
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Done"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = hexStringToUIColor(hex: MatchRiderColor)
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
       // datePicker.datePicker.datePickerMode = .dateAndTime
    }
    
    
    func setValue(date: Date){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de_DE")
        var difference = Calendar.current.dateComponents([.day,.hour,.minute], from: Date(), to: date)
        var day = ""
        if difference.day! == 0 && difference.hour! == 0{
            day = "Heute"
        }else{
            dateFormatter.dateFormat = "EEEE"
            day = dateFormatter.string(from: date)
        }
        if datePicker.tag == 1{
            if fromDateLabel.text!.isEmpty{
                dateFormatter.dateFormat = "yyyy-MM-dd"
                startDate = "\(dateFormatter.string(from: date))T00:00:00"
                
                dateFormatter.dateFormat = "dd MMM yy"
                fromDateLabel.text = dateFormatter.string(from: date)
                fromDayLabel.text = day
            }else{
                difference = Calendar.current.dateComponents([.day,.hour,.minute], from: Date(), to: date )
                if difference.day! >= 0 && difference.hour! >= 0{
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    startDate = "\(dateFormatter.string(from: date))T00:00:00"
                    dateFormatter.dateFormat = "dd MMM yy"
                    fromDateLabel.text = dateFormatter.string(from: date)
                    fromDayLabel.text = day
                    dateFormatter.dateFormat = "dd MMM yy"
                    let toDate = dateFormatter.date(from: toDateLabel.text!)
                    difference = Calendar.current.dateComponents([.day,.hour,.minute], from: toDate!, to: date )
                    if difference.day! >= 0 && difference.hour! >= 0 && difference.minute! >= 0{
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        endDate = "\(dateFormatter.string(from: date))T00:00:00"
                        dateFormatter.dateFormat = "dd MMM yy"
                        toDateLabel.text = dateFormatter.string(from: date)
                        toDayLabel.text = day
                    }
                }
            }
        } else{
            if toDateLabel.text!.isEmpty{
                dateFormatter.dateFormat = "yyyy-MM-dd"
                endDate = "\(dateFormatter.string(from: date))T00:00:00"
                dateFormatter.dateFormat = "dd MMM yy"
                toDateLabel.text = dateFormatter.string(from: date)
                toDayLabel.text = day
            }else{
                dateFormatter.dateFormat = "dd MMM yy"
                let fromDate = dateFormatter.date(from: fromDateLabel.text!)
                difference = Calendar.current.dateComponents([.day,.hour,.minute], from: fromDate!, to: date )
                if difference.day! >= 0 && difference.hour! >= 0 && difference.minute! >= 0{
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    endDate = "\(dateFormatter.string(from: date))T00:00:00"
                    dateFormatter.dateFormat = "dd MMM yy"
                    toDateLabel.text = dateFormatter.string(from: date)
                    toDayLabel.text = day
                }
            }
        }
    }
    //MARK:- Custom method action
    
    @IBAction func fromTapAction(_ sender: AnyObject) {
        datePicker.tag = 1
        datePicker.show(inVC: self)
    }
    
    @IBAction func toAction(_ sender: AnyObject) {
        datePicker.tag = 2
        datePicker.show(inVC: self)
    }
    
    @IBAction func submitButtonAction(_ sender: AnyObject) {
        if holiday != nil{
            updateCommuteHoliday(id: "\(holiday.id)")
        }else{
            updateCommuteHoliday(id: "")
        }
    }
    
    @IBAction func deleteButtonAction(_ sender: AnyObject) {
        deleteCommuteHolidays()
    }
    
    //MARK:- Service api call
    
    func deleteCommuteHolidays(){
        self.view.showLoader()
        let endPoint = "\(Endpoint.deleteCommuteHoliday)\(holiday.id)"
        HttpRequest.sharedRequest.performDeleteRequest(Store.sharedStore.token, endPoint:endPoint ) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json.bool
            if status == nil{
                return
            }
            if status!{
                self.delegate.reloadData()
                self.showSuccessMessage(message: CommuteHolidayDeleted)
                _ = self.navigationController?.popViewController(animated: true)
                
            }else{
                self.showErrorMessage(message: FailedToDeleteCommuteHoliday)
            }
        }
    }
    
    func updateCommuteHoliday(id: String!){
        let idTmp = id!
        let start = startDate!
        let end = endDate!
        let params = [
            "Id": idTmp,
            "HolidayStart": start,
            "HolidayEnd": end
        ]
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.insertUpdateCommuteHoliday) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json.bool
            if status!{
                self.delegate.reloadData()
                if id == ""{
                    self.showSuccessMessage(message: HolidayAddedSuccessfully)
                    
                }else{
                    self.showSuccessMessage(message: HolidayUpdatedSuccessfully)
                }
                _ = self.navigationController?.popViewController(animated: true)
                
            }else{
                if id == ""{
                    self.showErrorMessage(message: FailedToAddCommuteHoliday)
                }else{
                    self.showErrorMessage(message: FailedToUpdateCommuteHoliday)
                }
            }
        }
    }
}

extension CalendarViewController: MIDatePickerDelegate {
    
    func miDatePicker(_ amDatePicker: MIDatePicker, didSelect date: Date) {
        setValue(date: date)
    }
    func miDatePickerDidCancelSelection(_ amDatePicker: MIDatePicker) {
        // NOP
    }
    
}


