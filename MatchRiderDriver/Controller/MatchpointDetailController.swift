//
//  MatchpointDetailController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class MatchpointDetailController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var annotation: Annotation!
    var upcomingRide: UpComingRideModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Match Point"
        setBackButton()
        tableView.hideEmptyRow()
        tableView.register(UINib(nibName: "FellowDriverCell", bundle: nil), forCellReuseIdentifier: "FellowDriverCell")
    }
    
    //MARK:- Cusstom Method
    
    func setBackButton()
    {
        let buttonImage = UIImage(named: "ic_cancel_white");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.custom)
        leftBarButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        leftBarButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        leftBarButton.setImage(buttonImage, for: UIControlState.normal)
        leftBarButton.addTarget(self, action: #selector(MatchpointDetailController.backBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false);
    }
    
    //MARK:- Custom method action
    
    func backBtnClicked(sender: UIButton!)
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension MatchpointDetailController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if annotation.passengers.count != 0{
            return annotation.passengers.count + 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MatchPointCell") as! MatchPointCell
            cell.descriptionLabel.text = annotation.locationDescription
            cell.addressLabel.text = annotation.locationAddress
            cell.cityLabel.text = annotation.locationCity
            let imgPath = "\(Endpoint.getLocationImage)\(annotation.locationId!)"
            HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
                cell.imgView.image = image
            }
            return cell
        }else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "FellowDriverCell") as! FellowDriverCell
            if annotation.passengers.count != 0{
                cell.configureCell(passengers: annotation.passengers,index: indexPath.section-1)
            }else{
                cell.noBookingLabel.isHidden = false
                cell.baseView.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && annotation.passengers.count == 0{
            return 44
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.white
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.text = "Passagiere"
        label.backgroundColor = UIColor.clear
        label.frame = CGRect(x: 15, y: 8, width: 190, height: 20)
        if section == 1{
            vw.addSubview(label)
            let lineView = UIView()
            lineView.frame = CGRect(x: 0, y: 35, width: screenWidth, height: 1)
            lineView.backgroundColor = hexStringToUIColor(hex: LineLightGrayColor)
            vw.addSubview(lineView)
        }
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 36
        }
        return 0.01
    }
}
