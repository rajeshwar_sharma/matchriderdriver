//
//  MatchPointController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class MatchPointController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var matchpoint: Matchpoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Match Point"
        setBackButton()
    }
    
    // MARK:- Custom method

    func setBackButton()
    {
        let leftBarButton: UIButton = UIButton(type: UIButtonType.custom)
        leftBarButton.frame = CGRect(x: 0, y: 0, width: 70, height: 40)
        leftBarButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        leftBarButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        leftBarButton.setTitle("Abbrechen", for: .normal)
        leftBarButton.addTarget(self, action: #selector(backBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false);
    }
    
    // MARK:- Custom method action
    
    func backBtnClicked(sender: UIButton!)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MatchPointController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       let cell = self.tableView.dequeueReusableCell(withIdentifier: "MatchPointCell2") as! MatchPointCell2
        cell.nameLabel.text = matchpoint.descriptionInfo
        cell.infoLabel.text = matchpoint.whereToStand
        cell.streetLabel.text = matchpoint.street
        cell.postalLabel.text = "\(matchpoint.postalCode) \(matchpoint.city)"
        let imgPath = "\(matchpoint.photo)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            cell.photoView.image = image
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 460
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
        
    }
}


