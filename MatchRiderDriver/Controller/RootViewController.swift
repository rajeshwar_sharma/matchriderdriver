//
//  RootViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
class RootViewController: BaseViewController,CLLocationManagerDelegate {
    
    @IBOutlet var mapView: MKMapView!
   
    let locationManager =  CLLocationManager()
    var upcomingRide: Ride!
    let startAnnotation = Annotation()
    let destAnnotation = Annotation()
    var locationIdList: [Int:Annotation]! = [:]
    var matchpoints: [MKAnnotation] = []
    var matchPointsCount = 0
    var currentLocation : CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fahrtdetails"
        setBackButton()
        mapView.delegate = self
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        locationManager.requestWhenInUseAuthorization()
        self.mapView.add(upcomingRide.polyline)
        showMatchPoint()
    }
    
  //MARK:- Custom Method
    
    func showMatchPoint(){
        self.mapView.add(upcomingRide.polyline)
        matchpoints.removeAll()
        mapView.removeAnnotations(mapView.annotations)
        locationIdList.removeAll()
        startAnnotation.coordinate = CLLocationCoordinate2D(latitude: upcomingRide.start!.latitude, longitude: upcomingRide.start!.longitude)
        startAnnotation.locationDescription = upcomingRide.start!.descriptionInfo
        startAnnotation.locationAddress = "\(upcomingRide.start!.street) \(upcomingRide.start!.state) \(upcomingRide.start!.postalCode)"
        startAnnotation.locationCity = upcomingRide.start!.city
        startAnnotation.locationId = upcomingRide.start!.id
        startAnnotation.tag = 10
        
        destAnnotation.coordinate = CLLocationCoordinate2D(latitude: upcomingRide.destination!.latitude, longitude: upcomingRide.destination!.longitude)
        destAnnotation.locationDescription = upcomingRide.destination!.descriptionInfo
        destAnnotation.locationAddress = "\(upcomingRide.destination!.street) \(upcomingRide.destination!.state) \(upcomingRide.destination!.postalCode)"
        destAnnotation.locationCity = upcomingRide.destination!.city
        destAnnotation.locationId = upcomingRide.destination!.id
        destAnnotation.tag = 11
        
        for passengerDetail in upcomingRide.passengers! {
            
            let annotationStart = locationIdList[passengerDetail.start!.id]
            if annotationStart != nil {
                annotationStart!.passengers.append(passengerDetail)
            }
            else{
                let passengerStartAnnotation = Annotation()
                
                passengerStartAnnotation.coordinate = CLLocationCoordinate2D(latitude: passengerDetail.start!.latitude, longitude: passengerDetail.start!.longitude)
                passengerStartAnnotation.locationDescription = passengerDetail.start!.descriptionInfo
                passengerStartAnnotation.locationAddress =  "\(passengerDetail.start!.street) \(passengerDetail.start!.state) \(passengerDetail.start!.postalCode)"
                
                passengerStartAnnotation.locationId = passengerDetail.start!.id
                locationIdList[passengerDetail.start!.id] = passengerStartAnnotation
                passengerStartAnnotation.tag = 20
                
                passengerStartAnnotation.passengers.append(passengerDetail)
                matchpoints.append(passengerStartAnnotation)
            }
            
            let annotationDest = locationIdList[passengerDetail.destination!.id]
            if annotationDest != nil {
                annotationDest!.passengers.append(passengerDetail)
            }
            else{
                let passengerDestAnnotation = Annotation()
                passengerDestAnnotation.coordinate = CLLocationCoordinate2D(latitude: passengerDetail.destination!.latitude, longitude: passengerDetail.destination!.longitude)
                passengerDestAnnotation.locationDescription = passengerDetail.destination!.descriptionInfo
                passengerDestAnnotation.locationAddress = "\(passengerDetail.destination!.street) \(passengerDetail.destination!.state) \(passengerDetail.destination!.postalCode)"
                passengerDestAnnotation.locationId = passengerDetail.destination!.id
                locationIdList[passengerDetail.destination!.id] = passengerDestAnnotation
                passengerDestAnnotation.tag = 21
                passengerDestAnnotation.passengers.append(passengerDetail)
                matchpoints.append(passengerDestAnnotation)
            }
        }
        
        let annotationStart = locationIdList[upcomingRide.start!.id]
        if annotationStart == nil {
            matchpoints.append(startAnnotation)
        }
        
        let annotationEnd = locationIdList[upcomingRide.destination!.id]
        if annotationEnd == nil {
            matchpoints.append(destAnnotation)
        }
        matchPointsCount = matchpoints.count
        
        self.mapView.addAnnotations(matchpoints)
        self.mapView.showAnnotations(matchpoints, animated: false)
        self.view.layoutIfNeeded()
        boundsPolyLineRouteInMap()
        
    }
    
    func boundsPolyLineRouteInMap(){
        if let first = self.mapView.overlays.first {
            let rect = self.mapView.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
            self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
        }
    }
    
    func showCurrentLocation(){
        
        if currentLocation == nil{
            return
        }
        mapView.removeAnnotations(mapView.annotations)
        
        if matchpoints.count  > matchPointsCount  {
            matchpoints.removeLast()
        }
        let driverLocationannotation = Annotation()
        driverLocationannotation.coordinate = CLLocationCoordinate2D(latitude: currentLocation!.coordinate.latitude, longitude: currentLocation!.coordinate.longitude)
        driverLocationannotation.tag = 30
        driverLocationannotation.title = "You"
        matchpoints.append(driverLocationannotation)
        mapView.addAnnotations(matchpoints)
        // self.view.layoutIfNeeded()
        //mapView.showAnnotations(matchpoints, animated: false)
        
    }

    
    // MARK:- CLLocationManagerDelegate Method
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = locations.last! as CLLocation
        showCurrentLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
  
}

extension RootViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let matchpoint = view.annotation as? Annotation {
            if matchpoint.tag == 30{
                //mapView.deselectAnnotation(view.annotation, animated: false)
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchpointDetailController") as! MatchpointDetailController
            vc.annotation = matchpoint
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
            navigationController.navigationBar.tintColor = UIColor.white
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white
            ]
            self.navigationController?.present(navigationController, animated: true, completion: nil)
            mapView.deselectAnnotation(view.annotation, animated: false)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = hexStringToUIColor(hex: MatchRiderColor)
        polylineRenderer.lineWidth = 4
        polylineRenderer.lineCap = .square
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }
        guard let matchpoint = annotation as? Annotation else {
            return nil
        }
        let view = MKAnnotationView(annotation: matchpoint, reuseIdentifier: "Matchpoint")
        view.canShowCallout = false
        switch matchpoint.tag {
        case 10:
            //view.canShowCallout = false
            view.image = UIImage(named: "StartPoint")!
            break
        case 11:
            //view.canShowCallout = false
            view.image = UIImage(named: "EndPoint")!
            break
        case 20:
            //view.canShowCallout = false
            view.image = UIImage(named: "StartPoint")!
        case 21:
            //view.canShowCallout = false
            view.image = UIImage(named: "EndPoint")!
        case 30:
            view.canShowCallout = true
            view.image = UIImage(named: "driver")!
        default:
            break
        }
      return view
    }
}


