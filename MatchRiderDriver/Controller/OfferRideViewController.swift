//
//  OfferRideViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 18/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class OfferRideViewController: BaseViewController {
    
    @IBOutlet var startView: UIView!
    @IBOutlet var endView: UIView!
    @IBOutlet var dateView: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var swapView: UIView!
    @IBOutlet var sourceLabel: UILabel!
    @IBOutlet var destinationLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var seatSizeLabel: UILabel!
    
    
    var datePicker = MIDatePicker.getFromNib()
    var superHighway: SuperHighway!
    var routeSuperHighway: SuperHighwayRoute!
    var upRouteSuperHighway: SuperHighwayRoute!
    var reverseRouteSuperHighway: SuperHighwayRoute!
    var networkViewObj: NetworkView!
    var boolCheck = true
    var offerButtonCheck = false
    var directionIndex: Int!
    var dropDown: SimpleDropDownView!
    var dropDownData: NSMutableArray! = []
    let offerRideData = OfferRideData.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fahrt Anbieten"
        swapView.tag = directionIndex
        setBackButton()
        swapView.setCircleView()
        setupDatePicker()
        Store.sharedStore.startLocationId = ""
        Store.sharedStore.destinationLocationId = ""
        let calendar = Calendar.current
        let currentDate = calendar.date(byAdding: .hour, value: 1, to: Date())
        offerRideData.routeAStartPreconfiguredRouteLocationId = superHighway.routeAStartPreconfiguredRouteLocationId
        offerRideData.routeADestPreconfiguredRouteLocationId = superHighway.routeADestPreconfiguredRouteLocationId
        offerRideData.routeBStartPreconfiguredRouteLocationId = superHighway.routeBStartPreconfiguredRouteLocationId
        offerRideData.routeBDestPreconfiguredRouteLocationId = superHighway.routeBDestPreconfiguredRouteLocationId
        dateLabel.text = getDateWithYear(date: currentDate!)
        timeLabel.text = getTime(date: currentDate!)
        sourceLabel.text = "Start Location"
        destinationLabel.text = "Destination Location"
        if superHighway.seats > 0{
            seatSizeLabel.text = "\(superHighway.seats)"
        }else{
            seatSizeLabel.text = "1"
        }

        dropDown = SimpleDropDownView()
        dropDown.delegate = self
        checkNetworkAvailabilty()
        setMaxPoints()
    }
    
    
    //MARK:- Custom Method
    //Du kannst bis zu 1025 Punkte verdienen*, indem du diese Fahrt anbietest.
    func setMaxPoints(){
        var highLightStr1 = ""
        let highLightStr2 = "Punkte"
        if swapView.tag == 1{
            highLightStr1 = "\(superHighway.routeAMaxPoints)"
        }else{
            highLightStr1 = "\(superHighway.routeBMaxPoints)"
        }
        let msgStr = "Du kannst bis zu \(highLightStr1) Punkte verdienen*, indem du diese Fahrt anbietest."
        let range1 = (msgStr as NSString).range(of: highLightStr1)
        let range2 = (msgStr as NSString).range(of: highLightStr2)
        let myMutableString = NSMutableAttributedString(string: msgStr)
        
        myMutableString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 18)
            , NSForegroundColorAttributeName : hexStringToUIColor(hex: "659966")], range: range1)
        
        myMutableString.setAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 18)
            , NSForegroundColorAttributeName : hexStringToUIColor(hex: MatchRiderColor)], range: range2)
        
        pointLabel.attributedText = myMutableString
    }
    
    func setHeaderTitle(title: String){
        titleLabel.text = title
    }
    
    func setData(){
        
        setHeaderTitle(title: "\(routeSuperHighway.startLocationCity) ➡︎ \(routeSuperHighway.destinationLocationCity)")

        if swapView.tag == 1{
            
            if offerRideData.routeAStartPreconfiguredRouteLocationId! != 0{
                Store.sharedStore.startLocationId = "\(offerRideData.routeAStartPreconfiguredRouteLocationId!)"
                Store.sharedStore.destinationLocationId = "\(offerRideData.routeADestPreconfiguredRouteLocationId!)"
                
            }else{
                Store.sharedStore.startLocationId = "\(routeSuperHighway.startMatchPointId)"
                Store.sharedStore.destinationLocationId = "\(routeSuperHighway.destinationMatchPointId)"
            }
            for matchPointTmp in routeSuperHighway.matchpoints {
                
                if "\(matchPointTmp.id)" == Store.sharedStore.startLocationId {
                    sourceLabel.text = "\(matchPointTmp.descriptionInfo), \(matchPointTmp.city)"
                }
                if "\(matchPointTmp.id)" == Store.sharedStore.destinationLocationId {
                    destinationLabel.text = "\(matchPointTmp.descriptionInfo), \(matchPointTmp.city)"
                }
            }
            
        }else{
            
            if offerRideData.routeBStartPreconfiguredRouteLocationId! != 0{
                
                Store.sharedStore.startLocationId = "\(offerRideData.routeBStartPreconfiguredRouteLocationId!)"
                Store.sharedStore.destinationLocationId = "\(offerRideData.routeBDestPreconfiguredRouteLocationId!)"
                
            }else{
                Store.sharedStore.startLocationId = "\(routeSuperHighway.startMatchPointId)"
                Store.sharedStore.destinationLocationId = "\(routeSuperHighway.destinationMatchPointId)"
            }
           
            for matchPointTmp in routeSuperHighway.matchpoints {
                
                if "\(matchPointTmp.id)" == Store.sharedStore.startLocationId {
                    sourceLabel.text = "\(matchPointTmp.descriptionInfo), \(matchPointTmp.city)"
                }
                if "\(matchPointTmp.id)" == Store.sharedStore.destinationLocationId {
                    destinationLabel.text = "\(matchPointTmp.descriptionInfo), \(matchPointTmp.city)"
                }
            }
        }
    }
    
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            if boolCheck{
                boolCheck = false
                getPreconfiguredUpRoute(preConfiguredId:superHighway.startPreconfiguredRouteId)
                getPreconfiguredReverseRoute(preConfiguredId:superHighway.destPreconfiguredRouteId)
                return
            }
            if offerButtonCheck{
                offerButtonCheck = false
                addUpdatePreconfiguredRouteCustomization()
                return
            }
            if swapView.tag == 1{
                swapView.tag = 2
                if upRouteSuperHighway == nil{
                    getPreconfiguredReverseRoute(preConfiguredId:superHighway.destPreconfiguredRouteId)
                }else{
                    self.routeSuperHighway = self.reverseRouteSuperHighway
                    self.setData()
                    self.setMaxPoints()
                }
            }else{
                swapView.tag = 1
                if reverseRouteSuperHighway == nil{
                    getPreconfiguredUpRoute(preConfiguredId:superHighway.startPreconfiguredRouteId)
                }else{
                    self.routeSuperHighway = self.upRouteSuperHighway
                    self.setData()
                    self.setMaxPoints()
                }
            }
        }
        networkViewObj?.delegate = self
    }
    
   func setupDatePicker() {
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.config.animationDuration = 0.25
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Done"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = hexStringToUIColor(hex: MatchRiderColor)
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = Locale(identifier: "de_DE")
        let myString = formatter.string(from: Date())
        let currentDate = formatter.date(from: myString)
        datePicker.datePicker.minimumDate = currentDate
    }
    
    func navigateToMapViewController(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.routeSuperHighway = routeSuperHighway
        vc.upRouteSuperHighway = upRouteSuperHighway
        vc.reverseRouteSuperHighway = reverseRouteSuperHighway
        vc.directionIndex = swapView.tag
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getRideDateTime()->String{
        let timeStr = "\(dateLabel.text!) \(timeLabel.text!)"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de_DE")
        dateFormatter.dateFormat = "EEEE, dd MMM yyyy HH:mm"
        let date = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter .string(from: date!)
    }
    
    func checkRideTimeVaidation()-> Bool{
        let timeStr = "\(dateLabel.text!) \(timeLabel.text!)"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de_DE")
        dateFormatter.dateFormat = "EEEE, dd MMM yyyy HH:mm"
        let date = dateFormatter.date(from: timeStr)
        return (Date() < date!)
    }
    
    func navigateToThanksViewController(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ThanksViewController") as! ThanksViewController
        vc.preConfiguredRouteId = swapView.tag == 1 ? superHighway.startPreconfiguredRouteId : superHighway.destPreconfiguredRouteId
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func showAlert(message: String){
        let controller = ActionControllerCommonAlert(message)
        self.present(controller, animated: true, completion: nil)
    }

    
    //MARK:- Custom Method Action
    
    @IBAction func offerButtonAction(_ sender: Any) {
        offerButtonCheck = true
        checkNetworkAvailabilty()
    }
    
    @IBAction func swapViewTapAction(_ sender: Any) {
        
        checkNetworkAvailabilty()
    }
    
    @IBAction func startViewTapAction(_ sender: Any) {
        navigateToMapViewController()
    }
    
    @IBAction func endViewTapAction(_ sender: Any) {
        navigateToMapViewController()
    }
    
    @IBAction func dateViewTapAction(_ sender: Any) {
        datePicker.tag = 1
        datePicker.datePicker.datePickerMode = .date
        datePicker.show(inVC: self)
    }
    
    @IBAction func timeViewTapAction(_ sender: Any) {
        datePicker.tag = 2
        datePicker.datePicker.datePickerMode = .time
        datePicker.datePicker.locale = Locale(identifier: "en_GB")
        datePicker.show(inVC: self)
    }
    
    @IBAction func selectSeatTapAction(_ sender: UITapGestureRecognizer) {
        dropDownData.removeAllObjects()
        for i in 0..<superHighway.seats {
            dropDownData.add("\(i+1)")
        }
        if dropDownData.count == 0{
          dropDownData.add("1")
        }
        dropDown.setDropDownOn(self.view, below: sender.view, andData: dropDownData as! [Any]!)
        dropDown.toggle()
    }
    
    
    //MARK:- Service Api calling
    
    func addUpdatePreconfiguredRouteCustomization() {
        
        if !checkRideTimeVaidation(){
            self.showAlert(message: RideTimeError)
            return
        }
        
        let params = [
            "preconfiguredRouteCustomizationId": swapView.tag == 1 ? "\(superHighway.routeAStartPreconfiguredRouteCustomizationId)" : "\(superHighway.routeBStartPreconfiguredRouteCustomizationId)",
            
            "preconfiguredRouteId": swapView.tag == 1 ?"\(superHighway.startPreconfiguredRouteId)":"\(superHighway.destPreconfiguredRouteId)",
            "startLocationId": "\(Store.sharedStore.startLocationId!)",
            "destLocationId": "\(Store.sharedStore.destinationLocationId!)"
        ]
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.addUpdatePreconfiguredRouteCustomization) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                self.addPreconfiguredRouteRide()
            }else{
                self.view.hideLoader()
                self.showAlert(message: GenericError)
            }
        }
    }
    
    func addPreconfiguredRouteRide(){
        
        let params = [
            "preconfiguredRouteId": swapView.tag == 1 ?"\(superHighway.startPreconfiguredRouteId)":"\(superHighway.destPreconfiguredRouteId)",
            "rideDateTime": getRideDateTime(),
            "seats": seatSizeLabel.text!
            
        ]
        print(params)
        //self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.addPreconfiguredRouteRide) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                self.navigateToThanksViewController()
            }else{
                self.view.hideLoader()
                self.showAlert(message: GenericError)
            }
        }
    }
    
    func getPreconfiguredUpRoute(preConfiguredId: Int){
        self.view.showLoader()
        let components = [
            Endpoint.getPreconfiguredRoute,
            "\(preConfiguredId)"
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let superHighwayRoute = payload.map { json in
                    return SuperHighwayRoute(json: json)
                }
                self.upRouteSuperHighway = superHighwayRoute.first
                if self.swapView.tag == 1{
                    self.routeSuperHighway = self.upRouteSuperHighway
                    self.setData()
                    self.setMaxPoints()
                }
            }
        }
    }
    
    func getPreconfiguredReverseRoute(preConfiguredId: Int){
        self.view.showLoader()
        let components = [
            Endpoint.getPreconfiguredRoute,
            "\(preConfiguredId)"
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let superHighwayRoute = payload.map { json in
                    return SuperHighwayRoute(json: json)
                }
                
                self.reverseRouteSuperHighway = superHighwayRoute.first
                if self.swapView.tag == 2{
                    self.routeSuperHighway = self.reverseRouteSuperHighway
                    self.setData()
                    self.setMaxPoints()
                }
            }
        }
    }
  }

extension OfferRideViewController: MIDatePickerDelegate {
    
    func miDatePicker(_ amDatePicker: MIDatePicker, didSelect date: Date) {
        if datePicker.tag == 1{
            dateLabel.text = getDateWithYear(date: date)
        }else{
            timeLabel.text = getTime(date: date)
        }
        
    }
    
    func miDatePickerDidCancelSelection(_ amDatePicker: MIDatePicker) {
        // NOP
    }
}

extension OfferRideViewController: MapViewControllerDelegate{
    func updateMatchPointDetail(routeSuperHighway: SuperHighwayRoute!, directionIndex: Int) {
        swapView.tag = directionIndex
        sourceLabel.text = "\(Store.sharedStore.startLocationMatchPoint!.descriptionInfo), \(Store.sharedStore.startLocationMatchPoint!.city)"
        destinationLabel.text = "\(Store.sharedStore.destinationLocationMatchPoint!.descriptionInfo), \(Store.sharedStore.destinationLocationMatchPoint!.city)"
        self.routeSuperHighway = routeSuperHighway
        setHeaderTitle(title: "\(routeSuperHighway.startLocationCity) ➡︎ \(routeSuperHighway.destinationLocationCity)")
    }
}

extension OfferRideViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}

extension OfferRideViewController: SimpleDropDownViewDelegate{
    func selectedName(_ name: String!, andContainerView containerView: UIView!) {
        seatSizeLabel.text = name
    }
}





