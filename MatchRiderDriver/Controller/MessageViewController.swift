//
//  MessageViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
class MessageViewController: BaseViewController {
    
    @IBOutlet var textView: UITextView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bottomHeightConstraint: NSLayoutConstraint!
    var showDriverDetail : Bool!
    var checkDriverDetail : Bool!
    var messageThreadId : String!
    var numberOfRow : Int!
    var ride: Ride!
    var chats = [ChatModel]()
    var commonModel = [Any]()
    var driverDetail = [Int:Any]()
    var rideDetailIndexPath: Int = 0
    var networkViewObj: NetworkView!
    var placeholderLabel : UILabel!
    let user = Utilities.getSignInUser()
    var dataLoadingFlag = false
    var passenger: PassengerListModel!
    var passengerName: String!
    var passengerId: Int!
    
      override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Nachrichten"
        IQKeyboardManager.sharedManager().enable = false
        setBackButton()
        NotificationCenter.default.addObserver(self, selector: #selector(MessageViewController.fcmNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "FCMNotification"), object: nil)
        textView.delegate = self
        tableView.isHidden = true
        checkNetworkAvailabilty()
        if messageThreadId != nil{
            Store.sharedStore.currentMessageThreadId = messageThreadId
        }else{
            Store.sharedStore.currentMessageThreadId = "Default"
        }
        checkDriverDetail = false
       // navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setTextViewPlaceHolder()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FCMNotification"), object: nil)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    //MARK:- Custome method
    
    //*********** Check network availability **********
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            getMessagesByThread()
            registerFCMDeviceTokenToServer()
            updateMessageViewDate(messageThreadId: messageThreadId)
        }
        networkViewObj?.delegate = self
    }
    
    
    //MARK:- Service call
    
    //****** Calling api to get Message by thread ******
    func getMessagesByThread(){
        if ride == nil{
            self.bottomHeightConstraint.constant = -70
        }else{
            self.bottomHeightConstraint.constant = 0
        }
        if commonModel.count != 0{
            commonModel.removeAll()
            tableView.reloadData()
        }
        if messageThreadId == nil{
            if ride != nil{
                commonModel.append(NSNull())
                let dic:[String : Any] = [ "ride": ride]
                driverDetail[0] =  dic
                tableView.isHidden = false
                tableView.reloadData()
            }
            return
        }
        self.bottomHeightConstraint.constant = -70
        self.view.showLoader()
        dataLoadingFlag = true
        let components = [
            Endpoint.getMessagesByThread,
            messageThreadId!
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                
                let chats = payload.map { json in
                    return ChatModel(json: json)
                }
                self.chats = chats
                self.configureCommonModelData()
            }
            self.dataLoadingFlag = false
            self.view.hideLoader()
        }
    }
    
    func updateMessageViewDate(messageThreadId:String?){
        if messageThreadId == nil{
            return
        }
        let params: [String : Any] = [
            "MessageThreadId": messageThreadId!
        ]
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateMessageViewDate) { payload in
            guard let data = payload else{
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                
            }
        }
    }
    
    func setTextViewPlaceHolder(){
        self.view.layoutIfNeeded()
        placeholderLabel = UILabel()
        placeholderLabel.text = "Bitte Nachricht eingeben"
        placeholderLabel.font = UIFont.systemFont(ofSize: 14)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: textView.frame.height / 2 - placeholderLabel.frame.height/2 )
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func configureCommonModelData(){
        var index: Int = 0
        if commonModel.count != 0 {
            index = commonModel.count - 1
        }
        var addRideCheck = true
        for chat in chats {
            if chat.driverRequestId != 0{
                commonModel.append(NSNull())
                let dic:[String : Any] = ["ride": NSNull()]
                driverDetail[index] =  dic
                rideDetailIndexPath = index
                getRideDetail(index: index, driverRequestId: chat.driverRequestId)
                index += 1
            }
            if ride != nil && chat.driverRequestId == ride.id{
                addRideCheck = false
            }
            commonModel.append(chat)
            index += 1
        }
        if addRideCheck && ride != nil{
            rideDetailIndexPath = index
            commonModel.append(NSNull())
            let dic:[String : Any] = ["ride": ride]
            driverDetail[index] =  dic
            self.title = "Ich und \(passengerName!)"
            self.bottomHeightConstraint.constant = 0
        }
        tableView.reloadData()
        tableViewScrollToBottom(animated: false)
    }
    
    func getRideDetail(index: Int, driverRequestId: Int) {
        
        self.view.showLoader()
        let components = [
            Endpoint.getDriverRideDetails,
            "\(driverRequestId)"
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"]
                let rideDetail =   Ride(json: payload)
                let dic = self.driverDetail[index]
                var dicTmp = dic as! [String: Any]
                dicTmp["ride"] = rideDetail
                self.driverDetail[index] = dicTmp
            
                self.tableView.reloadData()
                if self.rideDetailIndexPath == index{
                    if rideDetail.driver != nil{
                        self.bottomHeightConstraint.constant = 0
                        self.tableViewScrollToBottom(animated: false)
                    } else{
                        if self.ride != nil{
                            let dic = self.driverDetail[index]
                            var dicTmp = dic as! [String: Any]
                            dicTmp["ride"] = self.ride
                            self.driverDetail[index] = dicTmp
                            self.bottomHeightConstraint.constant = 0
                            self.tableViewScrollToBottom(animated: false)
                            //self.title = "Ich und \(self.ride.driver!.firstName)"
                            
                        }
                    }
                }
                if self.passengerName != nil || self.passengerName == nil{
                    if self.passengerName == nil {
                       self.title = "Ich und Passenger"
                    }else{
                    self.title = "Ich und \(self.passengerName!)"
                    }
                }
                
            }
            self.view.hideLoader()
        }
    }
    
    //************ This method get call when app receive notification to add message in list **********
    
    func fcmNotificationReceiver(_ notification: NSNotification){
        if dataLoadingFlag{
            return
        }
        let dic = notification.userInfo as! [String:Any]
        let chatTmp = dic["chatDetail"] as! ChatModel
        let check = dic["check"] as! Bool
        ride = nil
        passengerId = chatTmp.personId
        passengerName = chatTmp.personName
        if check{
            self.chats.removeAll()
            self.chats.append(chatTmp)
            configureCommonModelData()
            
        }else{
            messageThreadId = chatTmp.messageThreadId
            Store.sharedStore.currentMessageThreadId = messageThreadId
            getMessagesByThread()
        }
    }
    
    //********  Use to Scroll tableview to last row **********
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let numberOfSections = self.tableView.numberOfSections
            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = NSIndexPath(row: numberOfRows-1, section: numberOfSections-1)
                self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: animated)
                self.tableView.isHidden = false
            }
        }
    }
    
    @IBAction func sendButtonAction(_ sender: AnyObject) {
        sendMessage(chatData: nil)
    }
    
    func sendMessage(chatData: ChatModel?){
        if textView.text.trim().isEmpty && chatData == nil{
            return
        }
        var  recentChat: ChatModel!
        if chatData != nil {
            recentChat = chatData
        }else{
            recentChat = getRecentChatModelDummyData()
        }
        if commonModel.count == 0{
            return
        }
        commonModel.append(recentChat)
        tableView.reloadData()
        tableViewScrollToBottom(animated: false)
        let dic = driverDetail[rideDetailIndexPath]
        let dicTmp = dic as! [String: Any]
        let rideTmp = dicTmp["ride"]
        if rideTmp is NSNull {
            return
        }
        let rideDetail = rideTmp as! Ride
        sendMessage(rideDetail: rideDetail, recentChat: recentChat, index: commonModel.count-1)
        textView.text = ""
    }
    
    func sendMessage(rideDetail: Ride!,recentChat: ChatModel!,index: Int){
        
        var receiverPersonId = 0
        if passengerId != nil{
          receiverPersonId = passengerId
        }
        
        let rideIdStr = "\(rideDetail.id)"
        let params = [
            "receiverPersonId": "\(receiverPersonId)",
            "rideId": rideIdStr,
            "message": recentChat.messagePosting
        ]
        print(params)
        HttpRequest.sharedRequest.performPostRequest( Store.sharedStore.token, requestParam: params, endPoint: Endpoint.sendMessage) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            print("Send message status....\(json)")
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let chats = payload.map { json in
                    return ChatModel(json: json)
                }
                let chatTmp = chats.first!
                self.updateMessageViewDate(messageThreadId: chatTmp.messageThreadId)
                self.commonModel[index] = chats.first! as ChatModel
            }else{
                if index < self.commonModel.count{
                    let recentChatData = self.commonModel[index]
                    self.resendMessage(recentChat: recentChatData as! ChatModel)
                    self.commonModel.remove(at: index)
                    self.tableView.reloadData()
                }
                return
            }
        }
    }
    
    func resendMessage(recentChat: ChatModel){
        let alert=UIAlertController(title: "Senden der Nachricht fehlgeschlagen", message: recentChat.messagePosting, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "Abbrechen", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction) in
            print("cancel")
        }));
        alert.addAction(UIAlertAction(title: "Erneut senden", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
            self.sendMessage(chatData: recentChat)
        }));
        present(alert, animated: true, completion: nil);
    }
    
    //********** It create chat model object while sending new message ********
    //textView.text.trim().encodeEmoji
    func getRecentChatModelDummyData()-> ChatModel{
        let jsonObject: [Any]  = [
            [
                "PersonName": NSNull(),
                "MessagePostId": 0,
                "MessageThreadId": "",
                "MessageThreadParticipantId": "0",
                "DriverRequestId": 0,
                "MessagePosting":textView.text.trim() ,
                "DateCreated": "",
                "PersonId": 0,
                "PersonPhoto": "\(Endpoint.getPersonImage)\(user!.id)/l",
                "IsUnread": false,
                "PersonUserId": user!.id
            ]
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        let json = JSON(data:jsonData )
        let payload = json.array!
        return ChatModel(json: payload.first!)
    }
    
    //********* Calculate the size of Message ***********
    func getMessageSize(message:String) -> CGSize {
        let lblMsg = UILabel()
        lblMsg.backgroundColor = UIColor.yellow
        lblMsg.text = message
        let font = UIFont.systemFont(ofSize: 14.0)
        var desiredWidth: CGFloat = 190
        if screenWidth == 375{
            desiredWidth = 240
        }
        else if screenWidth == 414{
            desiredWidth = 270
        }
        lblMsg.font = font
        lblMsg.numberOfLines = 0;
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size: CGSize = lblMsg.sizeThatFits(CGSize(width: desiredWidth, height: .greatestFiniteMagnitude))
        return size
    }
}

extension MessageViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholderLabel.isHidden = true
        tableViewScrollToBottom(animated: false)
        UIView.animate(withDuration: 1, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.bottomHeightConstraint.constant = 302
        }, completion: nil)
     
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
        self.bottomHeightConstraint.constant = 0
    }
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commonModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = commonModel[indexPath.row]
        if model is NSNull{
            let cell:DriverDetailChatCell = self.tableView.dequeueReusableCell(withIdentifier: "DriverDetailChatCell") as! DriverDetailChatCell
            
            let dic = driverDetail[indexPath.row]
            let dicTmp = dic as! [String: Any]
            let rideTmp = dicTmp["ride"]
            
            if rideTmp is NSNull {
                cell.bindCellWithBlankData()
            }else{
                let rideDetail = rideTmp as! Ride
                cell.bindDataWithCell(rideDetail)
            }
            return cell
            
        }else{
            return configureMessageCell(indexPath as NSIndexPath, chat: model as! ChatModel)
        }
        
    }
    
    //************** Configuration of message cell **************
    
    func configureMessageCell(_ indexPath:NSIndexPath,chat: ChatModel) -> MessageCell {
        var x : CGFloat ;
        let y : CGFloat = 13.0
        var cell : MessageCell!
        let lblMsg = CustomLabel()
        lblMsg.tag = 100
        lblMsg.backgroundColor = UIColor.yellow
        lblMsg.text = chat.messagePosting
        let font = UIFont.systemFont(ofSize: 14.0)
        lblMsg.font = font
        lblMsg.numberOfLines = 0;
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size = getMessageSize(message: chat.messagePosting)
        let timeLabel = getTimelabel()
        var additionalWidth: CGFloat = 28
        if size.width  < 70{
            additionalWidth = 24 + 18 + 65
        }
        
        if chat.personUserId != user!.id{
            cell = self.tableView.dequeueReusableCell(withIdentifier: "DriverCell") as! MessageCell
            
            x = 90
            lblMsg.backgroundColor = hexStringToUIColor(hex: "7DBE7F")
            timeLabel.textColor = UIColor.white
            lblMsg.textColor = UIColor.white
            let triangle = TriangleView(frame: CGRect(x: 0, y: 0, width: 18 , height: 26), mode: .Left)
            cell.arrowImageView.addSubview(triangle)
            
            
        }else{
            cell = self.tableView.dequeueReusableCell(withIdentifier: "PassengerCell") as! MessageCell
            lblMsg.backgroundColor = UIColor.white
            x = screenWidth-90
            x = x - size.width - additionalWidth
            let triangle = TriangleView(frame: CGRect(x: 0, y: 0, width: 18 , height: 26), mode: .Right)
            cell.arrowImageView.addSubview(triangle)
            lblMsg.textColor = hexStringToUIColor(hex: "858585")
            timeLabel.textColor = UIColor.gray
        }
        cell.selectionStyle = .none
        lblMsg.frame = CGRect(x: x, y: y, width: size.width + additionalWidth , height: size.height + 24 + 10 )
        if size.height < 30 {
            lblMsg.frame = CGRect(x: x, y: y, width: size.width + additionalWidth , height: 54 )
        }
        let removeLbl = cell.viewWithTag(100)
        if removeLbl != nil{
            removeLbl?.removeFromSuperview()
        }
        cell.addSubview(lblMsg)
        timeLabel.frame = CGRect(x: lblMsg.frame.width-44 - 65, y: lblMsg.frame.height-20, width: 40 + 65, height: 20)
        lblMsg.addSubview(timeLabel)
        cell.userImageView.image = UIImage(named: "icn_default")
        timeLabel.text = getMessageCreatedTime(dateCreated: chat.dateCreated)
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url:chat.personPhoto , useCache: true) { image in
            if let image = image{
                cell.userImageView.image = image
            }else{
                cell.userImageView.image = UIImage(named: "icn_default")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = commonModel[indexPath.row]
        if model is NSNull{
            return UITableViewAutomaticDimension
        }else{
            let  chat  = model as! ChatModel
            let size = getMessageSize(message: chat.messagePosting)
            
            if size.height < 30{
                return 80
            }
            return size.height + 26 + 24
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func getTimelabel()-> UILabel{
        let timeLabel = UILabel()
        timeLabel.backgroundColor = UIColor.clear
        timeLabel.textAlignment = .right
        timeLabel.font = UIFont.systemFont(ofSize: 12.0)
        return timeLabel
    }
    
    func getMessageCreatedTime(dateCreated: String)-> String{
        let date = stringToDate(dateCreated)
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd"
        let date1 = timeFormatter.string(from: date)
        let date2 = timeFormatter.string(from: Date())
        var timeStr = ""
        
        if date1 == date2 {
            timeFormatter.dateFormat = "HH:mm"
            timeStr = timeFormatter.string(from: date)
        }else{
            timeFormatter.locale = Locale(identifier: "de_DE")
            timeFormatter.dateFormat = "d. MMM'T'yy, HH:mm"
            timeStr = timeFormatter.string(from: date)
            timeStr =  timeStr.replacingOccurrences(of: "T", with: "'")
        }
        return timeStr
    }
    
    func stringToDate(_ string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
        if let date = dateFormatter.date(from: string) {
            return date
        }else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let  date = dateFormatter.date(from: string) {
                return date
            }
        }
        return Date()
    }
}

extension MessageViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}

