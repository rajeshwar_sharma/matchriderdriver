//
//  LeftMenuViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {
    
    @IBOutlet var profileView: UIView!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var gmailLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var menuArray: [String] = []
    var imgArr: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMenuNameList()
        userImgView.setCircleView()
        userImgView.setBorderWidth()
        profileView.addGradientColorWithBaseView()
    }
    
    func getMenuNameList(){
        let dic = Utilities.bundelResource(withName: "SideMenuList")
        let user = Utilities.getSignInUser()
        if let user = user{
            if  user.isAdmin {
                menuArray = dic["MenuAdmin"] as! [String]
                imgArr = ["ic_account.png","ic_booking_calender.png","ic_messaging.png","ic_contact_us.png","ic_help.png","ic_account.png"]
            }else{
                menuArray = dic["Menu"] as! [String]
                imgArr = ["ic_account.png","ic_booking_calender.png","ic_messaging.png","ic_contact_us.png","ic_help.png"]
            }
        }
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(LeftMenuViewController.menuStateEventOccurred(notification:)), name: NSNotification.Name(rawValue: MFSideMenuStateNotificationEvent), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: MFSideMenuStateNotificationEvent), object: nil)
    }
    
    @IBAction func profileViewTapAction(_ sender: AnyObject) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigateToViewController(vc: vc)
        
    }
    
    // ************ This method get call whenever side menu open or close *************
    func menuStateEventOccurred(notification: NSNotification) {
        let event = notification.userInfo?["eventType"] as! Int
        let user = Utilities.getSignInUser()
        if Utilities.isUserLogedIn(){
            HttpRequest.sharedRequest.cachedImages.removeAll()
            let imgPath = "\(Endpoint.getPersonImage)\(user!.id)/l"
            HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: false) { image in
                if image != nil{
                self.userImgView.image = image
                }else{
                  self.userImgView.image = UIImage(named: "icn_default")
                }
            }
        }
        if event == 0{
            getMenuNameList()
            if Utilities.isUserLogedIn(){
                nameLabel.text = "\(user!.firstName) \(user!.lastName)"
                gmailLabel.text = user!.email
                
            }
        }
    }
}

extension LeftMenuViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row < menuArray.count - 1{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
            cell.menuItemName.text = menuArray[indexPath.row]
            let imgName = imgArr[indexPath.row]
            cell.menuItemName.viewWithTag(11)?.removeFromSuperview()
            cell.imgView.image = UIImage(named: imgName)
            if indexPath.row == 2{
                setBadgeOnNotificationLabel(cell: cell)
            }
            return cell
        }else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SideMenuCell2") as! SideMenuCell2
            cell.menuItemName.text = menuArray[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func setBadgeOnNotificationLabel(cell: SideMenuCell){
        let totalBadgeLbl = BadgeLabel()
        totalBadgeLbl.tag = 11
        getTotalNotificationCount(totalBadgeLbl: totalBadgeLbl)
        totalBadgeLbl.text = "\(Store.sharedStore.totalNotificationCount!)"
        totalBadgeLbl.isHidden = true
        totalBadgeLbl.frame = CGRect(x: cell.menuItemName.frame.width-45, y: 2, width: 25, height: 25)
        cell.menuItemName.addSubview(totalBadgeLbl)
        if Store.sharedStore.totalNotificationCount > 0{
            totalBadgeLbl.isHidden = false
        }
    }
    
    func getTotalNotificationCount(totalBadgeLbl :BadgeLabel){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getTotalDriverNotificationsCount) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                var count = 0
                _ = dataArray.map { json in
                    count = json.int!
                }
                Store.sharedStore.totalNotificationCount = count
                if count == 0{
                    totalBadgeLbl.isHidden = true
                }else{
                    totalBadgeLbl.isHidden = false
                }
                totalBadgeLbl.text = "\(count)"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var viewController: UIViewController!
        if indexPath.row == 0{
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        }
        else if indexPath.row == 1{
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "HolidaysViewController") as! HolidaysViewController
        }
        else if indexPath.row == 2{
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        }
        else if indexPath.row == 3{
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        }
        else if indexPath.row == 4{
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        }
        else if indexPath.row == 5{
            let user = Utilities.getSignInUser()
            if user!.isAdmin {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ImpersonateNotification"), object: nil)
                Utilities.toggelMenuToLeft()
                return
            }else{
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "ImpressumViewController") as! ImpressumViewController
            }
            
            
        }
        else if indexPath.row == 6{
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "ImpressumViewController") as! ImpressumViewController
        }
        
        if viewController == nil{
            return
        }
        navigateToViewController(vc: viewController)
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if indexPath.row < menuArray.count-1{
            let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SideMenuCell
            cell.menuItemName.textColor = hexStringToUIColor(hex: MatchRiderColor)
            cell.imgView.changeImageColor(color: hexStringToUIColor(hex: MatchRiderColor))
            }else{
            let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SideMenuCell2
            cell.menuItemName.textColor = hexStringToUIColor(hex: MatchRiderColor)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if indexPath.row < menuArray.count-1{
            let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SideMenuCell
            cell.menuItemName.textColor = UIColor.black
            cell.imgView.changeImageColor(color: UIColor.black)
        }else{
            let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SideMenuCell2
            cell.menuItemName.textColor = UIColor.black
        }
    }
    
    func navigateToViewController(vc: UIViewController) {
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        (self.menuContainerViewController.centerViewController as AnyObject).pushViewController(vc, animated: true)
        Utilities.toggelMenuToLeft()
    }
    
    
}
