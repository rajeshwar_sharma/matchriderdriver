//
//  RoutesViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 17/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class RoutesViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var superHighwayModel: [SuperHighway] = []
    var networkViewObj: NetworkView!
    let noDataView = NoDataView.initView()
    var descriptionLabel: FRHyperLabel!
    
    var redirectSuperHighwaymodel: SuperHighway!
    var redirectDirection: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Routen"
        setBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        checkNetworkAvailabilty()
    }
    
    //MARK:- Custom Method
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            getPreconfiguredHighwaysList()
        }
        networkViewObj?.delegate = self
    }
    
    //MARK:- Service Api calling
    
    func getPreconfiguredHighwaysList(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getPreconfiguredHighwaysWithCustomization) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                let superHighway = payload.map { json in
                    return SuperHighway(json: json)
                }
                self.superHighwayModel = superHighway
                if self.superHighwayModel.count == 0{
                    self.tableView.backgroundView = self.noDataView
                    self.noDataView.isHidden = false
                    self.noDataView.setMessage(message: "")
                    self.setNoRoutesAvailableMessage()
                    
                }else{
                    self.noDataView.isHidden = true
                }
                self.tableView.reloadData()
            }
        }
    }
  
    
    func setNoRoutesAvailableMessage(){
        descriptionLabel = FRHyperLabel()
        let string = "Bitte kontaktiere uns um deine regelmäßige Fahrt einzustellen."
        let attributes = [NSForegroundColorAttributeName: hexStringToUIColor(hex: "686868"),
                          NSFontAttributeName: UIFont.systemFont(ofSize: 17)]
        descriptionLabel.attributedText = NSAttributedString(string: string, attributes: attributes)
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
         self.navigateToContactViewController()
        }
        descriptionLabel.font = UIFont.systemFont(ofSize: 17)
        descriptionLabel.setLinksForSubstrings(["kontaktiere uns"], withLinkHandler: handler)
        descriptionLabel.frame = CGRect(x: 10, y: 207, width: screenWidth-20, height: 60)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        self.noDataView.addSubview(descriptionLabel)
    }
    
    func navigateToContactViewController(){
      let contactVC = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "", message: "Bitte füge erst ein Auto hinzu.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Nein", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Hinzufügen", style: UIAlertActionStyle.default, handler: { action in
            if appDelegate.carDetail == nil {
                self.getDriverCarDetails()
            }else{
                self.navigateToCarDetailViewController()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension RoutesViewController: UITableViewDelegate,UITableViewDataSource{
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return superHighwayModel.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RouteCustomCell") as! RouteCustomCell
        let superHighway = superHighwayModel[indexPath.row]
        cell.startLabel.text = superHighway.startHighwayDescription.trim()
        cell.destinationLabel.text = superHighway.destHighwayDescription.trim()
       
        let leftViewTap = UITapGestureRecognizer(target: self, action: #selector(self.leftViewTapGestureAction(_:)))
        cell.leftView.addGestureRecognizer(leftViewTap)
        
        let rightViewTap = UITapGestureRecognizer(target: self, action: #selector(self.rightViewTapGestureAction(_:)))
        cell.rightView.addGestureRecognizer(rightViewTap)
        
        return cell
    }
    
    func leftViewTapGestureAction(_ gestureRecognizer: UITapGestureRecognizer) {
        let cell = gestureRecognizer.view?.superview?.superview as! RouteCustomCell
        let indexPath = tableView.indexPath(for: cell)
        let superHighway = superHighwayModel[indexPath!.row]
        redirectSuperHighwaymodel = superHighway
        redirectDirection = 1
        navigateToOfferViewController(direction: 1, superHighway: superHighway)
    }
    func rightViewTapGestureAction(_ gestureRecognizer: UITapGestureRecognizer) {
        let cell = gestureRecognizer.view?.superview?.superview as! RouteCustomCell
        let indexPath = tableView.indexPath(for: cell)
        let superHighway = superHighwayModel[indexPath!.row]
        redirectSuperHighwaymodel = superHighway
        redirectDirection = 2
        navigateToOfferViewController(direction: 2, superHighway: superHighway)
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func getRoutesDescription(superHighway: SuperHighway) -> String{
        return "\(superHighway.startHighwayDescription) - \(superHighway.destHighwayDescription)"
    }
    //appDelegate.vehicleId
    func navigateToOfferViewController(direction:Int, superHighway: SuperHighway){
        
        if appDelegate.carDetail == nil || appDelegate.carDetail.id == 0{
            showAlert()
            
        }else{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferRideViewController") as! OfferRideViewController
        vc.directionIndex = direction
        vc.superHighway = superHighway
        self.navigationController?.pushViewController(vc, animated: true)
      }
    }

    func navigateToCarDetailViewController(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        vc.carDetail = appDelegate.carDetail
        vc.delegate = self
        vc.fromRouteFlag = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func getDriverCarDetails(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriverCarDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let car = dataArray.map { json in
                    return Car(json: json)
                }
                appDelegate.carDetail = car[0]
                self.navigateToCarDetailViewController()
            }
            self.view.hideLoader()
        }
    }
}

extension RoutesViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}

extension RoutesViewController: ChooseDirectionViewDelegate{
    func getSelectedDirection(direction: Int, superHighway: SuperHighway) {
        navigateToOfferViewController(direction: direction, superHighway: superHighway)
    }
}

extension RoutesViewController: CarDetailViewControllerDelegate{
    func reloadCarDetail() {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferRideViewController") as! OfferRideViewController
//        vc.directionIndex = redirectDirection
//        vc.superHighway = redirectSuperHighwaymodel
//        self.navigationController?.pushViewController(vc, animated: true)
        redirectSuperHighwaymodel.seats = appDelegate.carDetail.seats
        navigateToOfferViewController(direction: redirectDirection, superHighway: redirectSuperHighwaymodel)
    }
}
