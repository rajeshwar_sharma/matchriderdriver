//
//  HolidaysViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 12/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class HolidaysViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addView: UIView!
    
    var holidays: [Holiday]! = []
    let noDataView = NoDataView.initView()
    var reloadDataFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Abwesenheitsplaner"
        setBackButton()
        getCommuteHolidays()
        addView.setCircleView()
        tableView.hideEmptyRow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if reloadDataFlag{
         reloadDataFlag = false
         getCommuteHolidays()
        }
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
    }
    
    //MARK:- Custom Method
    
    func navigateToCalendarScreen(holiday: Holiday?){
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        if let holiday = holiday{
            vc.holiday = holiday
        }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Custom method action
    
    @IBAction func addViewTapAction(_ sender: AnyObject) {
        navigateToCalendarScreen(holiday: nil)
    }
    
    //MARK:- Service api calling
    
    func getCommuteHolidays(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getCommuteHolidays) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let holiday = dataArray.map { json in
                    return Holiday(json: json)
                }
                self.holidays = holiday
                if self.holidays.count == 0{
                    self.tableView.backgroundView = self.noDataView
                    self.noDataView.isHidden = false
                    self.noDataView.setMessage(message: "Du hast noch keinen Urlaub geplant.")
                }else{
                     self.noDataView.isHidden = true
                }
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
}

extension HolidaysViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return holidays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "HolidaysCell") as! HolidaysCell
        let holiday = holidays[indexPath.row]
        cell.startLabel.text = "Vom: \(getCompleteDateFromDate(date: holiday.holidayStart!))"
        cell.endLabel.text = "bis zum: \(getCompleteDateFromDate(date: holiday.holidayEnd!))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToCalendarScreen(holiday: holidays[indexPath.row])
     }
}

extension HolidaysViewController: CalendarViewControllerDelegate{
    func reloadData() {
        self.reloadDataFlag = true
    }
}
