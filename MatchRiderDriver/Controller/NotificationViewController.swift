//
//  NotificationViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 06/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var segmentedControll: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    
    var threadsData : [ThreadsModel] = []
    let messageBadgeLbl = BadgeLabel()
    let ratingBadgeLbl = BadgeLabel()
    var rides = [Ride]()
    var networkViewObj: NetworkView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        self.title = "Nachrichten"
        setBadgeOnSegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
       // navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        checkNetworkAvailabilty()
    }
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: contentView)
        if networkViewObj.isNetworkAvailable! {
            getRidesNotRated(Store.sharedStore.token!)
            getUnreadMessageNotificationCount(Store.sharedStore.token!)
            getNotRatedRideNotificationCount(Store.sharedStore.token!)
            getThreadsByPerson(Store.sharedStore.token!)
        }
        networkViewObj?.delegate = self
    }
    
    //******* Calling api to get Threads by Person *******
    
    fileprivate func getThreadsByPerson(_ token: Token) {
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getThreadsByDriver) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                
                let threads = dataArray.map { json in
                    return ThreadsModel(json: json)
                }
                self.threadsData = threads
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
     
    fileprivate func getRidesNotRated(_ token: Token) {
        
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriverRidesNotRated) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                
                if (json["Payload"].null != nil) {
                    self.view.hideLoader()
                    return
                }
                
                let payload = json["Payload"].array!
                
                let rides = payload.map { json in
                    return Ride(json: json)
                }
                self.rides = rides
                self.tableView.reloadData()
                
            }
            self.view.hideLoader()
        }
    }
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        tableView.reloadData()
    }
    
    func setBadgeOnSegmentedControl()  {
        
        messageBadgeLbl.isHidden = true
        self.view.addSubview(messageBadgeLbl)
        self.view.addSubview(ratingBadgeLbl)
        ratingBadgeLbl.isHidden = true
    }
    
    func getUnreadMessageNotificationCount(_ token: Token){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getUnreadThreadCount) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                var count = 0
                _ = dataArray.map { json in
                    count = json.int!
                }
                self.messageBadgeLbl.text = "\(count)"
                if count > 0{
                    self.messageBadgeLbl.isHidden = false
                }else {
                    self.messageBadgeLbl.isHidden = true
                }
            }
        }
    }
    
    func getNotRatedRideNotificationCount(_ token: Token){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getNotRatedRideCount) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                var count = 0
                _ = dataArray.map { json in
                    count = json.int!
                }
                self.ratingBadgeLbl.text = "\(count)"
                if count > 0{
                    self.ratingBadgeLbl.isHidden = false
                }else {
                    self.ratingBadgeLbl.isHidden = true
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        messageBadgeLbl.frame = CGRect(x: screenWidth/2-30, y: segmentedControll.frame.minY+4, width: 20, height: 20)
        ratingBadgeLbl.frame = CGRect(x: segmentedControll.frame.maxX-30, y: segmentedControll.frame.minY+4, width: 20, height: 20)
    }
}

extension NotificationViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControll.selectedSegmentIndex == 0{
            if threadsData.count == 0{
                tableView.showNoNewsView()
                return threadsData.count
            }
            tableView.backgroundView = nil
            return threadsData.count
        }else{
            if rides.count == 0{
                tableView.showNoNotificationView()
                return rides.count
            }
            tableView.backgroundView = nil
            return rides.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .singleLine
        if segmentedControll.selectedSegmentIndex == 0{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
            let thread = threadsData[indexPath.row]
            cell.bindDataWithCell(thread: thread)
            return cell
        }else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "RatingDetailCell") as! RatingDetailCell
            let ride = rides[indexPath.row]
            cell.bindDataWithCell(ride: ride)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControll.selectedSegmentIndex == 0{
            return 120
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segmentedControll.selectedSegmentIndex == 0{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
            let thread = threadsData[indexPath.row]
            controller.messageThreadId = thread.messageThreadId
            controller.showDriverDetail = false
            controller.passengerName = thread.personName
            controller.passengerId = thread.personId
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
            controller.ride = rides[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension NotificationViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}
