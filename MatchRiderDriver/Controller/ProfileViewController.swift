//
//  ProfileViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
@objc protocol ProfileViewControllerDelegate {
    @objc optional func updateUserDescription()
    @objc optional func updateUserImage()
}

class ProfileViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    var userImgView: UIImageView!
    let rightButton = UIButton(type: .custom)
    var userImgDelegate: ProfileViewControllerDelegate!
    var descriptionDelegate: ProfileViewControllerDelegate!
    var reloadTableCheck = false
    var carDetail: Car!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Konto"
        setBackButton()
        rightButton.tag = 1
        rightButton.changeImageColor(color: UIColor.white)
        rightButton.setImage(UIImage(named: "ic_edit"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButton.addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
        let rightBtnItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.setRightBarButton(rightBtnItem, animated: true)
        tableView.register(UINib(nibName: "PointsEarnedCell", bundle: nil), forCellReuseIdentifier: "PointsEarnedCell")
        tableView.register(UINib(nibName: "SignOutCustomCell", bundle: nil), forCellReuseIdentifier: "SignOutCustomCell")
        
        getAccountDetails()
        getDriverCarDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if reloadTableCheck{
            reloadTableCheck = false
            tableView.reloadData()
        }
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
    }
    
    func rightButtonAction(_ sender: UIButton){
        userImgDelegate.updateUserImage!()
        descriptionDelegate.updateUserDescription!()
        if rightButton.tag == 1{
            rightButton.setImage(UIImage(named: "ic_done"), for: .normal)
            rightButton.changeImageColor(color: UIColor.white)
            rightButton.tag = 2
        }else{
            rightButton.setImage(UIImage(named: "ic_edit"), for: .normal)
            rightButton.changeImageColor(color: UIColor.white)
            rightButton.tag = 1
            updateUserDetail()
        }
    }
    
    func resetValueWhenUpdationFailed(){
        userImgDelegate.updateUserImage!()
        descriptionDelegate.updateUserDescription!()
        if rightButton.tag == 1{
            rightButton.setImage(UIImage(named: "ic_done"), for: .normal)
            rightButton.changeImageColor(color: UIColor.white)
            rightButton.tag = 2
        }else{
            rightButton.setImage(UIImage(named: "ic_edit"), for: .normal)
            rightButton.changeImageColor(color: UIColor.white)
            rightButton.tag = 1
        }
    }
    
    @IBAction func logOutTapAction(_ sender: AnyObject) {
        //Utilities.logout()
    }
    
    func updateUserDetail(){
        self.view.showLoader()
        var base64: AnyObject = NSNull()
        if let photo = UserDetail.sharedInstance.image {
            base64 = UIImagePNGRepresentation(photo)!.base64EncodedString(options: .lineLength64Characters) as AnyObject
        }
        let params = [
            "firstname": NSNull(),
            "lastname": NSNull(),
            "description": UserDetail.sharedInstance.aboutDescription,
            "photo": base64,
            "ext": "png"
            ] as [String : Any]
        
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateAccountDetails) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                Utilities.saveUserData(user: data)
                HttpRequest.sharedRequest.cachedImages.removeAll()
            }else{
                self.resetValueWhenUpdationFailed()
            }
            self.view.hideLoader()
        }
    }
    
    func getDriverCarDetails(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriverCarDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let car = dataArray.map { json in
                    return Car(json: json)
                }
                self.carDetail = car[0]
                appDelegate.carDetail = car[0]
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
    
    func getAccountDetails(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getAccountDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                
                Utilities.saveUserData(user: data)
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
    
}

extension ProfileViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileViewCell") as! ProfileViewCell
            userImgDelegate = cell
            cell.configureCell()
            return cell
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "AboutDetailCell") as! AboutDetailCell
            descriptionDelegate = cell
            cell.configureCell()
            return cell
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            setBadgeOnNotificationLabel(cell: cell)
            return cell
        case 3:
            let user = Utilities.getSignInUser()
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "PointsEarnedCell") as! PointsEarnedCell
            cell.pointLabel.text = "\(user!.totalPointsEarned)"
           // cell.distanceLabel.text = "\(user!.totalPassengerKMs) km"
            cell.distanceLabel.text = String(format: "%.1f km", user!.totalPassengerKMs)
            return cell
            
        case 4:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CarImageCell") as! CarImageCell
            let tap = UITapGestureRecognizer(target: self, action:#selector(editCarDetailTapAction(_:)))
            cell.editCarDetail.addGestureRecognizer(tap)
            if carDetail != nil{
                cell.configureCell(car: carDetail)
            }
            return cell
        case 5:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SignOutCustomCell") as! SignOutCustomCell
            
            return cell
            
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 210
        case 1:
            return 120
        case 2:
            return 55
        case 3:
            return 85
        case 4:
            return 220
        case 5:
            return 55
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = hexStringToUIColor(hex: "EBEBF1")
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        lineView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 1)
        view.addSubview(lineView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0.01
        case 1:
            return 0.01
        case 2,3,4,5:
            return 16
//        case 3:
//            return 16
//        case 4:
//            return 16
//        case 5:
//            return 16
            
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2{
            reloadTableCheck = true
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 5{
            Utilities.logout()
        }
    }
    
    func editCarDetailTapAction(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        vc.carDetail = carDetail
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setBadgeOnNotificationLabel(cell: ProfileCell){
        let totalBadgeLbl = BadgeLabel()
        totalBadgeLbl.tag = 11
        getTotalNotificationCount(totalBadgeLbl: totalBadgeLbl)
        totalBadgeLbl.text = "\(Store.sharedStore.totalNotificationCount!)"
        totalBadgeLbl.isHidden = true
        totalBadgeLbl.frame = CGRect(x: cell.nameLabel.frame.width-45, y: 2, width: 25, height: 25)
        cell.nameLabel.addSubview(totalBadgeLbl)
        if Store.sharedStore.totalNotificationCount != 0{
            totalBadgeLbl.isHidden = false
        }
    }
    
    func getTotalNotificationCount(totalBadgeLbl :BadgeLabel){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getTotalDriverNotificationsCount) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                var count = 0
                _ = dataArray.map { json in
                    count = json.int!
                }
                Store.sharedStore.totalNotificationCount = count
                if count == 0{
                    totalBadgeLbl.isHidden = true
                }else{
                    totalBadgeLbl.isHidden = false
                }
                totalBadgeLbl.text = "\(count)"
            }
        }
    }
    
    func navigateToViewController(vc: UIViewController) {
        (self.menuContainerViewController.centerViewController as AnyObject).pushViewController(vc, animated: true)
        Utilities.toggelMenuToLeft()
    }
}

extension ProfileViewController: CarDetailViewControllerDelegate{
    func reloadCarDetail() {
        getDriverCarDetails()
    }
}
