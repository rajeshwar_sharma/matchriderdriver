//
//  MapViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
import QuartzCore

protocol MapViewControllerDelegate {
    func updateMatchPointDetail(routeSuperHighway: SuperHighwayRoute!, directionIndex: Int)
}

class MapViewController: BaseViewController {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var startLabel: UITextField!
    @IBOutlet weak var destinationLabel: UITextField!
    @IBOutlet weak var goButton: UIButton!
    
    var start: Matchpoint?
    var destination: Matchpoint?
    var routeSuperHighway: SuperHighwayRoute!
    var upRouteSuperHighway: SuperHighwayRoute!
    var reverseRouteSuperHighway: SuperHighwayRoute!
    var offRoute: SuperHighwayRoute!
    let selectionView = UIView()
    let locationManager =  CLLocationManager()
    var delegate: MapViewControllerDelegate! = nil
    var directionIndex: Int!
    var rightBarButton: UIButton = UIButton(type: UIButtonType.custom)
    var changeLocationId = false
    let offerRideData = OfferRideData.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        setSwapButton()
        goButton.isEnabled = false
        goButton.alpha = 0.5
        let content = Bundle.main.loadNibNamed("MatchpointCustomView", owner: self, options: nil)?.first as! UIView
        selectionView.frame = content.frame
        selectionView.addSubview(content)
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.add(routeSuperHighway.polyline)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateRoute()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if changeLocationId {
            let tmp = Store.sharedStore.startLocationId
            Store.sharedStore.startLocationId = Store.sharedStore.destinationLocationId
            Store.sharedStore.destinationLocationId = tmp
        }
    }
    
    // MARK:- Custom method
    
    func setSwapButton()
    {
        let buttonImage = UIImage(named: "ic_swap");
        rightBarButton.tag = directionIndex
        rightBarButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        rightBarButton.setImage(buttonImage, for: UIControlState.normal)
        rightBarButton.addTarget(self, action: #selector(self.swapBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: rightBarButton)
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: false);
    }
    
    func setStartMatchPoint(matchPoint: Matchpoint){
        Store.sharedStore.startLocationId = "\(matchPoint.id)"
        Store.sharedStore.startLocationMatchPoint = matchPoint
        if rightBarButton.tag == 1{
            offerRideData.routeAStartPreconfiguredRouteLocationId = matchPoint.id
        }else{
            offerRideData.routeBStartPreconfiguredRouteLocationId = matchPoint.id
        }
    }
    
    func setDestinationMatchPoint(matchPoint: Matchpoint){
        Store.sharedStore.destinationLocationId = "\(matchPoint.id)"
        Store.sharedStore.destinationLocationMatchPoint = matchPoint
        if rightBarButton.tag == 1{
            offerRideData.routeADestPreconfiguredRouteLocationId = matchPoint.id
        }else{
            offerRideData.routeBDestPreconfiguredRouteLocationId = matchPoint.id
        }
    }

    func updateRoute() {
        let title = RouteDirectionString(routeSuperHighway.startLocationCityShort, to: routeSuperHighway.destinationLocationCityShort)
        self.title = title
        self.mapView.showAnnotations(routeSuperHighway.matchpoints, animated: false)
    }
    
    func updateAnnotation(_ matchpoint: Matchpoint) {
        mapView.deselectAnnotation(matchpoint, animated: true)
        for annotation in mapView.annotations {
            guard let matchpoint = annotation as? Matchpoint else {
                continue
            }
            let view = mapView.view(for: annotation)
            view?.image = UIImage(named: "BetweenPoint")!
            view?.layer.zPosition = 1
            if matchpoint == start {
                view?.image = UIImage(named: "StartPoint")!
                view?.layer.zPosition = 2
            }
            if matchpoint == destination {
                view?.image = UIImage(named: "EndPoint")!
                view?.layer.zPosition = 2
            }
        }
        updateGoState()
    }
    
    func updateGoState() {
        if start != nil{
            startLabel.text = "\(start!.descriptionInfo), \(start!.city)"
        }
        if destination != nil{
            destinationLabel.text =  "\(destination!.descriptionInfo), \(destination!.city)"
        }
        
        if start != nil && destination != nil {
            goButton.isEnabled = true
            goButton.alpha = 1
        } else {
            goButton.isEnabled = false
            goButton.alpha = 0.5
        }
    }

    
    // MARK:- Custom method action
    
    @IBAction func toggleRoute() {
        start = nil
        destination = nil
        let route = self.routeSuperHighway
        let offRoute = self.offRoute
        self.routeSuperHighway = offRoute
        self.offRoute = route
        updateRoute()
        updateGoState()
    }
    
    
    func swapBtnClicked(sender: UIButton!)
    {
        changeLocationId = true
        self.mapView.remove(routeSuperHighway.polyline)
        mapView.removeAnnotations(mapView.annotations)
        start = nil
        destination = nil
        if sender.tag == 1{
            sender.tag = 2
            routeSuperHighway = reverseRouteSuperHighway
        }else{
            sender.tag = 1
            routeSuperHighway = upRouteSuperHighway
        }
        
        
        self.mapView.add(routeSuperHighway.polyline)
        let tmp = Store.sharedStore.startLocationId
        Store.sharedStore.startLocationId = Store.sharedStore.destinationLocationId
        Store.sharedStore.destinationLocationId = tmp
        updateRoute()
        
    }
    
    @IBAction func showSatellite(_ sender: UIBarButtonItem) {
        if mapView.mapType == .satellite {
            sender.tintColor = UIColor.lightGray
            mapView.mapType = .standard
            return
        }
        if mapView.mapType == .standard {
            sender.tintColor = hexStringToUIColor(hex: MatchRiderColor)
            mapView.mapType = .satellite
            return
        }
    }
    
    @IBAction func selectStart() {
        
        let startMatchPointTmp = mapView.selectedAnnotations.first! as! Matchpoint
        if startMatchPointTmp == destination {
            destination = nil
        }
        if let destination = destination {
            
            if destination.delay < startMatchPointTmp.delay {
                mapView.deselectAnnotation(startMatchPointTmp, animated: true)
                let controller = ActionControllerWrongDirection {
                    // self.toggleRoute()
                }
                present(controller, animated: true, completion: nil)
            }else{
                self.start = startMatchPointTmp
                updateAnnotation(startMatchPointTmp)
            }
        }
        
        if destination == nil{
            start = startMatchPointTmp
            updateAnnotation(startMatchPointTmp)
        }
    }
    
    
    @IBAction func selectDestination() {
        let destinationMatchPointTmp = mapView.selectedAnnotations.first! as! Matchpoint
        if destinationMatchPointTmp == start {
            start = nil
        }
        
        if let start = start {
            if destinationMatchPointTmp.delay < start.delay {
                mapView.deselectAnnotation(destinationMatchPointTmp, animated: true)
                let controller = ActionControllerWrongDirection {
                    // self.toggleRoute()
                }
                present(controller, animated: true, completion: nil)
            }else{
                self.destination = destinationMatchPointTmp
                updateAnnotation(destinationMatchPointTmp)
                
            }
        }
        if start == nil{
            destination = destinationMatchPointTmp
            updateAnnotation(destinationMatchPointTmp)
        }
    }
    
    
    @IBAction func showMatchpointDetails() {
        guard let matchpoint = mapView.selectedAnnotations.first as? Matchpoint else {
            return
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPointController") as! MatchPointController
        vc.matchpoint = matchpoint
        let navController = UINavigationController(rootViewController: vc)
        navController.setThemeDefaultColor()
        self.present(navController, animated:true, completion: nil)
    }
    
    
    @IBAction func goButtonAction(_ sender: Any) {
        setStartMatchPoint(matchPoint: start!)
        setDestinationMatchPoint(matchPoint: destination!)
        delegate.updateMatchPointDetail(routeSuperHighway: routeSuperHighway, directionIndex: rightBarButton.tag)
        changeLocationId = false
        self.navigationController?.popViewController(animated: true)
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = hexStringToUIColor(hex: MatchRiderColor)
        polylineRenderer.lineWidth = 4
        polylineRenderer.lineCap = .square
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let matchpoint = annotation as? Matchpoint else {
            return nil
        }
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "Matchpoint")
        view.image = UIImage(named: "BetweenPoint")!
        view.layer.zPosition = 1
        if matchpoint == start || Store.sharedStore.startLocationId == "\(matchpoint.id)"{
            view.image = UIImage(named: "StartPoint")!
            view.layer.zPosition = 2
            start = matchpoint
            
        }
        
        if matchpoint == destination || Store.sharedStore.destinationLocationId == "\(matchpoint.id)"{
            
            view.image = UIImage(named: "EndPoint")!
            view.layer.zPosition = 2
            destination = matchpoint
        }
        
        if start != nil && destination != nil{
            
            if destination!.delay < start!.delay {
                let destinationView = mapView.view(for: destination!)
                destinationView?.image = UIImage(named: "BetweenPoint")
                destinationView?.layer.zPosition = 1
                destination = nil
            }
           updateGoState()
        }
        view.centerOffset = CGPoint(x: 0, y: -18)
        view.canShowCallout = true
        view.rightCalloutAccessoryView = selectionView
        return view
    }
}
