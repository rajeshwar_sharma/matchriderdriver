//
//  CarDetailViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

protocol CarDetailViewControllerDelegate {
    func reloadCarDetail()
}

class CarDetailViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var carDetail: Car!
    var selectedcCarDetail: Car!
    var delegate: CarDetailViewControllerDelegate! = nil
    var fromRouteFlag = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fahrzeug"
        setBackButton()
    }
}


extension CarDetailViewController: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if carDetail == nil{
            return 0
        }
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CarDetailImageCell") as! CarDetailImageCell
            if carDetail.photoCar != nil{
                cell.carImageView.image = carDetail.photoCar
                
            }else{
                let imgPath = carDetail.photo
                self.view.showLoader()
                HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: false) { image in
                    if image != nil{
                    cell.carImageView.image = image
                    self.carDetail.photoCar = image
                    }
                    self.view.hideLoader()
                }
            }
            return cell
        case 1,2,3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CarDetailCell") as! CarDetailCell
            cell.detailTextField.delegate = self
            if indexPath.row == 1{
                cell.detailLabel.text = "Marke"
                cell.detailTextField.text = carDetail.make
                cell.detailTextField.tag = 1
                
            } else if indexPath.row == 2{
                cell.detailLabel.text = "Modell"
                cell.detailTextField.text = carDetail.model
                cell.detailTextField.tag = 2
            } else{
                cell.detailLabel.text = "Kennzeichen"
                cell.detailTextField.text = carDetail.license
                cell.detailTextField.tag = 3
            }
            return cell
            
        case 4:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CarColorCell") as! CarColorCell
            if carDetail.colorRGBA.isEmpty{
                cell.colorView.isHidden = true
                cell.chooseVehicleColor.isHidden = false
            }else{
                cell.colorView.isHidden = false
                cell.chooseVehicleColor.isHidden = true
                cell.colorView.backgroundColor = hexStringToUIColor(hex: carDetail.colorRGBA)
            }
            
            return cell
        case 5:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CarDetailSizeCell") as! CarDetailCell
            cell.detailLabel.text = "Sitze"
            cell.detailTextField.text = "\(carDetail.seats)"
            cell.detailTextField.tag = 5
            cell.detailTextField.delegate = self
            return cell
        case 6:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SendCarDetailCell") as! SendCarDetailCell
            
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 220                                                                               
        case 1,2,3:
            return 72
        case 4,5,6:
            return 55
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            takePicture()
            
        }else if indexPath.row == 4{
            let colorPickerView =  Bundle.main.loadNibNamed("ColorPickerView", owner: nil, options: nil)?[0] as! ColorPickerView
            colorPickerView.delegate = self
            colorPickerView.reloadData(color: carDetail.colorRGBA)
            colorPickerView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
            self.navigationController?.view.addSubview(colorPickerView)
            
        }else if indexPath.row == 6{
            print(carDetail)
            updateCarDetail()
        }
        
    }
    
    func updateCarDetail(){
        
//        if !Validation.validateVehicleDetail(carDetail){
//            return
//        }
        
        var base64: AnyObject = NSNull()
        if let photo = carDetail.photoCar {
            
            base64 = UIImagePNGRepresentation(photo)!.base64EncodedString(options: .lineLength64Characters) as AnyObject
        }
        
        let params: [String : Any] = [
            "Id": carDetail.id,
            "make": carDetail.make,
            "model": carDetail.model,
            "color": carDetail.color,
            "colorRGBA": carDetail.colorRGBA,
            "seats": carDetail.seats,
            "license": carDetail.license,
            "photo": base64,
            "photoExt": ".png"
        ]
        
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDriverVehicleDetails) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                
                if self.fromRouteFlag{
                    self.getDriverCarDetails()
                }else{
                    
                self.showSuccessMessage(message: "Autobeschreibung erfolgreich aktualisiert.")
                self.navigationController?.popViewController(animated: true)
                self.delegate.reloadCarDetail()
                self.view.hideLoader()
            }
            }else{
                self.view.hideLoader()
            }
        }
    }
    
    func takePicture() {
        //let vc = Utilities.getTopViewController()
        let controller = UIAlertController(title: "Foto", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel) { action in
            controller.dismiss(animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Kamera", style: .default) { action in
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.cameraDevice = .front
            picker.showsCameraControls = true
            picker.allowsEditing = false
            picker.delegate = self
            self.navigationController?.present(picker, animated: false, completion: nil)
        }
        let library = UIAlertAction(title: "Fotosammlung", style: .default) { action in
            let picker = UIImagePickerController()
            picker.navigationBar.isTranslucent = false
            picker.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white
            ]
            picker.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
            picker.navigationBar.tintColor = UIColor.white;
            picker.sourceType = .photoLibrary
            picker.allowsEditing = false
            picker.delegate = self
            self.navigationController?.present(picker, animated: true, completion: nil)
        }
        controller.addAction(cancel)
        controller.addAction(camera)
        controller.addAction(library)
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    //************* TextField Delegate Method ************
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1:
            carDetail.make = textField.text!
        case 2:
            carDetail.model = textField.text!
            
        case 3:
            carDetail.license = textField.text!
            
        case 5:
            if textField.text!.isEmpty{
                carDetail.seats = 0
            }else{
                carDetail.seats = Int(textField.text!)!
            }
            
            
        default:
            break
        }
    }
    
    func getDriverCarDetails(){
        
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getDriverCarDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            self.view.hideLoader()
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let car = dataArray.map { json in
                    return Car(json: json)
                }
                appDelegate.carDetail = car[0]
                self.showSuccessMessage(message: "Autobeschreibung erfolgreich aktualisiert.")
                self.navigationController?.popViewController(animated: false)
                self.delegate.reloadCarDetail()
                
            }
            
        }
    }
}

extension CarDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        //let size = CGSize(width: 600, height: 400)
        let size = CGSize(width: 500, height: 750)
        carDetail.photoCar = image.resizeImage(targetSize: size)
        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    }
    
}

extension CarDetailViewController: ColorPickerViewDelegate{
    func getSelectedColor(dic: NSDictionary) {
        
        let colorCode = dic.object(forKey: "Code") as! String
        let colorName = dic.object(forKey: "Name") as! String
        carDetail.colorRGBA = colorCode
        carDetail.color = colorName
        tableView.reloadData()
    }
}
