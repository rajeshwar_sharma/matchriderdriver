//
//  MatchPointMapViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 08/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
class MatchPointMapViewController: BaseViewController {
    
    @IBOutlet var mapView: MKMapView!
    var matchPoint: MatchPointModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        setBackButton()
        self.title = "Match Point Map"
        showMatchPoint()
    }
    
    func showMatchPoint(){
        let annotation = Annotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: matchPoint.latitude, longitude: matchPoint.longitude)
        annotation.title = matchPoint.locationNameWithCity
        mapView.addAnnotation(annotation)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.7, longitudeDelta: 0.7))
        self.mapView.setRegion(region, animated: true)
    }
}

extension MatchPointMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let matchpoint = annotation as? Annotation else {
            return nil
        }
        let view = MKAnnotationView(annotation: matchpoint, reuseIdentifier: "Matchpoint")
        view.image = UIImage(named: "markerMP")!
        view.centerOffset = CGPoint(x: 0, y: -18)
        view.canShowCallout = true
        return view
    }
}

