//
//  ImpressumViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class ImpressumViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Impressum, AGB, Datenschutz"
        setBackButton()
        webView.delegate = self
        self.view.showLoader()
        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "AGB", ofType: "docx")!)))
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.view.hideLoader()
    }
}
