//
//  RideDetailViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 31/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
protocol RideDetailViewControllerDelegate {
    func cancelTripStatus(success: Bool)
}

class RideDetailViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    var upcomingRide: Ride!
    var delegate: RideDetailViewControllerDelegate!
    var networkViewObj: NetworkView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fahrtdetails"
        if upcomingRide != nil{
          sortPassengerList()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailViewController.rideSpecificNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        
        tableView.register(UINib(nibName: "DriverDetailCell", bundle: nil), forCellReuseIdentifier: "DriverDetailCell")
        tableView.register(UINib(nibName: "FellowDriverCell", bundle: nil), forCellReuseIdentifier: "FellowDriverCell")
        if DataContainer.sharedInstance.rideSpecificId != nil{
            checkNetworkAvailabilty()
        }
            
        setBackButton()
    }
    func sortPassengerList(){
        upcomingRide.passengers!.sort(by: { $0.driverTimeAtStopDate!.compare($1.driverTimeAtStopDate!) == ComparisonResult.orderedAscending })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        }

    func rideSpecificNotificationReceiver(_ notification: NSNotification){
        
        if DataContainer.sharedInstance.rideSpecificId! == upcomingRide.id || DataContainer.sharedInstance.reloadRideDetail{
            DataContainer.sharedInstance.reloadRideDetail = false
          checkNetworkAvailabilty()
        }
    }
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
           getRideDetail()
        }
        networkViewObj?.delegate = self
    }
    
    func getRideDetail() {
        let rideId = DataContainer.sharedInstance.rideSpecificId
        if rideId == nil{
            return
        }
        self.view.showLoader()
        let components = [
            Endpoint.getDriverRideDetails,
            "\(rideId!)"
        ]
        let endpoint = NSString.path(withComponents: components)
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: endpoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"]
                let rideDetail =   Ride(json: payload)
                print(rideDetail)
                self.upcomingRide = rideDetail
                self.sortPassengerList()
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
    
}
extension RideDetailViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if upcomingRide == nil{
            return 0
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        } else if section == 1{
            if let passengers = upcomingRide.passengers{
                if passengers.count == 0{
                    return 1
                }
                var passengerCount = 0
                for passenger in upcomingRide.passengers!{
                    
                    if passenger.hasDriverRejectedBooking || passenger.hasCanceledBooking {
                        upcomingRide.passengers?.remove(at: passengerCount)
                    }else{
                        passengerCount += 1
                    }
                }
                if passengers.count == 0{
                    return 1
                }
                return passengers.count
            }
        } else if section == 2{
            
            var passengerCount = 0
            for passenger in upcomingRide.passengers!{
                
                if passenger.hasDriverRejectedBooking || passenger.hasCanceledBooking {
                    upcomingRide.passengers?.remove(at: passengerCount)
                }else{
                    passengerCount += 1
                }
            }
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section {
        case 0:
            return configureCellForFirstSection(indexPath: indexPath as NSIndexPath)
        case 1:
            return configureCellForSecondSection(indexPath: indexPath as NSIndexPath)
        case 2:
            return configureCellForThirdSection(indexPath: indexPath as NSIndexPath)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = hexStringToUIColor(hex: "EBEBEB")
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        switch section {
        case 0:
            label.text = "Routen"
        case 1:
            label.text = "Deine Mitfahrer"
        default:
            label.text = "Fahrtdetails"
        }
        vw.addSubview(label)
        label.backgroundColor = UIColor.clear
        label.frame = CGRect(x: 15, y: 8, width: 190, height: 20)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            
            if indexPath.row == 1 {
                return 65
            }
            return UITableViewAutomaticDimension // 75
            
        case 1:
            
            if upcomingRide.passengers?.count != 0 && indexPath.row == upcomingRide.passengers!.count  {
                return 62
            }
            if upcomingRide.passengers!.count != 0{
                return UITableViewAutomaticDimension //135
            }
            return 44
            
        case 2:
            if indexPath.row == 0{
                return 140
            }else{
                return 62
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func configureCellForFirstSection(indexPath: NSIndexPath)->UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "LocationDetailCell") as! LocationDetailCell
        if indexPath.row == 0{
           cell.startCityLabel.text = "Richtung: \(upcomingRide.start!.city) ➡︎ \(upcomingRide.destination!.city)"
            
        }else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MapDirectionCell") as! MapDirectionCell
            let directionTap = UITapGestureRecognizer(target: self, action:#selector(directionViewTapAction(_:)))
            cell.directionView.addGestureRecognizer(directionTap)
            return cell
        }
        return cell
    }
    
    func configureCellForSecondSection(indexPath: NSIndexPath)->UITableViewCell{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "FellowDriverCell") as! FellowDriverCell
        cell.upcomingRide = upcomingRide
        if upcomingRide.passengers?.count != 0 {
            cell.configureCell(passengers: upcomingRide.passengers!,index: indexPath.row)
        }else{
            cell.noBookingLabel.isHidden = false
            cell.baseView.isHidden = true
        }
        return cell
    }

    func configureCellForThirdSection(indexPath: NSIndexPath)->UITableViewCell{
        if indexPath.row == 0{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "JourneyDetailCell") as! JourneyDetailCell
            cell.startLabel.text = "Start: \(getCompleteDateFromDate(date: upcomingRide.startTime!))"
            cell.distanceLabel.text = "Entfernung: \(DistanceToString(upcomingRide.distance))"
            cell.durationLabel.text = "Fahrzeit: \(SecondsToHsMs(upcomingRide.duration))"
            cell.fareLabel.text = "Fahrpreis: \(CentsToString(Int(upcomingRide.price)))"
            return cell
        }else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CancelTripCell") as! CancelTripCell
            if indexPath.row == 1{
                cell.nameLabel.text = "Fahrt absagen"
            }
            cell.backView.backgroundColor = hexStringToUIColor(hex: "E88889")
            return cell
        }
        
    }
    
    func directionViewTapAction(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
        vc.upcomingRide = upcomingRide
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            if upcomingRide.passengers?.count != 0 && indexPath.row == upcomingRide.passengers!.count  {
                getThreadId()
            }
        }
        if indexPath.section == 2{
            if indexPath.row == 1{
                let alert = UIAlertController(title: "Fahrt absagen", message: "Willst du die Fahrt absagen?", preferredStyle: UIAlertControllerStyle.alert)
                let okaction = UIAlertAction(title: "Fahrt absagen", style: UIAlertActionStyle.default, handler: { action in
                    self.cancelTrip()
                })
                alert.view.tintColor = hexStringToUIColor(hex: MatchRiderColor)
                alert.addAction(okaction)
                let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    func getThreadId(){
        var receiverPersonIds: [String] = []
        for passenger in upcomingRide.passengers!{
            receiverPersonIds.append(passenger.userId)
        }
        let params = [
            "receiverPersonIds": receiverPersonIds
        ]
        self.view.showLoader()
        HttpRequest.sharedRequest.performPostRequest( Store.sharedStore.token, requestParam: params, endPoint: Endpoint.getMessageThreadId) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                var threadId = ""
                _ = payload.map { json in
                    threadId = json.string!
                }
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
                vc.messageThreadId = threadId
                vc.ride = self.upcomingRide
                self.navigationController?.pushViewController(vc, animated: true)
            }
            self.view.hideLoader()
        }
    }
    
    func cancelTrip(){
        self.view.showLoader()
        let endPoint = Endpoint.driverCancelRide
        let params = [
            "driverRequestId": upcomingRide.id
        ]
        print(params)
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: endPoint) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                if self.delegate != nil{
                self.delegate.cancelTripStatus(success: true)
                }
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
extension RideDetailViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
       checkNetworkAvailabilty()
    }
}

