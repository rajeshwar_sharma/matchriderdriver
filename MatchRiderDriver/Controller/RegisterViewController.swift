//
//  RegisterViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import Firebase
class RegisterViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let registerDetail = RegisterDetail.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Anmelden"
        setBackButton()
    }
    
}


extension RegisterViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RegisterDetailCell") as! RegisterDetailCell
        cell.txtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            cell.txtField.isSecureTextEntry = false
            cell.txtField.keyboardType = UIKeyboardType.default
            cell.txtField.autocapitalizationType = .none
        switch indexPath.row {
        case 0:
            cell.txtField.placeholder = "Vorname"
            cell.txtField.tag = 0
            cell.txtField.autocapitalizationType = .words

        case 1:
            cell.txtField.placeholder = "Nachname"
            cell.txtField.tag = 1
            cell.txtField.autocapitalizationType = .words
            
        case 2:
            cell.txtField.placeholder = "Email"
            cell.txtField.tag = 2
            cell.txtField.keyboardType = UIKeyboardType.emailAddress
        case 3:
            cell.txtField.placeholder = "Passwort"
            cell.txtField.tag = 3
            cell.txtField.isSecureTextEntry = true
        case 4:
            cell.txtField.placeholder = "Passwort wiederholen"
            cell.txtField.tag = 4
            cell.txtField.isSecureTextEntry = true
        default:
            break
        }
        
            return cell
        }else if indexPath.section == 1{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "RegisterGenderCell") as! RegisterGenderCell
            
            
            let checkTap = UITapGestureRecognizer(target: self, action: #selector(termsConditionImgViewTapAction(_:)))
            cell.checkImgView.addGestureRecognizer(checkTap)
            cell.checkImgView.tag = 2
            
            
            let agbViewTap = UITapGestureRecognizer(target: self, action: #selector(termsConditionViewTapAction(_:)))
            cell.agbLabel.addGestureRecognizer(agbViewTap)

            
            
            return cell
        }else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "RegisterCell") as! RegisterCell
            cell.registerButton.addTarget(self, action:#selector(self.registerButtonClicked), for: .touchUpInside)
            return cell
        }
       // return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 50
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
        
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        }
    
    func termsConditionImgViewTapAction(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview as! RegisterGenderCell
        if sender.view?.tag == 1{
            cell.checkImgView.tag = 2
            cell.checkImgView.image = UIImage(named: "unchecked")
            registerDetail.termsCondition = false
            
        }else{
            cell.checkImgView.tag = 1
            cell.checkImgView.image = UIImage(named: "checked")
            registerDetail.termsCondition = true
        }
    }
    func termsConditionViewTapAction(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImpressumViewController") as! ImpressumViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 0 {
            registerDetail.firstName = textField.text!.trim()
            
        } else if textField.tag == 1{
            registerDetail.lastName = textField.text!.trim()
            
        } else if textField.tag == 2{
            registerDetail.email = textField.text!.trim()
         
        } else if textField.tag == 3{
            registerDetail.password = textField.text!.trim()
            
        } else if textField.tag == 4{
            registerDetail.confirmPassword = textField.text!.trim()
        }
        
    }
    func registerButtonClicked() {
        
//        navigateToCommuteViewController()
//        return
//        return
        
        if NetworkReachability.isConnectedToNetwork(){
            registerUser()
        }else{
            self.showErrorMessage(message: NoNetworkError)
        }
          
    }
    
    func registerUser(){
        if !Validation.validateSignUp(registerDetail.firstName, secondName: registerDetail.lastName, email: registerDetail.email, password: registerDetail.password, confirmPassword: registerDetail.confirmPassword, termsCondition: registerDetail.termsCondition){
            return
        }
        
        let params = [
            "firstname": registerDetail.firstName,
            "lastname": registerDetail.lastName,
            "email": registerDetail.email,
            "password": registerDetail.password,
            "gender": "M"
        ]
        self.view.showLoader()
        print(params)
        HttpRequest.sharedRequest.performPostRequest(requestParam: params, endPoint: Endpoint.registerDriver) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                self.loginUser()
            }else{
                let payload = json["Payload"].array!.first?.dictionary
                let errMsg = payload?["error"]?.string
                self.showErrorMessage(message: errMsg!)
                
            }
        }
    }
    
    func loginUser(){
        
        self.view.showLoader()
        let params: [String : Any] = [
            "email": registerDetail.email,
            "password": registerDetail.password
        ]
        HttpRequest.sharedRequest.performPostRequest( requestParam:params, endPoint: Endpoint.Login) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json == nil{
                self.view.hideLoader()
                return
            }
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                Store.sharedStore.email = self.registerDetail.email
                let token = Token(json: (json["Payload"].array?.first)!)
                Store.sharedStore.token = token
                self.getAccountDetails()
            }else{
                self.view.hideLoader()
                self.showErrorMessage(message: LoginError)
            }
        }
    }
    
    func getAccountDetails(){
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getAccountDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                self.registerFCMDeviceTokenToServer()
                Utilities.saveUserData(user: data)
                self.navigateToCommuteViewController()
                self.registerDetail.setInitialValue()
            }
            
        }
    }
    
    func registerFCMDeviceTokenToServer(){
        if let deviceTokenId = FIRInstanceID.instanceID().token() {
            let params: [String : Any] = [
                "deviceId": deviceTokenId
            ]
            HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDeviceFCMToken           ) { payload in
                guard let data = payload else{
                    self.view.hideLoader()
                    return
                }
                let json = HttpRequest.sharedRequest.parseData(data: data)
                let status = json["Status"].string
                if status == HttpRequest.Status.Success{
                }
                self.view.hideLoader()
            }
        }
    }
    
    func navigateToCommuteViewController(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommuteViewController") as! CommuteViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
