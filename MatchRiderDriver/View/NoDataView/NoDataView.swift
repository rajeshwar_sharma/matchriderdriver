//
//  NoDataView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 16/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class NoDataView: UIView {
    @IBOutlet var messageLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    class func initView()->NoDataView{
        let noDataView = Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)?.first as! NoDataView
        return noDataView
    }
    func setMessage(message: String){
        messageLabel.text = message
    }
}
