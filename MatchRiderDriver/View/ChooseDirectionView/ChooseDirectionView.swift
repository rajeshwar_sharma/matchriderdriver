//
//  ChooseDirectionView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 31/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

protocol ChooseDirectionViewDelegate {
    func getSelectedDirection(direction: Int, superHighway: SuperHighway)
}

class ChooseDirectionView: UIView,UIGestureRecognizerDelegate {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var normalDirectionLabel: UILabel!
    @IBOutlet var reverseDirectionLabel: UILabel!
    
    var superHighway: SuperHighway!
    var delegate: ChooseDirectionViewDelegate! = nil
    
    class func initView()->ChooseDirectionView{
        let chooseDirectionView = Bundle.main.loadNibNamed("ChooseDirectionView", owner: self, options: nil)?.first as! ChooseDirectionView
        return chooseDirectionView
    }
    
    override func awakeFromNib() {
        self.setBackgroundColorTransparent()
    }
    
    func setData(superHighway: SuperHighway){
        self.superHighway = superHighway
        normalDirectionLabel.text = "\(superHighway.startHighwayDescription) ➡︎ \(superHighway.destHighwayDescription)"
        reverseDirectionLabel.text = "\(superHighway.destHighwayDescription) ➡︎ \(superHighway.startHighwayDescription)"
    }
    
    @IBAction func topViewTapAction(_ sender: Any) {
        delegate.getSelectedDirection(direction: 1, superHighway: self.superHighway)
        self.removeFromSuperview()
    }
    
    @IBAction func bottomViewTapAction(_ sender: Any) {
       delegate.getSelectedDirection(direction: 2, superHighway: self.superHighway)
        self.removeFromSuperview()
    }
    
    @IBAction func removeViewTapAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    //Mark:- UIGestureRecognizerDelegate Method
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: contentView){
            return false
        }
        return true
    }

    
}
