//
//  GPSTrackingMapView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 07/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
class GPSTrackingMapView: UIView {
    
    @IBOutlet var mapView: MKMapView!
    var currentLocation : CLLocation!
    let startAnnotation = Annotation()
    let destAnnotation = Annotation()
    var nextRide: Ride!
    var locationIdList: [Int:Annotation]! = [:]
    var matchpoints: [MKAnnotation] = []
    
    var matchPointsCount = 0
    
    class func initView()->GPSTrackingMapView{
        let trackingMapView = Bundle.main.loadNibNamed("GPSTrackingMapView", owner: self, options: nil)?.first as! GPSTrackingMapView
        return trackingMapView
    }
    override func awakeFromNib() {
        mapView.delegate = self
        matchpoints = []
    }
    func showCurrentLocation(){
        
        mapView.removeAnnotations(mapView.annotations)
        
        if matchpoints.count  > matchPointsCount  {
            matchpoints.removeLast()
        }
        
        let driverLocationannotation = Annotation()
        driverLocationannotation.coordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        driverLocationannotation.tag = 30
        driverLocationannotation.title = "You"
        matchpoints.append(driverLocationannotation)
        mapView.addAnnotations(matchpoints)
        //self.layoutIfNeeded()
        boundsPolyLineRouteInMap()
    }
    
    func drawRoot(ride: Ride?){
        nextRide = ride
        showMatchPoint()
    }
    
    func moveUserToCurrentLocation(){
        mapView.removeAnnotations(mapView.annotations)
        let annotation = Annotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        mapView.addAnnotation(annotation)
    }
    
    func showMatchPoint(){
        mapView.removeAnnotations(mapView.annotations)
        self.mapView.add(nextRide.polyline)
        
        matchpoints.removeAll()
        appDelegate.navigationMatchpoints.removeAll()
        
        mapView.removeAnnotations(mapView.annotations)
        locationIdList.removeAll()
        startAnnotation.coordinate = CLLocationCoordinate2D(latitude: nextRide.start!.latitude, longitude: nextRide.start!.longitude)
        startAnnotation.locationDescription = nextRide.start!.descriptionInfo
        startAnnotation.locationAddress = "\(nextRide.start!.street) \(nextRide.start!.state) \(nextRide.start!.postalCode)"
        startAnnotation.locationCity = nextRide.start!.city
        startAnnotation.locationId = nextRide.start!.id
        startAnnotation.tag = 10
        
        
        destAnnotation.coordinate = CLLocationCoordinate2D(latitude: nextRide.destination!.latitude, longitude: nextRide.destination!.longitude)
        destAnnotation.locationDescription = nextRide.destination!.descriptionInfo
        destAnnotation.locationAddress = "\(nextRide.destination!.street) \(nextRide.destination!.state) \(nextRide.destination!.postalCode)"
        destAnnotation.locationCity = nextRide.destination!.city
        destAnnotation.locationId = nextRide.destination!.id
        destAnnotation.tag = 11
        
        for passengerDetail in nextRide.passengers! {
            
            let annotationStart = locationIdList[passengerDetail.start!.id]
            if annotationStart != nil {
                annotationStart!.passengers.append(passengerDetail)
            }
            else{
                let passengerStartAnnotation = Annotation()
                
                passengerStartAnnotation.coordinate = CLLocationCoordinate2D(latitude: passengerDetail.start!.latitude, longitude: passengerDetail.start!.longitude)
                passengerStartAnnotation.locationDescription = passengerDetail.start!.descriptionInfo
                passengerStartAnnotation.locationAddress =  "\(passengerDetail.start!.street) \(passengerDetail.start!.state) \(passengerDetail.start!.postalCode)"
                
                passengerStartAnnotation.locationId = passengerDetail.start!.id
                locationIdList[passengerDetail.start!.id] = passengerStartAnnotation
                passengerStartAnnotation.tag = 20
                passengerStartAnnotation.driverTimeAtStopDate = passengerDetail.driverTimeAtStopDate
                passengerStartAnnotation.isStartPoint = true
                //passengerStartAnnotation.passenger = passengerDetail
                passengerStartAnnotation.passengers.append(passengerDetail)
                matchpoints.append(passengerStartAnnotation)
                appDelegate.navigationMatchpoints.append(passengerStartAnnotation)
            }
            
            let annotationDest = locationIdList[passengerDetail.destination!.id]
            if annotationDest != nil {
                annotationDest!.passengers.append(passengerDetail)
            }
            else{
                let passengerDestAnnotation = Annotation()
                passengerDestAnnotation.coordinate = CLLocationCoordinate2D(latitude: passengerDetail.destination!.latitude, longitude: passengerDetail.destination!.longitude)
                passengerDestAnnotation.locationDescription = passengerDetail.destination!.descriptionInfo
                passengerDestAnnotation.locationAddress = "\(passengerDetail.destination!.street) \(passengerDetail.destination!.state) \(passengerDetail.destination!.postalCode)"
                passengerDestAnnotation.locationId = passengerDetail.destination!.id
                locationIdList[passengerDetail.destination!.id] = passengerDestAnnotation
                passengerDestAnnotation.tag = 21
                passengerDestAnnotation.driverTimeAtStopDate = passengerDetail.driverTimeAtDropDate
                passengerDestAnnotation.isStartPoint = false
                
                // passengerDestAnnotation.passenger = passengerDetail
                passengerDestAnnotation.passengers.append(passengerDetail)
                matchpoints.append(passengerDestAnnotation)
                appDelegate.navigationMatchpoints.append(passengerDestAnnotation)
            }
        }
        
        let annotationStart = locationIdList[nextRide.start!.id]
        if annotationStart == nil {
            matchpoints.append(startAnnotation)
        }
        
        let annotationEnd = locationIdList[nextRide.destination!.id]
        if annotationEnd == nil {
            matchpoints.append(destAnnotation)
            appDelegate.navigationMatchpoints.append(destAnnotation)
        }
        
        matchPointsCount = matchpoints.count
        self.mapView.addAnnotations(matchpoints)
        self.mapView.showAnnotations(matchpoints, animated: false)
        self.layoutIfNeeded()
        boundsPolyLineRouteInMap()
        
    }
    
    func boundsPolyLineRouteInMap(){
        if let first = self.mapView.overlays.first {
            let rect = self.mapView.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
            self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
        }
    }
    
}

extension GPSTrackingMapView: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let trackingViewController = Utilities.getTopViewController()
        if let matchpoint = view.annotation as? Annotation {
            //|| matchpoint.tag == 10 || matchpoint.tag == 11
            if matchpoint.tag == 30 {
                //mapView.deselectAnnotation(view.annotation, animated: false)
                return
            }
            
            let vc = trackingViewController.storyboard?.instantiateViewController(withIdentifier: "MatchpointDetailController") as! MatchpointDetailController
            vc.annotation = matchpoint
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
            navigationController.navigationBar.tintColor = UIColor.white
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white
            ]
            trackingViewController.navigationController?.present(navigationController, animated: true, completion: nil)
            mapView.deselectAnnotation(view.annotation, animated: false)
        }
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = hexStringToUIColor(hex: MatchRiderColor)
        polylineRenderer.lineWidth = 4
        polylineRenderer.lineCap = .square
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }
        
        guard let matchpoint = annotation as? Annotation else {
            return nil
        }
        let view = MKAnnotationView(annotation: matchpoint, reuseIdentifier: "Matchpoint")
        view.canShowCallout = false
        switch matchpoint.tag {
        case 10:
            view.image = UIImage(named: "StartPoint")!
            break
        case 11:
            view.image = UIImage(named: "EndPoint")!
            break
        case 20:
            view.image = UIImage(named: "StartPoint")!
        case 21:
            view.image = UIImage(named: "EndPoint")!
        case 30:
            view.canShowCallout = true
            view.image = UIImage(named: "driver")!
        default:
            break
        }
        
        return view
    }
    
}
