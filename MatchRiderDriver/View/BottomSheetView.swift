//
//  BottomSheetView.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/01/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import UIKit
protocol BottomSheetViewDelegate {
    func filterButtonAction()
    func getSelected(route: Route?,city: CityModel?)
}
class BottomSheetView: UIView,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    var tableView: UITableView  =   UITableView()
    var delegate: BottomSheetViewDelegate!
    var count: CGFloat!
    var contentView = UIView()
    var topView = UIView()
    var cancelButton = UIButton()
    var filterButton = UIButton()
    var titleLabel = UILabel()
    var routes: [Route] = []
    var cities: [CityModel] = []
    var hideFilterButton: Bool!
    var isHidenFlag: Bool!
    override func awakeFromNib() {
        //isHidden = true
        hideFilterButton = false
        contentView.backgroundColor = UIColor.yellow
        topView.backgroundColor = hexStringToUIColor(hex: "7DBE7F")
        count = 2
        setCancelButton()
        setFilterButton()
        setTitleLabel()
        tableView.delegate      =   self
        tableView.dataSource    =   self
        tableView.register(UINib(nibName: "BottomSheetCell", bundle: nil), forCellReuseIdentifier: "BottomSheetCell")
        contentView.addSubview(tableView)
        contentView.addSubview(topView)
        self.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.addSubview(contentView)
    }
    
    func setViewHeight(){
        var yPoint = screenHeight/2
        var heightPoint = count*44
        yPoint = screenHeight/2  - 64
        heightPoint = screenHeight/2
        topView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 50)
        
        contentView.frame = CGRect(x: 0, y: screenHeight - 50, width: screenWidth, height:  heightPoint + 50)
        tableView.frame = CGRect(x: 0, y: 50, width: screenWidth, height: heightPoint)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.contentView.frame = CGRect(x: 0, y: yPoint - 50, width: screenWidth, height:  heightPoint + 50)
        })
        
    }
    
    
    func hideContentViewWithAnimation(){
        let heightPoint = screenHeight/2
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.frame = CGRect(x: 0, y: screenHeight - 50, width: screenWidth, height:  heightPoint + 50)
        }) { _ in
            self.isHidenFlag = true
            self.removeFromSuperview()
        }
    }
    
    func setCancelButton(){
        cancelButton.frame =  CGRect(x: 4, y: 10, width: 90 , height: 30)
        cancelButton.setTitle("Abbrechen", for: .normal)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        topView.addSubview(cancelButton)
    }
    func setFilterButton(){
        filterButton.frame =  CGRect(x: screenWidth-45, y: 10, width: 30, height: 30)
        filterButton.setImage(UIImage(named: "ic_city"), for: .normal)
        filterButton.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
        topView.addSubview(filterButton)
    }
    func setTitleLabel(){
        titleLabel.frame =  CGRect(x: screenWidth/2-75, y: 7, width: 150, height: 35)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        titleLabel.textColor = UIColor.white
        topView.addSubview(titleLabel)
        
    }
    func cancelButtonTapped(){
        hideContentViewWithAnimation()
    }
    func filterButtonTapped(){
        isHidenFlag = true
        self.removeFromSuperview()
        delegate.filterButtonAction()
        
    }
    @IBAction func removeBottomSheet(_ sender: AnyObject) {
        hideContentViewWithAnimation()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Int(count)
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BottomSheetCell", for: indexPath as IndexPath) as! BottomSheetCell
        cell.selectionStyle = .none
        var name = ""
        if routes.count != 0{
            let route = routes[indexPath.row]
            name = RouteDirectionString(route.startLocation, to: route.destinationLocation)
            //cell.nameLabel.textColor = hexStringToUIColor(hex: "7DBE7F")
            cell.nameLabel.textColor = UIColor.gray
        }
        else{
            let city = cities[indexPath.row]
            name = city.cityName
            var selectedCity = -1
            if (userDefault.object(forKey: "CityId") != nil){
                selectedCity = userDefault.object(forKey: "CityId") as! Int
            }
            if selectedCity == city.cityId {
                cell.nameLabel.textColor = hexStringToUIColor(hex: "7DBE7F")
              }
            else {
                cell.nameLabel.textColor = UIColor.black
            }
            
        }
        
        cell.nameLabel.text = name
        return cell;
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if routes.count != 0{
            delegate.getSelected(route: routes[indexPath.row], city: nil)
        }else{
            delegate.getSelected(route: nil, city: cities[indexPath.row])
        }
        isHidenFlag = true
        self.removeFromSuperview()
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: contentView))!{
            return false
        }
        return true
    }
    func setData(routes: [Route]? = nil,city: [CityModel]? = nil){
        if let routes = routes{
            self.routes = routes
            count = CGFloat(routes.count)
            self.cities.removeAll()
            setSelectedRouteName()
            setViewHeight()
            tableView.reloadData()
            }
        if let city = city{
            self.cities = city
            count = CGFloat(city.count)
            self.routes.removeAll()
            titleLabel.text = "Wählen Sie Stadt"
            setViewHeight()
            tableView.reloadData()
        }
        hideFilterButton! ?(filterButton.isHidden = true): (filterButton.isHidden = false)
        
    }
    
    func setSelectedRouteName(){
        let cityList = Store.sharedStore.cityList
        var selectedCityId = -1
        if (userDefault.object(forKey: "CityId") != nil){
            selectedCityId = userDefault.object(forKey: "CityId") as! Int
        }
        for city in cityList!{
            if selectedCityId == city.cityId {
                titleLabel.text = "\(city.cityName) Routen"
            }
            
        }

    }
}
