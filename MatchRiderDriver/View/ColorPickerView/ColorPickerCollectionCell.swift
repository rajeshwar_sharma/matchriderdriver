//
//  ColorPickerCollectionCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class ColorPickerCollectionCell: UICollectionViewCell {

    @IBOutlet var tickView: UIView!
    @IBOutlet var tickImgView: UIImageView!
    @IBOutlet var colorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
