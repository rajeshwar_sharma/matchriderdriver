//
//  CarDetailImageCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CarDetailImageCell: UITableViewCell {

    @IBOutlet var carImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        carImageView.clipsToBounds = true
    }
    
    func configureCell(car: Car){
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
