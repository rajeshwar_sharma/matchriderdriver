//
//  MatchPointCell2.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class MatchPointCell2: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var streetLabel: UILabel!
    @IBOutlet var postalLabel: UILabel!
    @IBOutlet var photoView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photoView.clipsToBounds = true
        photoView.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
