//
//  SideMenuCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var menuItemName: UILabel!
    
    //@IBOutlet var xConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.setCircleView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
