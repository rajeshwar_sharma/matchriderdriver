//
//  RouteCustomCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 18/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class RouteCustomCell: UITableViewCell {

    //@IBOutlet var routeNameLabel: UILabel!
    //@IBOutlet var leftArrowButton: UIButton!
    //@IBOutlet var rightArrowButton: UIButton!
    
    @IBOutlet var startLabel: UILabel!
    @IBOutlet var destinationLabel: UILabel!
    @IBOutlet var leftView: UIView!
    @IBOutlet var rightView: UIView!
    @IBOutlet var leftArrow: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       leftArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
//       leftArrowButton.setCircleView()
//       rightArrowButton.setCircleView()
    }

}
