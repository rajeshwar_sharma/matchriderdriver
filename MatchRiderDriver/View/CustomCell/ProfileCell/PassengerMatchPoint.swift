//
//  PassengerMatchPoint.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 10/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class PassengerMatchPoint: UITableViewCell {

    @IBOutlet var startLocationImgView: UIImageView!
    @IBOutlet var startLocationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var destinationLocationImgView: UIImageView!
    @IBOutlet var destinationNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        startLocationImgView.setCircleView()
        destinationLocationImgView.setCircleView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
