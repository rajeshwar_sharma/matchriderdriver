//
//  AcceptCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class AcceptCell: UITableViewCell {
    
    @IBOutlet var acceptView: UIView!
    var delegate: AcceptRejectPassengerDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(){
        let acceptTap = UITapGestureRecognizer(target: self, action:#selector(acceptViewTapAction(_:)))
        acceptView.addGestureRecognizer(acceptTap)
        
           }
    
    func acceptViewTapAction(_ sender: UITapGestureRecognizer) {
        print("Accept")
        delegate.acceptRejectPassenger(value: 1)
    }
   

}
