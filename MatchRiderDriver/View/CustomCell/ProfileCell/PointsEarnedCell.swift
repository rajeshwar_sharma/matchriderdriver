//
//  PointsEarnedCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 29/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class PointsEarnedCell: UITableViewCell {
    @IBOutlet var pointLabel: UILabel!

    @IBOutlet var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
