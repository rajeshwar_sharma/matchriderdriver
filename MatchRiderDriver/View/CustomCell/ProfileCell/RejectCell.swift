//
//  RejectCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class RejectCell: UITableViewCell {
    @IBOutlet var rejectView: UIView!
    var delegate: AcceptRejectPassengerDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(){
    let rejectTap = UITapGestureRecognizer(target: self, action:#selector(rejectViewTapAction(_:)))
    rejectView.addGestureRecognizer(rejectTap)
    }
    func rejectViewTapAction(_ sender: UITapGestureRecognizer) {
        print("reject")
        delegate.acceptRejectPassenger(value: 0)
    }
}
