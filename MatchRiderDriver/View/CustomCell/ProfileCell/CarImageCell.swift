//
//  CarImageCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CarImageCell: UITableViewCell {

    @IBOutlet var carImageView: UIImageView!
    @IBOutlet var editCarDetail: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        editCarDetail.setCircleView()
        carImageView.clipsToBounds = true
        
    }
    
    func configureCell(car: Car){
     
        let imgPath = car.photo
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: false) { image in
            if image != nil{
            self.carImageView.image = image
            }
        }
}


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
