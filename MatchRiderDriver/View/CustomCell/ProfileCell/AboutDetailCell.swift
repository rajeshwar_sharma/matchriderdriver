//
//  AboutDetailCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class AboutDetailCell: UITableViewCell {
    @IBOutlet var editAboutView: UIView!
    
    @IBOutlet var editImgView: UIImageView!
    @IBOutlet var textView: UITextView!
    var placeholderLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textView.delegate = self
        editAboutView.isHidden = true
        editImgView.changeImageColor(color: hexStringToUIColor(hex: MatchRiderColor))
        editAboutView.backgroundColor = UIColor.clear
        editImgView.tag = 1
        setTextViewPlaceHolder()
        textView.isEditable = false
    }
    func setTextViewPlaceHolder(){
        placeholderLabel = UILabel()
        placeholderLabel.text = "Wer  bist du?"
        placeholderLabel.font = UIFont.systemFont(ofSize: 14)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(){
        let tap = UITapGestureRecognizer(target: self, action:#selector(editAboutTapAction(_:)))
        let user = Utilities.getSignInUser()
        textView.text = user!.description
        placeholderLabel.isHidden = !textView.text.isEmpty
        editAboutView.addGestureRecognizer(tap)
    }
    func editAboutTapAction(_ sender: UITapGestureRecognizer) {
        if editImgView.tag == 1{
            editImgView.image = UIImage(named: "ic_done")
            editImgView.changeImageColor(color: hexStringToUIColor(hex: MatchRiderColor))
            editImgView.tag = 2
            textView.isEditable = true
        }
        else{
            editImgView.image = UIImage(named: "ic_edit")
            editImgView.changeImageColor(color: hexStringToUIColor(hex: MatchRiderColor))
            editImgView.tag = 1
            textView.isEditable = false
        }
    }
    
}
extension AboutDetailCell: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
extension AboutDetailCell: ProfileViewControllerDelegate{
    func updateUserDescription() {
        UserDetail.sharedInstance.aboutDescription = textView.text
        if textView.isEditable{
            
            textView.isEditable = false
        }
        else{
           
            textView.isEditable = true
        }

    }
}

