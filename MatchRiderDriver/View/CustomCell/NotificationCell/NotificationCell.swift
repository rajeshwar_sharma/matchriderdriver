//
//  NotificationCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 06/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet var notificationLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var profilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        accessoryType = .disclosureIndicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindDataWithCell(thread:ThreadsModel){
        nameLabel.text = thread.personName
        notificationLabel.text = thread.messagePosting
        if thread.isUnread{
           nameLabel.textColor = hexStringToUIColor(hex: "A19991")
           notificationLabel.textColor = hexStringToUIColor(hex: "A19991")
        }
        else{
            nameLabel.textColor = hexStringToUIColor(hex: MatchRiderColor)
            notificationLabel.textColor = hexStringToUIColor(hex: MatchRiderColor)
        }
        
        
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: thread.personPhoto, useCache: true) { image in
            if let image = image{
                self.profilePic.image = image
            }else{
                self.profilePic.image = UIImage(named: "icn_default")
            }
            
        }

    }
    
}
