//
//  RatingDetailCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 19/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
import Cosmos
class RatingDetailCell: UITableViewCell {

    @IBOutlet var userImageView: IconView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var deescriptionLabel: UILabel!
    
    @IBOutlet var halfCosmosView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindDataWithCell(ride:Ride){
//        HttpRequest.sharedRequest.image(ride.driver!.photo, useCache: true) { image in
//            self.userImageView.image = image
//        }
        
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: ride.passengers!.first!.photo, useCache: true) { image in
            self.userImageView.image = image
        }

        
        dateLabel.text = getDate(date: ride.startTime!)
        deescriptionLabel.text = "Wie war deine Fahrt mit \(ride.passengers!.first!.firstName)?"
        
        //halfCosmosView.rating = Double(ride.driver!.score)

    }
    func getDate(date:Date)->String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "de_DE")
        formatter.dateFormat = "dd.MM.yy HH:mm"
        return formatter.string(from: date)
        

    }
}
