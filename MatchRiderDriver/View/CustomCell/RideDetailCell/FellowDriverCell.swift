//
//  FellowDriverCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 31/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class FellowDriverCell: UITableViewCell {

    @IBOutlet var noBookingLabel: UILabel!
    
    @IBOutlet var baseView: UIView!
    
    @IBOutlet var passengerImageView: UIImageView!
    
    @IBOutlet var startLocationImageView: UIImageView!
    
    @IBOutlet var passengerNameLabel: UILabel!
    
    @IBOutlet var codeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
  // var upcomingRide: UpComingRideModel!
    
    //var passenger: PassengerListModel!
    var passengers: [PassengerListModel]!
    var upcomingRide: Ride!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layoutIfNeeded()
        passengerImageView.layer.cornerRadius = passengerImageView.frame.width/2
        passengerImageView.clipsToBounds = true
        startLocationImageView.layer.cornerRadius = startLocationImageView.frame.width/2
        startLocationImageView.layer.masksToBounds = true

    }
    
    override func layoutSubviews() {
         super.layoutSubviews()
            }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configureCell(passengers: [PassengerListModel]!,index: Int){
        self.passengers = passengers
        
            baseView.isHidden = false
            noBookingLabel.isHidden = true
            //let passenger = passengers[indexPath.row]
           let passenger = passengers[index]
            //var imgPath = "\(Endpoint.getPersonImage)\(passenger.userId)/m"
            var imgPath = "\(Endpoint.getPersonImage)\(passenger.userId)/l"
            HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
                self.passengerImageView.image = image
            }
            let passengerTap = UITapGestureRecognizer(target: self, action: #selector(self.handlePassengerImgViewTap(sender:)))
            passengerImageView.isUserInteractionEnabled = true
            passengerImageView.addGestureRecognizer(passengerTap)
            passengerImageView.tag = index
            
            imgPath = "\(Endpoint.getLocationImage)\(passenger.start!.id)"
            HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
                self.startLocationImageView.image = image
            }
            let startLocationTap = UITapGestureRecognizer(target: self, action: #selector(self.handleLocationImgViewTap(sender:)))
            startLocationImageView.isUserInteractionEnabled = true
            startLocationImageView.addGestureRecognizer(startLocationTap)
            startLocationImageView.tag = index
            
            passengerNameLabel.text = passenger.firstName
        //codeLabel.text = "\(passenger.start!.name)->\(passenger.destination!.name)"
           codeLabel.text = "\(passenger.start!.descriptionInfo) -> \(passenger.destination!.descriptionInfo)"
            dateLabel.text = passenger.driverTimeAtStop
        }
    func handleLocationImgViewTap(sender: UITapGestureRecognizer) {
        let rideNav = Utilities.getCurrentNavigationController()
        let rideVC = Utilities.getTopViewController()
        
        let view = sender.view
        let passenger = passengers![view!.tag]
        let vc = rideVC.storyboard?.instantiateViewController(withIdentifier: "MatchPointViewController") as! MatchPointViewController
        vc.passenger = passenger
        //vc.matchPointStr = "Start"
        rideNav.pushViewController(vc, animated: true)
    }
    func handlePassengerImgViewTap(sender: UITapGestureRecognizer) {
        let rideNav = Utilities.getCurrentNavigationController()
        let rideVC = Utilities.getTopViewController()
        let view = sender.view
        let passenger = passengers![view!.tag]
        let vc = rideVC.storyboard?.instantiateViewController(withIdentifier: "PassengerProfileViewController") as! PassengerProfileViewController
        vc.passenger = passenger
        vc.upcomingRide = upcomingRide
        rideNav.pushViewController(vc, animated: true)
    }
}
