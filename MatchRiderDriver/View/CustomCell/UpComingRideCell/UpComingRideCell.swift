//
//  UpComingRideCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class UpComingRideCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
