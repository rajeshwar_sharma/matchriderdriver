//
//  BottomSheetCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/01/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import UIKit

class BottomSheetCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
