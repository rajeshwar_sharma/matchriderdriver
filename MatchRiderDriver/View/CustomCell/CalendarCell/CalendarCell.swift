//
//  CalendarCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 12/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CalendarCell: UITableViewCell {
    @IBOutlet var startLabel: UILabel!
    @IBOutlet var endLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
