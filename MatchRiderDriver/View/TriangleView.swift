//
//  TriangleView.swift
//  ChatDemo
//
//  Created by daffolapmac on 21/12/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit
class TriangleView : UIView {
    var modeTriangle: TriangleMode
     init(frame: CGRect,mode: TriangleMode) {
        
        modeTriangle = mode
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        
        if modeTriangle == .Left {
            context.move(to: CGPoint(x: rect.maxX, y: rect.maxY))
            context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
            context.addLine(to: CGPoint(x: rect.minX, y: rect.midY))
            //context.addLine(to: CGPoint(x: rect.minX, y: rect.midY+3))
            context.setFillColor(hexStringToUIColor(hex: "7DBE7F").cgColor)
           }else if modeTriangle == .Right {
            context.move(to: CGPoint(x: rect.minX, y: rect.minY))
            context.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
            context.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
           // context.addLine(to: CGPoint(x: rect.maxX, y: rect.midY+3))
            context.setFillColor(UIColor.white.cgColor)
         }
        else{
//            context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
//            context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
//            context.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        }
        
        context.closePath()
        context.fillPath()
    }
}
