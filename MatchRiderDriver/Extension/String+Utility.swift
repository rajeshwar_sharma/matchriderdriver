//
//  String+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 25/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
extension String{
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
     func getActualContactNumber()->String{
        var num = self
       
        if num.hasPrefix("0049"){
        
            num = self.substring(from: self.characters.index(self.startIndex, offsetBy: 3))
            num = "0\(num)"
        }
        
        return num
    }
    
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
}
