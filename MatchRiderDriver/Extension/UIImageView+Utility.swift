//
//  UIImageView+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
extension UIImageView{
    func changeImageColor(color: UIColor){
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
}
