//
//  UINavigationController+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 04/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
extension UINavigationController{
    func setThemeDefaultColor(){
        self.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.isTranslucent = false
        self.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white
        ]
    }
}
