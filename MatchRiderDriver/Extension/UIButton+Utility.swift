//
//  UIButton+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
extension UIButton{
    func changeImageColor(color: UIColor){
//        self.image = self.image!.withRenderingMode(.alwaysTemplate)
//        self.tintColor = color
        
        let origImage = self.image(for: .normal)
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        self.setImage(tintedImage, for: .normal)
        self.tintColor = color
    }
}
