//
//  UITableView+Utility.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 28/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
extension UITableView{
    
    func showNoNotificationView() {
        self.backgroundView = Bundle.main.loadNibNamed("NoNotificationView", owner: self, options: nil)?.first as? UIView
        self.separatorStyle = .none
    }
    
    func showNoNewsView() {
        self.backgroundView = Bundle.main.loadNibNamed("NoNewsView", owner: self, options: nil)?.first as? UIView
        self.separatorStyle = .none
    }
    
    func hideEmptyRow(){
        self.tableFooterView = UIView()
    }
}
